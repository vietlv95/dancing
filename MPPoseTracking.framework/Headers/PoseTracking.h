//
//  PoseTracking.h
//  _idx_PoseTrackingGpuAppLibrary_93996E03_ios_min11.0
//
//  Created by Viet Le on 25/07/2023.
//

#import <Foundation/Foundation.h>
#import <CoreVideo/CoreVideo.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <Metal/Metal.h>
#import "PoseLandmarkPoint.h"
NS_ASSUME_NONNULL_BEGIN

@protocol PoseTrackingDelegate <NSObject>
@optional
- (void)poseTrackingDidReceiveTexture:(id<MTLTexture>)texture poses:(NSArray <NSArray<PoseLandmarkPoint *>*>*)poses timeStamp:(NSInteger)timeStamp;
@end

@interface PoseTracking : NSObject
- (instancetype)init;
- (void)startGraph;
- (void)processVideoFrame:(CVPixelBufferRef)imageBuffer;
- (void)processVideoFrame:(CVPixelBufferRef)imageBuffer timeStamp:(CMTime)timeStamp;
@property(weak, nonatomic) id<PoseTrackingDelegate> delegate;
@property(nonatomic) size_t timestamp;
@end

NS_ASSUME_NONNULL_END
