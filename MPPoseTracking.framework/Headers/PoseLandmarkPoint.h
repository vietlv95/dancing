//
//  PoseLandmarkPoint.h
//  _idx_PoseTrackingGpuAppLibrary_93996E03_ios_min11.0
//
//  Created by Viet Le on 25/07/2023.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PoseLandmarkPoint : NSObject
@property (nonatomic) CGFloat x;
@property (nonatomic) CGFloat y;
@property (nonatomic) CGFloat z;
@end

NS_ASSUME_NONNULL_END
