//
//  DIContainer.swift
//  dmv
//
//  Created by Khai Vuong on 09/02/2022.
//

import Foundation
import Swinject

class DIContainer {
    static let shared = DIContainer()
    
    var container: Container!
    
    init() {
        container = Container()
        registerMainViewModel()
        registerDanceInfoViewModel()
        registerGameViewModel()
        registerGameResultViewModel()
        registerRankViewModel()
        registerPreviewVideoViewModel()
        registerSettingViewModel()
        registerProfileViewModel()
        registerTrainViewModel()
        registerUploadedViewModel()
    }
    
    func registerMainViewModel() {
        self.container.register(MainViewModelFactory.self) { _ in
            return MainViewModel()
        }
    }
    
    func registerDanceInfoViewModel() {
        self.container.register(DanceInfoViewModelFactory.self) { _ in
            return DanceInfoViewModel()
        }
    }
    
    func registerGameViewModel() {
        self.container.register(GameViewModelFactory.self) { _ in
            return GameViewModel()
        }
    }
    
    func registerGameResultViewModel() {
        self.container.register(GameResultViewModelFactory.self) { _ in
            return GameResultViewModel()
        }
    }
    
    func registerRankViewModel() {
        self.container.register(RankViewModelFactory.self) { _ in
            return RankViewModel()
        }
    }
    
    func registerPreviewVideoViewModel() {
        self.container.register(PreviewVideoViewModelFactory.self) { _ in
            return PreviewVideoViewModel()
        }
    }
    
    func registerSettingViewModel() {
        self.container.register(SettingViewModelFactory.self) { _ in
            return SettingViewModel()
        }
    }
    
    func registerProfileViewModel() {
        self.container.register(ProfileViewModelFactory.self) { _ in
            return ProfileViewModel()
        }
    }
    
    func registerTrainViewModel() {
        self.container.register(TrainningViewModelFactory.self) { _ in
            return TrainningViewModel()
        }
    }
    
    func registerUploadedViewModel() {
        self.container.register(UploadedViewModelFactory.self) { _ in
            return UploadedViewModel()
        }
    }
}
