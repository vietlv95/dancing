//
//  MatchInfo.swift
//  dancing
//
//  Created by viet on 01/08/2023.
//

import Foundation
struct MatchInfo {
    let userID: Int
    let audioID: Int
    let score: Int
    let accuracy: CGFloat
    let pose_time: CGFloat
    let video_md5: String
    let cover_md5: String
    let playInfo: String
    let deviceID: String
}
