//
//  ScoreRake.swift
//  dancing
//
//  Created by viet on 31/07/2023.
//

import Foundation

private struct Constant {
    static let ANGLE_CONDITION_1: CGFloat = 5 //degree - difference angle
    static let ANGLE_CONDITION_2: CGFloat = 15 //degree - difference angle
    static let ANGLE_CONDITION_3: CGFloat = 22
    static let ANGLE_CONDITION_4: CGFloat = 30 //degree - difference angle
    static let ANGLE_CONDITION_5: CGFloat = 45
    static let ANGLE_POINT_1 = 4 //Added scores
    static let ANGLE_POINT_2 = 3 //Added scores
    static let ANGLE_POINT_3 = 2 //Added scores
    static let ANGLE_POINT_4 = 1 //Added scores
    static let ANGLE_POINT_5 = 1 //Added scores

    static let HIT_1_DURATION = 600 //miliseconds
    static let HIT_2_DURATION = 8000 //miliseconds
    static let HIT_3_DURATION = 1000 //miliseconds
    static let HIT_4_DURATION = 1100 //miliseconds
    static let HIT_5_DURATION = 1200 //miliseconds
    static let HIT_1_POINT = 10 //Added scores
    static let HIT_2_POINT = 8 //Added scores
    static let HIT_3_POINT = 6 //Added scores
    static let HIT_4_POINT = 4 //Added scores
    static let HIT_5_POINT = 2 //Added scores
    
    static let COMBO_LENGTH = 3
    static let COMBO_FACTOR: CGFloat = 1.5
}

enum PoseLandmarkPos: Int {
    case NOSE = 0
    case LEFT_EYE_INNER = 1
    case LEFT_EYE = 2
    case LEFT_EYE_OUTER = 3
    case RIGHT_EYE_INNER = 4
    case RIGHT_EYE = 5
    case RIGHT_EYE_OUTER = 6
    case LEFT_EAR = 7
    case RIGHT_EAR = 8
    case LEFT_MOUTH = 9
    case RIGHT_MOUTH = 10
    case LEFT_SHOULDER = 11
    case RIGHT_SHOULDER = 12
    case LEFT_ELBOW = 13
    case RIGHT_ELBOW = 14
    case LEFT_WRIST = 15
    case RIGHT_WRIST = 16
    case LEFT_PINKY = 17
    case RIGHT_PINKY = 18
    case LEFT_INDEX = 19
    case RIGHT_INDEX = 20
    case LEFT_THUMB = 21
    case RIGHT_THUMB = 22
    case LEFT_HIP = 23
    case RIGHT_HIP = 24
    case LEFT_KNEE = 25
    case RIGHT_KNEE = 26
    case LEFT_ANKLE = 27
    case RIGHT_ANKLE = 28
    case LEFT_HEEL = 29
    case RIGHT_HEEL = 30
    case LEFT_FOOT_INDEX = 31
    case RIGHT_FOOT_INDEX = 32
}

class ScoreRake {
    
    func getAngle(source: CGPoint, target1: CGPoint, target2: CGPoint) -> CGFloat {
        let a1 = getAngle(source: source, target: target1)
        let a2 = getAngle(source: source, target: target2)
        var result = a1 - a2
        if result < 0 {
            result += 360
        }
        return result
    }
    
    func getAngle(source: CGPoint, target: CGPoint) -> CGFloat {
        return atan2(target.y - source.y, target.x - source.x) * 180 / CGFloat.pi
    }
    
    func caculator(poseFrame: PoseFrame, danceFrame: DanceFrame, currentTime: Int, danceDetail: DanceDetail, index: Int) -> Score? {
        var score = 0
        var angleHolder = 0
        var isMatched = true
        for index in 0..<danceFrame.anglePoints.count {
            let anglePoint = danceFrame.anglePoints[index]
            let point0 = anglePoint.points[0]
            let point1 = anglePoint.points[1]
            let point2 = anglePoint.points[2]
//            if isFlip {
//                point0 = getFlipPoseLandmarkPos(pos: PoseLandmarkPos(rawValue: point0)!).rawValue
//                point1 = getFlipPoseLandmarkPos(pos: PoseLandmarkPos(rawValue: point1)!).rawValue
//                point2 = getFlipPoseLandmarkPos(pos: PoseLandmarkPos(rawValue: point2)!).rawValue
//            }
            
            let imageWidth = CGFloat(poseFrame.imageNode.texture.width)
            let imageHeight = CGFloat(poseFrame.imageNode.texture.height)
            
            let pose0 = CGPoint(x: poseFrame.poses.first![point0].x * imageWidth, y: poseFrame.poses.first![point0].y * imageHeight)
            let pose1 = CGPoint(x: poseFrame.poses.first![point1].x * imageWidth, y: poseFrame.poses.first![point1].y * imageHeight)
            let pose2 = CGPoint(x: poseFrame.poses.first![point2].x * imageWidth, y: poseFrame.poses.first![point2].y * imageHeight)
            
            let l_01 = sqrt((pose0.x - pose1.x) * (pose0.x - pose1.x) + (pose0.y - pose1.y) * (pose0.y - pose1.y))
            let l_02 = sqrt((pose0.x - pose2.x) * (pose0.x - pose2.x) + (pose0.y - pose2.y) * (pose0.y - pose2.y))
            let l_12 = sqrt((pose1.x - pose2.x) * (pose1.x - pose2.x) + (pose1.y - pose2.y) * (pose1.y - pose2.y))
            var userAngle = acos((l_01 * l_01 + l_02 * l_02 - l_12 * l_12) / (2 * l_01 * l_02)) * 180 / CGFloat.pi
//            var userAngle = getAngle(source: pose0, target1: pose1, target2: pose2)
            if userAngle < 0 {
                
            }
            let templateAngle = anglePoint.angle > 180 ? 360 - anglePoint.angle : anglePoint.angle
            userAngle = userAngle > 180 ? 360 - userAngle : userAngle
            
            let diff = abs(templateAngle - userAngle)
            
            if diff < Constant.ANGLE_CONDITION_1 || diff > 360 - Constant.ANGLE_CONDITION_1 {
                angleHolder = Constant.ANGLE_POINT_1
                score += Constant.ANGLE_POINT_1
                isMatched = isMatched && true
            } else if (diff < Constant.ANGLE_CONDITION_2 || diff > 360 - Constant.ANGLE_CONDITION_2) {
                score += Constant.ANGLE_POINT_2
                angleHolder = Constant.ANGLE_POINT_2
                isMatched = isMatched && true
            } else if (diff < Constant.ANGLE_CONDITION_3 || diff > 360 - Constant.ANGLE_CONDITION_3){
                score += Constant.ANGLE_POINT_3
                angleHolder = Constant.ANGLE_POINT_3
                isMatched = isMatched && true
            } else if (diff < Constant.ANGLE_CONDITION_4 || diff > 360 - Constant.ANGLE_CONDITION_4) {
                score += Constant.ANGLE_POINT_4
                angleHolder = Constant.ANGLE_POINT_4
                isMatched = isMatched && true
            } else if (diff < Constant.ANGLE_CONDITION_5 || diff > 360 - Constant.ANGLE_CONDITION_5) {
                score += Constant.ANGLE_POINT_5
                angleHolder = Constant.ANGLE_POINT_5
                isMatched = isMatched && true
            } else {
                isMatched = false
            }
            print("get \(score)")
        }
        
//        isMatched = true //test
        if isMatched {
            print("matchhh")
            var timeChecked = 1
            let timeAccess = danceFrame.middleTime
            // time acess = 1000
            // currentime = 800
            if currentTime <= timeAccess {
                if timeAccess < currentTime + Constant.HIT_1_DURATION {
                    score += Constant.HIT_1_POINT
                    timeChecked = 5
                } else if timeAccess < currentTime + Constant.HIT_2_DURATION {
                    score += Constant.HIT_2_POINT
                    timeChecked = 4
                } else if (timeAccess < currentTime + Constant.HIT_3_DURATION) {
                    score += Constant.HIT_3_POINT
                    timeChecked = 3
                } else if (timeAccess < currentTime + Constant.HIT_4_DURATION) {
                    score += Constant.HIT_4_POINT
                    timeChecked = 2
                } else if (timeAccess < currentTime + Constant.HIT_5_DURATION) {
                    score += Constant.HIT_5_POINT
                    timeChecked = 1
                }
            }
//            score = Int.random(in: 0..<5) //test
            return Score(minScore: danceFrame.anglePoints.count, score: score, isCombo: false, timeDiffScore: timeChecked, angleScore: angleHolder, des: "")
            
        }
        
        return nil
    }
    
    func rakeCombo(score: Score) -> Score {
        return Score(minScore: score.minScore, score: Int(CGFloat(score.score) * Constant.COMBO_FACTOR), isCombo: true, timeDiffScore: score.timeDiffScore, angleScore: score.angleScore, des: "")
    }
    
    func getFlipPoseLandmarkPos(pos: PoseLandmarkPos) -> PoseLandmarkPos {
        switch pos {
        case .NOSE:
            return .NOSE
        case .LEFT_EYE_INNER:
            return .RIGHT_EYE_INNER
        case .LEFT_EYE:
            return .RIGHT_EYE
        case .LEFT_EYE_OUTER:
            return .RIGHT_EYE_OUTER
        case .RIGHT_EYE_INNER:
            return .LEFT_EYE_INNER
        case .RIGHT_EYE :
            return .LEFT_EYE
        case .RIGHT_EYE_OUTER :
            return .LEFT_EYE_OUTER
        case .LEFT_EAR :
            return .RIGHT_EAR
        case .RIGHT_EAR :
            return .LEFT_EAR
        case .LEFT_MOUTH :
            return .RIGHT_MOUTH
        case .RIGHT_MOUTH :
            return .LEFT_MOUTH
        case .LEFT_SHOULDER :
            return .RIGHT_SHOULDER
        case .RIGHT_SHOULDER :
            return .LEFT_SHOULDER
        case .LEFT_ELBOW :
            return .RIGHT_ELBOW
        case .RIGHT_ELBOW :
            return .LEFT_ELBOW
        case .LEFT_WRIST :
            return .RIGHT_WRIST
        case .RIGHT_WRIST :
            return .LEFT_WRIST
        case .LEFT_PINKY :
            return .RIGHT_PINKY
        case .RIGHT_PINKY :
            return .LEFT_PINKY
        case .LEFT_INDEX :
            return .RIGHT_INDEX
        case .RIGHT_INDEX :
            return .LEFT_INDEX
        case .LEFT_THUMB :
            return .RIGHT_THUMB
        case .RIGHT_THUMB :
            return .LEFT_THUMB
        case .LEFT_HIP :
            return .RIGHT_HIP
        case .RIGHT_HIP :
            return .LEFT_HIP
        case .LEFT_KNEE :
            return .RIGHT_KNEE
        case .RIGHT_KNEE :
            return .LEFT_KNEE
        case .LEFT_ANKLE :
            return .RIGHT_ANKLE
        case .RIGHT_ANKLE :
            return .LEFT_ANKLE
        case .LEFT_HEEL :
            return .RIGHT_HEEL
        case .RIGHT_HEEL :
            return .LEFT_HEEL
        case .LEFT_FOOT_INDEX :
            return .RIGHT_FOOT_INDEX
        case .RIGHT_FOOT_INDEX :
            return .LEFT_FOOT_INDEX
        }
    }
}
