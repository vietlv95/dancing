//
//  Md5.swift
//  dancing
//
//  Created by viet on 01/08/2023.
//

import Foundation
import Foundation
import CommonCrypto


struct MD5 {
    static func get(data: Data) -> String {
        let hash = data.withUnsafeBytes { (bytes: UnsafePointer<Data>) -> [UInt8] in
                var hash: [UInt8] = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
                CC_MD5(bytes, CC_LONG(data.count), &hash)
                return hash
            }
        return hash.map { String(format: "%02x", $0) }.joined()
    }
}
