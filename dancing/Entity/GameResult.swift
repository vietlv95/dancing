//
//  GameResult.swift
//  dancing
//
//  Created by viet on 28/07/2023.
//

import UIKit

class GameResult: NSObject {
    var videoURL: URL!
    var score = 0
    var danceDetail: DanceDetail
    
    init(videoURL: URL!, score: Int = 0, danceDetail: DanceDetail) {
        self.videoURL = videoURL
        self.score = score
        self.danceDetail = danceDetail
        super.init()
    }
}
