//
//  PoseImageNode.swift
//  dancing
//
//  Created by viet on 25/07/2023.
//

import UIKit

class PoseFrame: NSObject {
    let imageNode: ImageNode
    let poses: [[PoseLandmarkPoint]]
    
    init(imageNode: ImageNode, poses: [[PoseLandmarkPoint]]) {
        self.imageNode = imageNode
        self.poses = poses
    }
}

extension PoseLandmarkPoint {
    open override var description: String {
        return "x: \(self.x), y: \(self.y)"
    }
}
