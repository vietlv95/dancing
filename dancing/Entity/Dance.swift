//
//  Dance.swift
//  dancing
//
//  Created by viet on 19/07/2023.
//

import Foundation
class Dance {
    var id = ""
    var title = ""
    var duration = ""
    var artist = ""
    var totalMath = 0
    var audioCover = ""
    var videoURL = ""
    var audio = ""
    var numPlayer = 0
    init() {
        
    }
    
    init(dict: [String: Any]) {
        id = dict["audio_id"] as! String
        title = dict["audio_title"] as! String
        duration = dict["video_duration"] as! String
        artist = dict["author_name"] as! String
        totalMath = dict["total_match"] as! Int
        audioCover = dict["audio_cover"] as! String
        videoURL = dict["video_url"] as! String
        audio = dict["video_audio"] as! String
        numPlayer = dict["total_player"] as! Int
    }
    
    func durationTimeFormat() -> String {
        let time = Int(Float(duration)!)
        return String(format: "%02D:%02D", time / 60, time % 60)
    }
    
    func durationTimeMilisecond() -> Int {
        return Int(Float(duration)! * 1000)
    }
    
    func localAudioUrl() -> URL {
        //improve
        let name = URL(string: audio)?.lastPathComponent ?? ""
        let path = CacheManager.shared.audioFolder().appending("/\(name)")
        return URL(fileURLWithPath: path)
    }
}
