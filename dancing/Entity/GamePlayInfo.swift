//
//  GamePlayInfo.swift
//  dancing
//
//  Created by viet on 01/08/2023.
//

import Foundation

struct GamePlayInfo {
    let level: Int
    let maxCombo: Int
    let score: Int
    let soundId: Int
    let urlVideo: String
    
    func toParam() -> String {
        return "{\"level\":\"\(level)\", \"maxCombo\":\"\(maxCombo)\", \"score\":\"\(score)\", \"soundId\":\"\(soundId)\", \"urlVideo\":\"\(urlVideo)\"}"
    }
}
