//
//  DanceFrame.swift
//  dancing
//
//  Created by viet on 20/07/2023.
//

import Foundation

class DanceFrame: NSObject {
    var cover = ""
    var anglePoints = [AnglePoint]()
    var startTime: Int = 0 // miliseconds
    var endTime: Int = 0
    var middleTime: Int = 0
    var duration: Int = 0
    var idx = 0
    var danceID = ""
    
    func coverCacheURL() -> URL {
        // can improve
        let name = URL(string: cover)?.lastPathComponent ?? ""
        let path = CacheManager.shared.cacheFrameFolder().appending("/\(danceID)_\(name)")
        return URL(fileURLWithPath: path)
    }
}


class AnglePoint {
    var name = ""
    var angle: CGFloat = 0
    var points = [Int]()
    
    init(dict: [String: Any]) {
        angle = dict["angle"] as! CGFloat
        name = dict["name"] as! String
        points = dict["points"] as! [Int]
    }
}
