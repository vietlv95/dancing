//
//  Match.swift
//  dancing
//
//  Created by viet on 03/08/2023.
//

import UIKit

class Match: NSObject {
    var id = 0
    var userID = 0
    var audioID = 0
    var video = ""
    var cover = ""
    var videoMD5 = ""
    var coverMD5 = ""
    var score = 0
    var accuracy: CGFloat = 0
    var poseTime: CGFloat = 0
    var playInfo = ""
    var receivedTime = 0
    
    var user: User!
    
    init(dict: [String: Any]) {
        self.id = dict["id"] as? Int ?? 0
        self.userID = dict["user_id"] as? Int ?? 0
        self.audioID = dict["audio_id"] as? Int ?? 0
        self.video = dict["video"] as? String ?? ""
        self.cover = dict["cover"] as? String ?? ""
        self.videoMD5 = dict["video_md5"] as? String ?? ""
        self.coverMD5 = dict["cover_md5"] as? String ?? ""
        self.score = dict["score"] as? Int ?? 0
        self.accuracy = dict["accuracy"] as? CGFloat ?? 0
        self.poseTime = dict["pose_time"] as? CGFloat ?? 0
        self.playInfo = dict["play_info"] as? String ?? ""
        self.receivedTime = dict["received_time"] as? Int ?? 0
    }
}
