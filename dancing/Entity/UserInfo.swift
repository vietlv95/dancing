//
//  UserInfo.swift
//  dancing
//
//  Created by viet on 19/07/2023.
//

import Foundation

class UserInfo {
    var name = "ananymous"
    var avatar = ""
    var birth_day = ""
    var gender = ""
    var id = 0
    var access_token = ""
    var expire_time = 0
    var accuracy_avg = ""
    var time_avg = ""
    var total_score = ""
    var rank = -1
    
    func loadInfo(from dict: [String: Any]) {
        self.id = dict["id"] as? Int ?? 0
        self.name = dict["name"] as? String ?? ""
        self.birth_day = dict["birth_day"] as? String ?? ""
        self.gender = dict["gender"] as? String ?? ""
        self.access_token = dict["access_token"] as? String ?? ""
        self.expire_time = dict["expire_time"] as? Int ?? 0
        self.accuracy_avg = dict["accuracy_avg"] as? String ?? ""
        self.time_avg = dict["time_avg"] as? String ?? ""
        self.total_score = dict["total_score"] as? String ?? ""
        self.rank = dict["rank"] as? Int ?? 0
        self.avatar = dict["avatar"] as? String ?? ""
    }
    
    func score() -> Int {
        return Int(Float(total_score) ?? 0)
    }
}
