//
//  Pose.swift
//  dancing
//
//  Created by viet on 09/08/2023.
//

import Foundation

enum Pose {
    case none
    case raiseLeftHand
    case raiseTwoHand
    case unowned
}
