//
//  UploadMatchResponse.swift
//  dancing
//
//  Created by viet on 01/08/2023.
//

import Foundation


class UploadMatchResult: Decodable {
    var accuracy: CGFloat = 0
    var audioID = 0
    var cover = ""
    var coverMd5 = ""
    var id = 0
    var playInfo = ""
    var poseTime: CGFloat = 0
    var rereceivedTime = 0
    var score = 0
    var userID = "-1"
    var video = ""
    var videoMd5 = ""
    
    init(dict: [String: Any]) {
        accuracy = dict["accuracy"] as? CGFloat ?? 0
        audioID = dict["audio_id"] as? Int ?? 0
        cover = dict["cover"] as? String ?? ""
        coverMd5 = dict["cover_md5"] as? String ?? ""
        id = dict["id"] as? Int ?? 0
        playInfo = dict["play_info"] as? String ?? "0"
        poseTime = dict["pose_time"] as? CGFloat ?? 0
        rereceivedTime = dict["received_time"] as? Int ?? 0
        score = dict["score"] as? Int ?? 0
        userID = dict["user_id"] as? String ?? "-1"
        video = dict["video"] as? String ?? ""
        videoMd5 = dict["video_md5"] as? String ?? ""
    }
}
