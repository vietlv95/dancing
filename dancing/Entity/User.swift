//
//  User.swift
//  dancing
//
//  Created by viet on 03/08/2023.
//

import UIKit

class User: NSObject {
    var id = 0
    var name = ""
    var avatar = ""
    var birthDay = ""
    var gender = ""
    var accuracyAvg = ""
    var timeAvg = ""
    var totalScore = ""
    var rank = 0
    
    init(dict: [String: Any]) {
        super.init()
        self.id = dict["id"] as? Int ?? 0
        self.name = dict["name"] as? String ?? ""
        self.avatar = dict["avatar"] as? String ?? ""
        self.birthDay = dict["birth_day"] as? String ?? ""
        self.gender = dict["gender"] as? String ?? ""
        self.accuracyAvg = dict["accuracy_avg"] as? String ?? ""
        self.timeAvg = dict["time_avg"] as? String ?? ""
        self.totalScore = dict["total_score"] as? String ?? ""
        self.rank = dict["rank"] as? Int ?? 0
    }
}
