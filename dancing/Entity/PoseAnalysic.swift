//
//  PoseAnalysic.swift
//  dancing
//
//  Created by viet on 09/08/2023.
//

import Foundation
class PoseAnalysic {
    var left_angle_1: CGFloat = 0
    var left_angle_2: CGFloat = 0
    var right_angle_1: CGFloat = 0
    var right_angle_2: CGFloat = 0
    
    func analysic(poseLandmarkPoints: [PoseLandmarkPoint], imageSize: CGSize) -> Pose {
        let points = poseLandmarkPoints.map { p in
            return CGPoint(x: imageSize.width * p.x, y: imageSize.height * p.y)
        }
        
        if points.isEmpty {
            return .none
        }
        
        var isRaiseLeftHand = false
        left_angle_1 = angleBetweenPoints(centerPoint: points[14], point1: points[16], point2: points[12])
        left_angle_2 = angleBetweenPoints(centerPoint: points[12], point1: points[14], point2: points[24])
        
        if left_angle_1 > 60 && left_angle_1 < 130  && points[16].y < points[12].y {
            isRaiseLeftHand = true
        }
        if left_angle_2 > 90 && left_angle_2 < 170 && points[14].y <= points[12].y {
            isRaiseLeftHand = isRaiseLeftHand && true
        }
        
        var isRaiseRightHand = false
        right_angle_1 = angleBetweenPoints(centerPoint: points[13], point1: points[15], point2: points[11])
        right_angle_2 = angleBetweenPoints(centerPoint: points[11], point1: points[13], point2: points[23])
        if right_angle_1 > 60 && right_angle_1 < 130 && points[15].y < points[11].y {
            isRaiseRightHand = true
        }
        if right_angle_2 > 90 && right_angle_2 < 170  && points[13].y <= points[11].y {
            isRaiseRightHand = isRaiseRightHand && true
        }
        
        let isRaiseTwoHand = isRaiseLeftHand && isRaiseRightHand
        
        if isRaiseTwoHand {
            return .raiseTwoHand
        }
        
        if isRaiseLeftHand {
            return .raiseLeftHand
        }
        
        return .unowned
    }
    
    func angleBetweenPoints(centerPoint: CGPoint, point1: CGPoint, point2: CGPoint) -> CGFloat {
        let l_01 = sqrt((centerPoint.x - point1.x) * (centerPoint.x - point1.x) + (centerPoint.y - point1.y) * (centerPoint.y - point1.y))
        let l_02 = sqrt((centerPoint.x - point2.x) * (centerPoint.x - point2.x) + (centerPoint.y - point2.y) * (centerPoint.y - point2.y))
        let l_12 = sqrt((point1.x - point2.x) * (point1.x - point2.x) + (point1.y - point2.y) * (point1.y - point2.y))
        
        let userAngle = acos((l_01 * l_01 + l_02 * l_02 - l_12 * l_12) / (2 * l_01 * l_02)) * 180 / CGFloat.pi
        return userAngle
    }
}
