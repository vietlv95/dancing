//
//  GameState.swift
//  dancing
//
//  Created by viet on 25/07/2023.
//

import UIKit

enum GameState {
    case showGuide
    case detectPeople
    case showReady
    case playing
    case finished
}
