//
//  SaveDance.swift
//  dancing
//
//  Created by viet on 08/08/2023.
//

import Foundation
import RealmSwift

class SaveDance: Object {
    @Persisted(primaryKey: true) var danceID = 0
    @Persisted var score: Int = 0
    @Persisted var totalScore: Int = 0
    @Persisted var saveVideoName: String = ""
}
