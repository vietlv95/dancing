//
//  DanceDetail.swift
//  dancing
//
//  Created by viet on 20/07/2023.
//

import Foundation
class DanceDetail {
    var dance: Dance!
    var frames: [DanceFrame] = []
    
    func updateFrameTime() {
        for i in 0...frames.count - 1 {
            let frame = frames[i]
            if i == 0 {
                frame.startTime = 0
                frame.endTime = (frames[1].middleTime + frame.middleTime) / 2
            } else if i == frames.count - 1 {
                frame.startTime = (frame.middleTime + frames[i - 1].middleTime) / 2
                frame.endTime = dance.durationTimeMilisecond()
            } else {
                frame.startTime = (frame.middleTime + frames[i - 1].middleTime) / 2
                frame.endTime = (frame.middleTime + frames[i + 1].middleTime) / 2
            }
        }
    }
}
