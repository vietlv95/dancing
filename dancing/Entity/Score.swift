//
//  Score.swift
//  dancing
//
//  Created by viet on 25/07/2023.
//

import UIKit

enum ScoreRank: String {
    case perfect = "score_perfect"
    case great = "score_great"
    case cool = "score_cool"
    case ok = "score_ok"
    case fail = ""
}

struct Score {
    let minScore: Int
    let score: Int
    let isCombo: Bool
    let timeDiffScore: Int
    let angleScore: Int
    let des: String
    var scoreRank = ScoreRank.fail
    
    init(minScore: Int, score: Int, isCombo: Bool, timeDiffScore: Int, angleScore: Int, des: String) {
        self.minScore = minScore
        self.score = score
        self.isCombo = isCombo
        self.timeDiffScore = timeDiffScore
        self.angleScore = angleScore
        self.des = des
        
        switch score - minScore {
        case 2...6:
            scoreRank = .ok
        case 7...12:
            scoreRank = .cool
        case 12...24:
            scoreRank = .great
        case 25..<10000:
            scoreRank = .perfect
        default: scoreRank = .fail
        }
    }
}
