//
//  AppState.swift
//  dancing
//
//  Created by viet on 19/07/2023.
//

import Foundation

class AppState {
    static let shared = AppState()
    var userInfo: UserInfo?
    var appVersion: String = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    
    var settingMusicValue: Float {
        get {
            return UserDefaults.standard.value(forKey: "settingMusicValue") as? Float ?? 1
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "settingMusicValue")
        }
    }
    
    var settingSoundValue: Float {
        get {
            return UserDefaults.standard.value(forKey: "settingSoundValue") as? Float ?? 1
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "settingSoundValue")
        }
    }
}
