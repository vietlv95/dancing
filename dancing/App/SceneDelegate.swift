//
//  SceneDelegate.swift
//  dancing
//
//  Created by viet on 11/07/2023.
//

import UIKit
import Firebase

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        configWindow(for: scene)
        setRootViewController()
        
//        CacheManager.shared.clearFrameAndAudioCache()
        CacheManager.shared.initFolderIfNeed()
        configFirebase()
        guard let _ = (scene as? UIWindowScene) else { return }
    }
    
    private func configWindow(for scene: UIScene) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        try? AVAudioSession.sharedInstance().setCategory(.playback)
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        window?.isHidden = false
        if #available(iOS 13.0, *) {
            self.window?.overrideUserInterfaceStyle = .light
        }
    }
    
    func setRootViewController() {
//        let vc = HomeViewController()
//        window?.rootViewController = vc
//        return
        let coodinator = MainCoordinator(window: window!)
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//            let p = ProfileCoordinator(navigation: coodinator.nav)
//            p.start()
//        }
//        let coodinator = GameResultCoordinator(window: window!)
        coodinator.start()
    }
    
    private func configFirebase() {
        if FirebaseApp.app() == nil {
            FirebaseApp.configure()
        }
        
        let deviceID = UIDevice.current.identifierForVendor?.uuidString ?? "UserIDNotGeneric"
        Analytics.setUserID(deviceID)
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}

