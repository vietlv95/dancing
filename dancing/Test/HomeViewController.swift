//
//  HomeViewController.swift
//  dancing
//
//  Created by viet on 11/07/2023.
//

import UIKit
import SpriteKit
import Metal
import MetalKit

class HomeViewController: UIViewController {
    private var metalRender: MetalMixingRender!
    private var scene: Scene!
    private var skrenderer: SKRenderer!
    private var mtkView: MTKView!
    private var skView: SKView!
    
    private var device: MTLDevice!
    private var commandQueue: MTLCommandQueue!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configGameRender()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scene.test()
    }
    
    private func configGameRender() {
        scene = Scene(size: view.bounds.size)
        scene.backgroundColor = .clear
        scene.isPaused = false
        
        skView = SKView()
        skView.frame = self.self.view.bounds
        self.view.addSubview(skView)
        skView.fitSuperviewConstraint()
        
        skView.presentScene(scene)
        skView.showsFPS = true
        skView.backgroundColor = .clear
        
        let button = UIButton(type: .system)
        button.setTitle("test", for: .normal)
        button.frame = CGRect(x: 200, y: 300, width: 40, height: 40)
        button.backgroundColor = .blue
        button.addTarget(self, action: #selector(test), for: .touchUpInside)
        view.addSubview(button)
    }
    
    @objc func test() {
        scene.testAnimation()
    }
}
