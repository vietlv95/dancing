//
//  Scene.swift
//  dancing
//
//  Created by viet on 11/07/2023.
//

import Foundation
import SpriteKit

class Scene: SKScene {
    
    var starNode1: SKEmitterNode?
    var starNode2: SKEmitterNode?
    
    func playStar() {
        
        starNode1 = SKEmitterNode(fileNamed: "Star")
        starNode1?.position = CGPoint(x: size.width / 2, y: size.height / 2)

        addChild(starNode1!)
    }
    
    var node: SKSpriteNode!
    func test() {
        node = SKSpriteNode(imageNamed: "bg.png")
        addChild(node)
        node.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        node.position = CGPoint(x: 200, y: 300)
        node.size = CGSize(width: 200, height: 100)
        
        
    }
    
    func testAnimation() {
        let src = [
            // bottom row: left, center, right
            vector_float2(0.0, 0.0),
            vector_float2(0.5, 0.0),
            vector_float2(1.0, 0.0),

            // middle row: left, center, right
            vector_float2(0.0, 0.5),
            vector_float2(0.5, 0.5),
            vector_float2(1.0, 0.5),

            // top row: left, center, right
            vector_float2(0.0, 1.0),
            vector_float2(0.5, 1.0),
            vector_float2(1.0, 1.0)
        ]
        
        var dst = src
        dst[0] = vector_float2(0, -0.5)
        dst[2] = vector_float2(1, -0.5)
        
        let newWarp = SKWarpGeometryGrid(columns: 2, rows: 2, sourcePositions: src, destinationPositions: dst)

        // pull out the existing warp geometry so we have something to animate back to
        let oldWarp =  SKWarpGeometryGrid(columns: 2, rows: 2, sourcePositions: dst, destinationPositions: src)
        
        let action = SKAction.resize(toWidth: 400, height: 200, duration: 0.4)
        let wait = SKAction.wait(forDuration: 0.4)
        let resizeAction = SKAction.resize(byWidth: 200, height: 100, duration: 0.4)
        let schedule = SKAction.repeatForever(SKAction.sequence([action, wait, resizeAction, wait]))
        node.run(schedule)
    }
}
