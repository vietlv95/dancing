//
//  NSBundleExtensions.swift
//  dmv
//
//  Created by Khai Vuong on 10/02/2022.
//

import Foundation
import UIKit

extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
}
}
