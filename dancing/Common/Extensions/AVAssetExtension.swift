//
//  AVAssetExtension.swift
//  BabyPhoto
//
//  Created by Le Toan on 9/10/20.
//  Copyright © 2020 Solar. All rights reserved.
//

import AVFoundation
import UIKit

extension AVAsset {
    var thumbnailImage: UIImage {
        var image = UIImage()
        do {
            let imgGenerator = AVAssetImageGenerator(asset: self)
            let cgImage = try! imgGenerator.copyCGImage(at: .zero, actualTime: nil)
            image = UIImage.init(cgImage: cgImage, scale: 1, orientation: image.imageOrientation)
            image = image.resize(to: CGSize(width: 800, height: 800))
        }
        
        return image
    }
    
    func imageAtTime(_ time: CMTime) -> UIImage {
        var image = UIImage()
        do {
            let imgGenerator = AVAssetImageGenerator(asset: self)
            imgGenerator.maximumSize = CGSize(width: 800, height: 800)
            let cgImage = try! imgGenerator.copyCGImage(at: time, actualTime: nil)
            image = UIImage.init(cgImage: cgImage, scale: 1, orientation: image.imageOrientation)
        }
        
        return image
    }
    
    func trim(left: CGFloat, right: CGFloat) -> AVAsset {
        let compition = AVMutableComposition()
        compition.copyAllTrack(from: self)
        let newComposition = AVMutableComposition()
        
        compition.tracks.forEach { track in
            let newTrack = newComposition.addMutableTrack(withMediaType: track.mediaType, preferredTrackID: kCMPersistentTrackID_Invalid)
            newTrack?.preferredVolume = track.preferredVolume
            newTrack?.preferredTransform = track.preferredTransform
            let trimTime = CMTimeMultiplyByFloat64(compition.duration, multiplier: left)
            let duration = CMTimeMultiplyByFloat64(compition.duration, multiplier: right - left)
            try? newTrack?.insertTimeRange(CMTimeRange(start: trimTime, duration: duration), of: track, at: .zero)
        }
        
        return newComposition
    }
}
