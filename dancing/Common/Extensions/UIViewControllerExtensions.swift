//
//  UIViewControllerExtensions.swift
//  BabyPhoto
//
//  Created by Thanh Vu on 7/17/20.
//  Copyright © 2020 Solar. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func topVC() -> UIViewController {
        if let navigation = self as? UINavigationController, !navigation.viewControllers.isEmpty {
            return navigation.topViewController!.topVC()
        }
        
        if let presentedVC = self.presentedViewController {
            return presentedVC.topVC()
        }
        
        return self
    }
    
    func showAlertMessage(titleStr:String, messageStr:String, completion: CompletionBlock = nil) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
            completion?()
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
