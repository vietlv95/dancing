//
//  AVMutableCompositionExtensions.swift
//  BabyPhoto
//
//  Created by Le Toan on 9/14/20.
//  Copyright © 2020 Solar. All rights reserved.
//

import AVFoundation

extension AVMutableComposition {
    func copyAllTrack(from asset: AVAsset) {
        copyTrack(withType: .audio, from: asset)
        copyTrack(withType: .video, from: asset)
    }
    
    
    func copyAudioTrack(from asset: AVAsset, volume: Float) {
        let tracks = asset.tracks(withMediaType: .audio)
        for track in tracks {
            let compositionTrack = self.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
            compositionTrack?.preferredTransform = tracks.first!.preferredTransform
            compositionTrack?.preferredVolume = volume
            try? compositionTrack?.insertTimeRange(track.timeRange, of: track, at: track.timeRange.start)
        }
    }
    
    func copyTrack(withType type: AVMediaType, from asset: AVAsset) {
        let tracks = asset.tracks(withMediaType: type)
        for track in tracks {
            let compositionTrack = addMutableTrack(withMediaType: type, preferredTrackID: kCMPersistentTrackID_Invalid)
            compositionTrack?.preferredTransform = tracks.first!.preferredTransform
            compositionTrack?.preferredVolume = tracks.first!.preferredVolume
            try? compositionTrack?.insertTimeRange(track.timeRange, of: track, at: track.timeRange.start)
        }
    }
    
    func mergeAudioTrack(from asset: AVAsset) {
        let audioTrack = asset.tracks(withMediaType: .audio).first
        let compositionTrack = addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
        compositionTrack?.preferredTransform = tracks.first!.preferredTransform
        compositionTrack?.preferredVolume = tracks.first!.preferredVolume
        
        var time: CMTime = .zero
        while(CMTimeCompare(time, self.duration) == -1) {
            let timeRemain = CMTimeSubtract(self.duration, time)
            let insertTimeDuration = CMTimeMinimum(self.duration, timeRemain)
            try? compositionTrack?.insertTimeRange(CMTimeRange(start: .zero, duration: insertTimeDuration), of: audioTrack!, at: time)
            time = CMTimeAdd(time, asset.duration)
        }
        _ = 1
    }
}
