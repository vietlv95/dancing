//
//  UIColor.swift
//  dmv
//
//  Created by Khai Vuong on 10/02/2022.
//

import Foundation
import UIKit

struct AppColor {
    static let backgroundColor = UIColor.white
}
