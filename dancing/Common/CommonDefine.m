//
//  CommonDefine.m
//  dancing
//
//  Created by viet on 01/08/2023.
//

#import "CommonDefine.h"

CMTime CMTimeFromNSString(NSString *string) {
    return kCMTimeZero;
}

void openSettingApp() {
    NSURL *settingURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    if ([UIApplication.sharedApplication canOpenURL:settingURL]) {
        [UIApplication.sharedApplication openURL:settingURL options:@{} completionHandler:nil];
    }
}

NSString* appName() {
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
}

PHAssetCollection* GetAlbum() {
    PHFetchOptions* options = [[PHFetchOptions alloc] init];
    options.predicate = [NSPredicate predicateWithFormat:@"title = %@", appName()];
    PHFetchResult<PHAssetCollection*>* result = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAny options:options];
    return result.firstObject;
}

extern void createAlbum(_Nullable Completion completion){
    [PHPhotoLibrary.sharedPhotoLibrary
        performChanges:^{
            [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:appName()];
        }
        completionHandler:^(BOOL success, NSError* _Nullable error) {
        completion();
        }];
}

