//
//  IgnoreView.swift
//  dancing
//
//  Created by viet on 03/08/2023.
//

import UIKit

class IgnoreView: UIView {
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hitView = super.hitTest(point, with: event)
        if hitView == self {
            return nil
        }
        return hitView
    }
}
