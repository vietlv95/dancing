//
//  Utils.swift
//  dmv
//
//  Created by Khai Vuong on 10/02/2022.
//

import Foundation
typealias CompletionBlock = (() -> Void)?

struct DateFormat {
    static let longFormat = "yyyy-MM-dd HH:mm:ss"
    static let shortFormat = "yyyy-MM-dd"
}

struct NotificationName {
    static let ChooseTestDate = Notification.Name.init("ChooseTestDate")
    static let ShowLastedTest = Notification.Name.init("ShowLastedTest")
    static let PracticeNow = Notification.Name.init("PracticeNow")
    static let ShowAppGuideline = Notification.Name.init("ShowAppGuideline")
    static let ShowPracticeOverview = Notification.Name.init("ShowPracticeOverview")
    static let NeedReloadHomeScreen = Notification.Name.init(rawValue: "NeedReloadHomeScreen")
}

class Utils {
    
    static func stringFromDate(_ date: Date, format: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
    
    static func dateFromString(_ string: String, format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: string)
    }
    
    static func stringFromDateWithOrdinary(_ date: Date, format: String) -> (date: String?, ordinator: String?) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let day = date.day
        switch day {
        case 1, 21, 31:
            return (date: dateFormatter.string(from: date), ordinator: "st")
        case 2, 22:
            return (date: dateFormatter.string(from: date), ordinator: "nd")
        case 3, 23:
            return (date: dateFormatter.string(from: date), ordinator: "rd")
        default:
            return (date: dateFormatter.string(from: date), ordinator: "th")
        }
    }
}
