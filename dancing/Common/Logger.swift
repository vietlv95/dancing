//
//  Logger.swift
//  dmv
//
//  Created by Khai Vuong on 10/02/2022.
//

import Foundation
import UIKit
import Firebase
import FirebaseAnalytics

enum LogEventName: String {
    case changeAvatar = "change_avatar"
    case openProfile = "open_profile"
    case prepareData = "prepare_data"
    case previewVideo = "preview_video"
    case previewVideoInProfile = "preview_video_in_profile"
    case replayGame = "replay_game"
    case save = "save"
    case shareFile = "share_file"
    case showEndGame = "show_end_game"
    case showLeaderBoard = "show_leader_board"
    case startGame = "start_game"
}

class Logger {
    static func logEvent(_ name: LogEventName, params: [String: Any] = [:], onceTime: Bool = false) {
        var dict = params
        dict["P_UserID"] = userID
        dict["P_Mode"] = mode
        if onceTime && UserDefaults.standard.bool(forKey: name.rawValue) {
            return
        }
        
        Analytics.logEvent(name.rawValue, parameters: dict)
        
        if onceTime {
            UserDefaults.standard.setValue(true, forKey: name.rawValue)
        }
        
        print("Loggger Log event:   \(name)")
    }
    
//    static func logEvent(name: String, params: [String: Any] = [:], onceTime: Bool = false) {
//        var dict = params
//        var purchaseInfo: String {
//            return ""
////            if PremiumUser.isLifeTime {
////                return "LifetimeUser"
////            } else if PremiumUser.isTrial {
////                return "TrialUser"
////            } else if PremiumUser.isPremium {
////                return "PremiumUser"
////            } else {
////                return "FreeUser"
////            }
//        }
//        dict["P_UserID"] = userID
//        dict["P_Mode"] = mode
//        dict["P_PurchaseInfo"] = purchaseInfo
//        if onceTime && UserDefaults.standard.bool(forKey: name) {
//            return
//        }
//        
//        Analytics.logEvent(name, parameters: dict)
//        
//        if onceTime {
//            UserDefaults.standard.setValue(true, forKey: name)
//        }
//        
//        print("Loggger Log event:   \(name)")
//    }
    
    private static var mode: String {
        #if DEBUG
            return "Debug"
        #else
            return "Release"
        #endif
    }
    
    private static var userID: String {
        let deviceID = UIDevice.current.identifierForVendor?.uuidString ?? "UserIDNotGeneric"
        return deviceID
    }
    
    struct EventName {
        static let camera_permission_accepted = "camera_permission_accepted" //
                
        static let change_resolution = "change_resolution"
        
        static let open_app_with_alarm = "open_app_with_alarm" // no feature alarm
        static let photo_with_item = "photo_with_item" // no feature photo
    
        
        static let preview_mask = "preview_mask"
        static let preview_video = "preview_video"
        static let record_with_item = "record_with_item"
        
        static let remove_bookmark_item = "remove_bookmark_item"//* no feature bookmark
        
        
        static let remove_video_from_stack = "remove_video_from_stack"
        static let save_image = "save_image"// no feature
        
        static let save_video = "save_video"
        static let share_file = "share_file"
        static let show_mask_layout = "show_mask_layout"
        static let show_notification_from_alarm = "show_notification_from_alarm"
        
        static let stop_record = "stop_record"
        static let switch_camera = "switch_camera"
    }
    
    struct ScreenName {
        
    }
}
