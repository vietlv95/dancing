//
//  CommonDefine.h
//  dancing
//
//  Created by viet on 01/08/2023.
//
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>
#import <Photos/Photos.h>

#ifndef CommonDefine_h
#define CommonDefine_h

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#define CGFloatMax(number1,number2) number1 > number2 ? number1 : number2
#define CGFloatMin(number1,number2) number1 < number2 ? number1: number2

#define weakify(VAR) __attribute__((objc_ownership(weak))) __typeof__(VAR) __weak_##VAR = (VAR)
#define strongify(VAR) __attribute__((objc_ownership(strong))) __typeof__(__weak_##VAR) VAR = (__weak_##VAR)

#define StringFromInteger(number) [NSString stringWithFormat:@"%ld", (long)number]
#define kDurationFormat @"%02ld:%02ld"
#define DurationStringFromTimeInterval(timeInterval) [NSString stringWithFormat:kDurationFormat,(NSInteger)timeInterval / 60, (NSInteger)timeInterval % 60]
#define DurationStringFromCMTime(cmTime) DurationStringFromTimeInterval((NSInteger)CMTimeGetSeconds(cmTime))

#define ByPassNan(Number) isnan(Number) ? 0 : Number

#define isnan(x)                                                         \
    ( sizeof(x) == sizeof(float)  ? __inline_isnanf((float)(x))          \
    : sizeof(x) == sizeof(double) ? __inline_isnand((double)(x))         \
                                  : __inline_isnanl((long double)(x)))

#endif /* Define_h */


typedef void(^Completion)(void);

extern CMTime CMTimeFromNSString(NSString *string);
extern void openSettingApp(void);
extern NSString* appName(void);

extern PHAssetCollection* _Nullable GetAlbum(void);
extern void createAlbum(_Nullable Completion completion);

