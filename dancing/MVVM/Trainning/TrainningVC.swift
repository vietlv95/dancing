//
//  TrainningVC.swift
//  dancing
//
//  Created by viet on 15/08/2023.
//

import UIKit
import SpriteKit
import Metal
import MetalKit

class TrainningVC: BaseViewController<TrainningViewModelFactory> {
    var coordinator: TrainningCoordinator!
    var danceDetail: DanceDetail!
    // MARK: - IBOutlet
    
    @IBOutlet weak var passLabel: UILabel!
    private var skrenderer: SKRenderer!
    private var skView: SKView!
    private var mtkView: MTKView!
    private var metalRender: MetalMixingRender!
    private var detectPeopleView: DetectPeopleView!
    
    private var scene: TrainSence!
    private var cutOffX: CGFloat = 0
    private var cutOffY: CGFloat = 0
    private var device: MTLDevice!
    private var commandQueue: MTLCommandQueue!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
        
        viewModel.danceDetail = danceDetail
        viewModel.startCamera()
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.viewModel.startTrain()
        }
    }
    
    override func viewDidFirstLayoutSubView() {
        super.viewDidFirstLayoutSubView()
        self.view.layoutIfNeeded()
        updateCutoff()
    }

    // MARK: - Config
    func config() {
        passLabel.isHidden = true
        configRender()
    }
    
    private func configRender() {
        device = MTLCreateSystemDefaultDevice()
        commandQueue = device.makeCommandQueue()
        mtkView = MTKView()
        metalRender = MetalMixingRender(device: device, commandQueue: commandQueue!, mtkView: mtkView)
        mtkView.device = device
        view.insertSubview(mtkView, at: 0)
        mtkView.fitSuperviewConstraint()
        mtkView.delegate = metalRender
        
        scene = TrainSence(size: view.bounds.size)
        scene.backgroundColor = .clear
        scene.isPaused = false
        
        skrenderer = SKRenderer(device: device)
        skrenderer.scene = scene
    }
    
    func bindViewModel() {
        viewModel.danceDetail = danceDetail
        viewModel.poseFramePassthroughSubject?.receive(on: RunLoop.main).sink(receiveValue: { poseFrame in
            self.renderPoseFrame(poseFrame)
        }).store(in: &cancellables)
        
        viewModel.passCurrentFramePassthoughtSubject?.receive(on: RunLoop.main).sink(receiveValue: { poseFrame in
            self.renderPoseFrame(poseFrame)
            self.passLabel.isHidden = false
        }).store(in: &cancellables)
        
        viewModel.trainFramePassthroughSubject?.sink(receiveValue: { danceFrame in
            self.scene.updateModel(image: UIImage(contentsOfFile: danceFrame.coverCacheURL().path)!)
        }).store(in: &cancellables)
    }
    
    // MARK: - IBAction

    @IBAction func trainButtonDidTap(_ sender: Any) {
        viewModel.startTrain()
    }
    
    @IBAction func backButtonDidtap(_ sender: Any) {
        coordinator.stop()
    }
    
    @IBAction func trainNextFrame(_ sender: Any) {
        viewModel.trainNextFrame()
        passLabel.isHidden = true
    }
    
    // MARK: - Helper
    private func renderPoseFrame(_ poseFrame: PoseFrame) {
        let renderNode = poseFrame.imageNode
        var topLeft: Vertex!
        var botLeft: Vertex!
        var topRight: Vertex!
        var botRight: Vertex!
        guard let camera = viewModel.camera else {
            return
        }
        let expectSize = mtkView.frame.size
        if camera.size.width / camera.size.height > expectSize.width / expectSize.height {
            topLeft = Vertex(x: -1, y: 1, z: 0.95, s: Float(cutOffX) / 2, t: 0)
            botLeft = Vertex(x: -1, y: -1, z: 0.95, s: Float(cutOffX) / 2, t: 1)
            topRight = Vertex(x: 1, y: 1, z: 0.95, s: 1 - Float(cutOffX) / 2, t: 0)
            botRight = Vertex(x: 1, y: -1, z: 0.95, s: 1 - Float(cutOffX) / 2, t: 1)
        } else {
            topLeft = Vertex(x: -1, y: 1, z: 0.95, s: 0, t: Float(cutOffY) / 2)
            botLeft = Vertex(x: -1, y: -1, z: 0.95, s: 0, t: 1 - Float(cutOffY) / 2)
            topRight = Vertex(x: 1, y: 1, z: 0.95, s: 1, t: Float(cutOffY) / 2)
            botRight = Vertex(x: 1, y: -1, z: 0.95, s: 1, t: 1 - Float(cutOffY) / 2)
        }
        
        renderNode.updateVertexs(vertexs: [topLeft, botLeft, topRight , botRight, topRight, botLeft])
        metalRender.backgroundNode = renderNode
        
        metalRender.render(skrenderer: skrenderer)
    }
    
    private func updateCutoff() {
        cutOffX = 0
        cutOffY = 0
        let expectSize = view.frame.size
        guard let camera = viewModel.camera else {
            return
        }
        if camera.size.width / camera.size.height > expectSize.width / expectSize.height {
            let frameSize = CGSize(width: camera.size.height * expectSize.width / expectSize.height, height: camera.size.height)
            cutOffX = CGFloat((camera.size.width - frameSize.width) / camera.size.width)
        } else {
            let frameSize = CGSize(width: camera.size.width, height: camera.size.width * expectSize.height / expectSize.width)
            cutOffY = CGFloat((camera.size.height - frameSize.height) / camera.size.height)
        }
    }
}
