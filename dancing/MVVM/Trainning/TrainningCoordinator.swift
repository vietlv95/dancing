//
//  TrainningCoordinator.swift
//  dancing
//
//  Created by viet on 15/08/2023.
//

import UIKit

class TrainningCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: TrainningVC?
    var navigation: UINavigationController
    var danceDetail: DanceDetail
    
    init(navigation: UINavigationController, danceDetail: DanceDetail) {
        self.navigation = navigation
        self.danceDetail = danceDetail
    }
    
    func start() {
        if !started {
            started = true
            let controller = TrainningVC.factory()
            controller.coordinator = self
            controller.danceDetail = self.danceDetail
            
            navigation.pushViewController(controller, animated: true)
            // Show Controller
            self.controller = controller
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
            controller?.cancellables.removeAll()
            navigation.popViewController(animated: true)
        }
    }
}
