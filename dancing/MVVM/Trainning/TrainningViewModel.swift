//
//  TrainningViewModel.swift
//  dancing
//
//  Created by viet on 15/08/2023.
//

import UIKit
import Combine

protocol TrainningViewModelFactory: BaseViewModelFactory {
    var danceDetail: DanceDetail? { get set}
    var trainFramePassthroughSubject: PassthroughSubject<DanceFrame, Never>? {get set}
    var poseFramePassthroughSubject: PassthroughSubject<PoseFrame, Never>? {get set}
    var passCurrentFramePassthoughtSubject: PassthroughSubject<PoseFrame, Never>? { get set}
    var camera: MetalCamera? {get set}
    
    func startCamera()
    func startTrain()
    func trainNextFrame()
}

class TrainningViewModel: TrainningViewModelFactory {
    var trainFramePassthroughSubject: PassthroughSubject<DanceFrame, Never>? = PassthroughSubject()
    var poseFramePassthroughSubject: PassthroughSubject<PoseFrame, Never>? = PassthroughSubject()
    var passCurrentFramePassthoughtSubject: PassthroughSubject<PoseFrame, Never>? = PassthroughSubject()
    var danceDetail: DanceDetail?
    var camera: MetalCamera?
    
    private var device = MTLCreateSystemDefaultDevice()
    private var commandQueue: MTLCommandQueue!
    private var isShowdingResult = false
    private var trainIndex: Int = -1
    
    init() {
        commandQueue = device?.makeCommandQueue()
    }
    
    func startCamera() {
        camera = MetalCamera(device: device!, commandQueue: commandQueue, mtlDevice: self.device ?? device!, delegate: self)
        _ = camera?.startSession(quality: .hd)
    }
    
    func startTrain() {
        trainNextFrame()
    }
    
    func trainNextFrame() {
        trainIndex += 1
        isShowdingResult = false
        if trainIndex >= danceDetail?.frames.count ?? 0 {
            return
        }
        
        trainFramePassthroughSubject?.send(danceDetail!.frames[trainIndex])
    }
    
    
    func appear() {
        
    }
}

extension TrainningViewModel: MetalCameraDelegate {
    
    private func checkFrameIsPass(poseFrame: PoseFrame, frameIndex: Int) -> Bool {
        if frameIndex < 0 {
            return false
        }
        let score = ScoreRake().caculator(poseFrame: poseFrame, danceFrame: danceDetail!.frames[frameIndex], currentTime: 0, danceDetail: danceDetail!, index: frameIndex)
        if (score?.score ?? 0) > 0 {
            return true
        }
        return false
    }
    
    func metalCamera(_ camera: MetalCamera, didCapturePoseFrame poseFrame: PoseFrame) {
        if isShowdingResult {
            return
        }
        
        poseFramePassthroughSubject?.send(poseFrame)
        if poseFrame.poses.isEmpty {
            return
        }
        let isPass = checkFrameIsPass(poseFrame: poseFrame, frameIndex: trainIndex)
        
        if isPass {
            isShowdingResult = true
            passCurrentFramePassthoughtSubject?.send(poseFrame)
        } else {
            poseFramePassthroughSubject?.send(poseFrame)
        }
        
    }
}
