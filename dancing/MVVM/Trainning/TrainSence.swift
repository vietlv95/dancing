//
//  TrainSence.swift
//  dancing
//
//  Created by viet on 15/08/2023.
//

import Foundation
import SpriteKit

class TrainSence: SKScene {
    private var modelNode: SKSpriteNode!
    
    func updateModel(image: UIImage) {
        updateModelNode(modelImage: image)
    }
    
    private func updateModelNode(modelImage: UIImage) {
        modelNode?.removeFromParent()
        modelNode = SKSpriteNode.init(texture: SKTexture(image: modelImage))
        addChild(modelNode)
        modelNode.anchorPoint = .zero
        modelNode.size = CGSizeMake(279, 279)
        modelNode.position = CGPointMake(23, 65)
    }
}
