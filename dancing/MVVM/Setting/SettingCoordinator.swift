//
//  SettingCoordinator.swift
//  dancing
//
//  Created by viet on 03/08/2023.
//

import UIKit

class SettingCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: SettingVC?
    var preseting: UIViewController
    weak var delegate: SettingVCDelegate?
    
    init(preseting: UIViewController, delegate: SettingVCDelegate?) {
        self.preseting = preseting
        self.delegate = delegate
    }
    
    func start() {
        if !started {
            started = true
            let controller = SettingVC.factory()
            controller.modalTransitionStyle = .crossDissolve
            controller.modalPresentationStyle = .overFullScreen
            controller.coordinator = self
            controller.delegate = delegate
            preseting.present(controller, animated: true)
            // Show Controller
            self.controller = controller
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
            controller?.cancellables.removeAll()
            controller?.dismiss(animated: true, completion: completion)
        }
    }
}
