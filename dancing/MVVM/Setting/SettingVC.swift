//
//  SettingVC.swift
//  dancing
//
//  Created by viet on 03/08/2023.
//

import UIKit
import GoogleSignIn

protocol SettingVCDelegate: AnyObject {
    func settingVCDidTapLogout(_ settingVC: SettingVC)
}

class SettingVC: BaseViewController<SettingViewModelFactory> {
    var coordinator: SettingCoordinator!
    weak var delegate: SettingVCDelegate?
    // MARK: - IBOutlet
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var mucsicLineProgressBar: LineProgressBar!
    @IBOutlet weak var settingMusicInteractionView: UIView!
    
    @IBOutlet weak var soundLineProgressBar: LineProgressBar!
    @IBOutlet weak var settingSoundInteractionView: DimableView!
    
    @IBOutlet weak var languageButton: UIButton!
    
    @IBOutlet weak var contactUsButton: UIButton!
    
    
    @IBOutlet weak var rateUsButton: UIButton!
    
    @IBOutlet weak var logOutButton: UIButton!
    
    @IBOutlet weak var aboutUsButton: UIButton!
    @IBOutlet weak var versionLabel: UILabel!
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }

    // MARK: - Config
    func config() {
        view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        containerView.cornerRadius = 16
        containerView.borderColor = UIColor(rgb: 0x915E8F)
        containerView.borderWidth = 2
        
        languageButton.setBorder(color: .black, width: 1)
        languageButton.cornerRadius = 13
        contactUsButton.setBorder(color: .black, width: 1)
        contactUsButton.cornerRadius = 16
        rateUsButton.setBorder(color: .black, width: 1)
        rateUsButton.cornerRadius = 16
        logOutButton.setBorder(color: .black, width: 1)
        logOutButton.cornerRadius = 16
        
        let attributedText = NSMutableAttributedString(string: "About us", attributes: [
            .foregroundColor: UIColor.black,
            .font : UIFont(name: "Poppins-Light", size: 11)!,
            .underlineColor: UIColor.black,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ])
        aboutUsButton.setAttributedTitle(attributedText, for: .normal)
        
        mucsicLineProgressBar.progress = CGFloat(AppState.shared.settingMusicValue)
        soundLineProgressBar.progress = CGFloat(AppState.shared.settingSoundValue)
        
        versionLabel.text = "ver \(AppState.shared.appVersion)"
        
        addGesture()
    }
    
    func bindViewModel() {
        
    }
    
    // MARK: - IBAction
    @IBAction func languageButtonDidtap(_ sender: Any) {
        showAlert(withTitle: "", message: "Comming Soon")
    }
    
    @IBAction func contactUsButtonDidTap(_ sender: Any) {
        //TODO
    }
    
    @IBAction func rateUsButtonDidTap(_ sender: Any) {
        //TODO
    }
    
    @IBAction func logOutButtonDidTap(_ sender: Any) {
        GIDSignIn.sharedInstance.signOut()
        if let userID = AppState.shared.userInfo?.id {
            API.shared.signOut(userID: userID)
        }
        AppState.shared.userInfo = nil
        delegate?.settingVCDidTapLogout(self)
    }
    
    @IBAction func aboutUsButtonDidTap(_ sender: Any) {
        //TODO
    }
    
    
    // MARK: - Helper
    
    private func addGesture() {
        let musicProgressTapGesture = UITapGestureRecognizer(target: self, action: #selector(musicInterationViewTapGestureAction(_:)))
        settingMusicInteractionView.addGestureRecognizer(musicProgressTapGesture)
        
        let soundProgressTapGesture = UITapGestureRecognizer(target: self, action: #selector(soundInterationViewTapGestureAction(_:)))
        settingSoundInteractionView.addGestureRecognizer(soundProgressTapGesture)
        
        addDismissTouch()
    }
    
    
    
    private func addDismissTouch() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(dismissTapGestureAction(_:)))
        view.addGestureRecognizer(gesture)
        
        let blockGesture = UITapGestureRecognizer(target: self, action: #selector(blockDismissTapGestureAction(_:)))
        containerView.addGestureRecognizer(blockGesture)
    }
    
    @objc func dismissTapGestureAction(_ gesture: UITapGestureRecognizer) {
        self.coordinator.stop()
    }
    
    @objc func blockDismissTapGestureAction(_ gesture: UITapGestureRecognizer) {
        print(#function)
    }
    
    @objc func musicInterationViewTapGestureAction(_ gesture: UITapGestureRecognizer) {
        let view = gesture.view!
        let location = gesture.location(in: view)
        let progress = location.x / view.frame.width
        AppState.shared.settingMusicValue = Float(progress)
        mucsicLineProgressBar.progress = progress
    }
    
    @objc func soundInterationViewTapGestureAction(_ gesture: UITapGestureRecognizer) {
        let view = gesture.view!
        let location = gesture.location(in: view)
        let progress = location.x / view.frame.width
        AppState.shared.settingSoundValue = Float(progress)
        soundLineProgressBar.progress = progress
    }
}
