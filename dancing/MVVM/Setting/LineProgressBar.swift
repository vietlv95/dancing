//
//  LineProgressBar.swift
//  dancing
//
//  Created by viet on 03/08/2023.
//

import UIKit

class LineProgressBar: UIView {
    var progress: CGFloat = 1 {
        didSet {
            updateFrame()
        }
    }
    
    var gradientView: UIView!
    var gradientLayer: CAGradientLayer?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        customInit()
    }

    private func customInit() {
        backgroundColor = UIColor(rgb: 0xD9D9D9)
        
        gradientView = UIView()
        addSubview(gradientView)
        gradientView.fitSuperviewConstraint()
        
        gradientLayer = CAGradientLayer()
        gradientView.frame = CGRect.init(origin: .zero, size: CGSize(width: frame.width * progress, height: frame.height))
        gradientView.layer.addSublayer(gradientLayer!)
        gradientLayer?.startPoint = CGPoint(x: 0, y: 0.5)
        gradientLayer?.endPoint = CGPoint(x: 1, y: 0.5)
        gradientLayer?.locations = [0, 0.5, 1]
        gradientLayer?.colors = [UIColor(rgb: 0x7B915E).cgColor, UIColor(rgb: 0x84E58E).cgColor, UIColor(rgb: 0x57F2BA).cgColor]
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateFrame()
    }
    
    private func updateFrame() {
        cornerRadius = frame.height / 2
        gradientView.cornerRadius = frame.height / 2
        gradientView.frame = CGRect.init(origin: .zero, size: CGSize(width: frame.width * progress, height: frame.height))
        gradientLayer?.frame = CGRect.init(origin: .zero, size: CGSize(width: frame.width * progress, height: frame.height))
    }
}
