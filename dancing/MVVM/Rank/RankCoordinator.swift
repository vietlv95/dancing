//
//  RankCoordinator.swift
//  dancing
//
//  Created by viet on 01/08/2023.
//

import UIKit

class RankCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: RankVC?
    var navigation: UINavigationController
    var dance: Dance
    
    init(navigation: UINavigationController, dance: Dance) {
        self.navigation = navigation
        self.dance = dance
    }
    
    func start() {
        if !started {
            started = true
            let controller = RankVC.factory()
            controller.coordinator = self
            controller.dance = dance
            navigation.pushViewController(controller, animated: true)
            // Show Controller
            self.controller = controller
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
            controller?.cancellables.removeAll()
            navigation.popViewController(animated: true)
        }
    }
}
