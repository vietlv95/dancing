//
//  RankCell.swift
//  dancing
//
//  Created by viet on 02/08/2023.
//

import UIKit
import THLabel
class RankCell: UICollectionViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var rankLabel: THLabel!
    @IBOutlet weak var userNameLabel: THLabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var scoreLabel: THLabel!
    
    @IBOutlet weak var playImageView: UIImageView!
    
    var gradientLayer: CAGradientLayer?
    
    func startUpdateCell() {
        self.layoutIfNeeded()
    }
    
    var avatarBoderColor: UIColor = UIColor(rgb: 0xF29857){
        didSet {
            updateAvatarMask()
        }
    }
    
    func setBackgroundColors(_ colors: [UIColor], locations: [NSNumber], startPoint: CGPoint, endPoint: CGPoint, alpha: CGFloat) {
        if gradientLayer == nil {
            gradientLayer = CAGradientLayer()
            containerView.layer.insertSublayer(gradientLayer!, at: 0)
        }
        gradientLayer?.frame = containerView.bounds
        gradientLayer?.colors = colors.map({ color in
            color.withAlphaComponent(alpha).cgColor
        })
        gradientLayer?.locations = locations
        gradientLayer?.startPoint = startPoint
        gradientLayer?.endPoint = endPoint
    }
    
    
    func updateMask() {
        
        let containerSize = containerView.frame.size
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: containerSize.width - 18, y: 0))
        path.addLine(to: CGPoint(x: containerSize.width , y: containerSize.height / 2))
        path.addLine(to: CGPoint(x: containerSize.width - 18, y: containerSize.height))
        path.addLine(to: CGPoint(x: 0, y: containerSize.height))
        path.addLine(to: .zero)
        let mask = CAShapeLayer()
        mask.frame = containerView.bounds
        mask.path = path.cgPath
        containerView.layer.mask = mask
        
        containerView.shadowColor = UIColor.black
        containerView.shadowOffset = CGSize(width: 0, height: 4)
        containerView.shadowRadius = 4
        containerView.shadowOpacity = 0.25
    }
    
    func setMatch(_ match: Match, rankNum: Int) {
        self.rankLabel.text = "\(rankNum)"
        self.userNameLabel.text = match.user.name
        playImageView.isHidden = match.video.isEmpty
        self.scoreLabel.text = "\(match.score)"
        self.avatarImageView.sd_setImage(with: URL(string: match.user.avatar)) { image, _, _, _ in
            if image == nil {
                self.avatarImageView.image = UIImage(named: "ic_avatar")
            }
        }
        avatarImageView.backgroundColor = match.user.avatar.isEmpty ? .darkGray : .clear
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        rankLabel.gradientColors = [UIColor(rgb: 0xF29857), UIColor(rgb: 0xE58484), UIColor(rgb: 0x915E8F)]
        rankLabel.gradientStartPoint = CGPoint(x: 0.5, y: 0)
        rankLabel.gradientEndPoint = CGPoint(x: 0.5, y: 1)
        rankLabel.strokeColor = .white
        rankLabel.strokeSize = 1
        
        userNameLabel.strokeColor = UIColor(rgb: 0xC65E70)
        userNameLabel.strokeSize = 1
        
        scoreLabel.strokeColor = UIColor(rgb: 0x915E8F)
        scoreLabel.strokeSize = 1
        
//        containerView.layer.masksToBounds = false
        containerView.superview?.shadowColor = UIColor.black.withAlphaComponent(0.25)
        containerView.superview?.shadowOffset = CGSize(width: 0, height: 2)
        containerView.superview?.shadowRadius = 2
        containerView.superview?.shadowOpacity = 1
    }

    
    private func updateAvatarMask() {
        let avatarSize = avatarImageView.frame.size
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: avatarSize.height / 2))
        path.addLine(to: CGPoint(x: avatarSize.width / 4, y: 0))
        path.addLine(to: CGPoint(x: avatarSize.width / 4 * 3, y: 0))
        path.addLine(to: CGPoint(x: avatarSize.width - 0, y: avatarSize.height / 2))
        path.addLine(to: CGPoint(x: avatarSize.width / 4 * 3, y: avatarSize.height - 0))
        path.addLine(to: CGPoint(x: avatarSize.width / 4, y: avatarSize.height - 0))
        path.addLine(to: CGPoint(x: 0, y: avatarSize.height / 2))

        let mask = CAShapeLayer()
        mask.frame = avatarImageView.bounds
        mask.path = path.cgPath
        avatarImageView.layer.mask = mask
        
        if avatarImageView.layer.sublayers?.isEmpty ?? true {
            let borderLayer = CAShapeLayer()
            borderLayer.path = path.cgPath
            borderLayer.lineWidth = 1
            borderLayer.strokeColor = avatarBoderColor.cgColor
            borderLayer.fillColor = UIColor.clear.cgColor
            borderLayer.frame = avatarImageView.bounds
            avatarImageView.layer.addSublayer(borderLayer)
        }
        
    }
}
