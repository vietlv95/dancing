//
//  RankVC.swift
//  dancing
//
//  Created by viet on 01/08/2023.
//

import UIKit
import ProgressHUD

class RankVC: BaseViewController<RankViewModelFactory> {
    var coordinator: RankCoordinator!
    var dance: Dance!
    // MARK: - IBOutlet
    
    @IBOutlet weak var backButton: DimableView!
    @IBOutlet weak var thumbnailImageContainerView: UIView!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var numPlayerLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Variables
    private var matchs: [Match] = []
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
        viewModel.appear()
        Logger.logEvent(.showLeaderBoard)
    }
    
    override func viewDidFirstLayoutSubView() {
        super.viewDidFirstLayoutSubView()
        self.view.layoutIfNeeded()
        self.collectionView.reloadData()
    }

    // MARK: - Config
    func config() {
        configDanceInfo()
        configCollectionView()
    }
    
    private func configCollectionView() {
        collectionView.registerCell(type: RankCell.self)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    private func configDanceInfo() {
        thumbnailImageView.sd_setImage(with: URL(string: dance.audioCover))
        titleLabel.text = dance.title
        numPlayerLabel.text = "\(dance.numPlayer) players"
        
        thumbnailImageView.cornerRadius = 4
        thumbnailImageContainerView.shadowColor = .black
        thumbnailImageContainerView.shadowOpacity = 0.25
        thumbnailImageContainerView.shadowRadius = 4
        thumbnailImageContainerView.shadowOffset = CGSize(width: 2, height: 1)
        
        titleLabel.layer.shadowColor = UIColor.black.cgColor
        titleLabel.shadowOpacity = 0.25
        titleLabel.shadowRadius = 4
        titleLabel.layer.shadowOffset = CGSize(width: 0, height: 4)
    }
    
    func bindViewModel() {
        viewModel.dance = dance
        
        ProgressHUD.show()
        viewModel.matchPassthroughSubject?.sink(receiveValue: { matchs in
            self.matchs = matchs
            self.collectionView.reloadData()
            ProgressHUD.dismiss()
        }).store(in: &cancellables)
    }
    
    // MARK: - IBAction

    @IBAction func backButtonDidTap(_ sender: Any) {
        coordinator.stop()
    }
    
    // MARK: - Helper
}

extension RankVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return matchs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueCell(type: RankCell.self, indexPath: indexPath) else { return UICollectionViewCell()}
        cell.startUpdateCell()
        if indexPath.item % 2 == 0 {
            cell.setBackgroundColors([UIColor(rgb: 0x79B839), UIColor(rgb: 0xB7AE4B)], locations: [0, 1], startPoint: CGPoint(x: 0, y: 0.5), endPoint: CGPoint(x: 1, y: 0.5), alpha: 0.8)
            cell.avatarBoderColor = UIColor(rgb: 0xF29857)
        } else {
            cell.setBackgroundColors([UIColor(rgb: 0x7B915E), UIColor(rgb: 0x84E58E), UIColor(rgb: 0x57F2BA)], locations: [0, 0.5, 1], startPoint: .zero, endPoint: CGPoint(x: 1, y: 1), alpha: 1)
            cell.avatarBoderColor = .white
        }
        cell.updateMask()
        cell.setMatch(matchs[indexPath.item], rankNum: indexPath.item + 1)
        return cell
    }
}

extension RankVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 13
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 65)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !matchs[indexPath.item].video.isEmpty {
            if let videoURL = URL(string: matchs[indexPath.item].video) {
                let coordinator = PreviewVideoCoordinator(presenting: self, videoURL: videoURL, delegate: self, videoGravity: .resizeAspect)
                coordinator.start()
                backButton.isHidden = true
                
                Logger.logEvent(.previewVideo)
            }
        }
    }
}

extension RankVC: PreviewVideoVCDelegate {
    func previewVideoDidTapDeleteButton(_ vc: PreviewVideoVC) {
        
    }
    
    func previewVideoDidTapShareButton(_ vc: PreviewVideoVC) {
        
    }
    
    func previewVideoDidDismiss(_ vc: PreviewVideoVC) {
        backButton.isHidden = false
    }
}
