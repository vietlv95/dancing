//
//  RankViewModel.swift
//  dancing
//
//  Created by viet on 01/08/2023.
//

import UIKit
import Combine

protocol RankViewModelFactory: BaseViewModelFactory {
    var dance: Dance? {get set}
    var matchPassthroughSubject: PassthroughSubject<[Match], Never>? {get set}
}

class RankViewModel: RankViewModelFactory {
    var matchPassthroughSubject: PassthroughSubject<[Match], Never>? = PassthroughSubject()
    var dance: Dance?
    
    func appear() {
        guard let dance = dance else {
            return
        }
        API.shared.getTopMatch(danceID: Int(dance.id)!, userID: -1) { matchs in
            DispatchQueue.main.async {
                self.matchPassthroughSubject?.send(matchs)
            }
        }
    }
}
