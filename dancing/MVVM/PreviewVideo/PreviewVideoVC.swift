//
//  PreviewVideoVC.swift
//  dancing
//
//  Created by viet on 03/08/2023.
//

import UIKit
protocol PreviewVideoVCDelegate: NSObject {
    func previewVideoDidDismiss(_ vc: PreviewVideoVC)
    func previewVideoDidTapDeleteButton(_ vc: PreviewVideoVC)
    func previewVideoDidTapShareButton(_ vc: PreviewVideoVC)
}

class PreviewVideoVC: BaseViewController<PreviewVideoViewModelFactory> {
    var coordinator: PreviewVideoCoordinator!
    var videoURL: URL!
    var canShare: Bool = false
    var canDelete: Bool = false
    var videoGravity: AVLayerVideoGravity!
    weak var delegate: PreviewVideoVCDelegate?
    // MARK: - IBOutlet
    
    @IBOutlet weak var deleteButton: DimableView!
    @IBOutlet weak var shareButton: DimableView!
    @IBOutlet weak var previewView: UIView!
    
    // MARK: - Variable
    private var player: AVPlayer?
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }

    // MARK: - Config
    func config() {
        shareButton.isHidden = !canShare
        deleteButton.isHidden = !canDelete
        view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        self.player = AVPlayer(url: videoURL)
        let playVideoVC = PlayVideoVC()
        playVideoVC.videoGravity = videoGravity
        addChild(playVideoVC)
        previewView.addSubview(playVideoVC.view)
        playVideoVC.view.fitSuperviewConstraint()
        playVideoVC.player = self.player!
    }
    
    func bindViewModel() {
        
    }
    
    // MARK: - IBAction
    @IBAction func closeButtonDidTap(_ sender: Any) {
        coordinator.stop()
        delegate?.previewVideoDidDismiss(self)
    }
    
    @IBAction func shareButtonDidTap(_ sender: Any) {
        delegate?.previewVideoDidTapShareButton(self)
    }
    
    @IBAction func deleteButtonDidTap(_ sender: Any) {
        delegate?.previewVideoDidTapDeleteButton(self)
        delegate?.previewVideoDidDismiss(self)
        coordinator.stop()
    }
    
    // MARK: - Helper
}
