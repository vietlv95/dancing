//
//  PreviewVideoCoordinator.swift
//  dancing
//
//  Created by viet on 03/08/2023.
//

import UIKit

class PreviewVideoCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: PreviewVideoVC?
    var presenting: UIViewController
    var videoURL: URL
    weak var delegate: PreviewVideoVCDelegate?
    var canShare: Bool
    var canDelete: Bool
    var videoGravity: AVLayerVideoGravity
    
    init(presenting: UIViewController, videoURL: URL, canShare: Bool = false, canDelete: Bool = false, delegate: PreviewVideoVCDelegate? = nil, videoGravity: AVLayerVideoGravity = .resizeAspect) {
        self.presenting = presenting
        self.videoURL = videoURL
        self.delegate = delegate
        self.canShare = canShare
        self.canDelete = canDelete
        self.videoGravity = videoGravity
    }
    
    func start() {
        if !started {
            started = true
            let controller = PreviewVideoVC.factory()
            controller.coordinator = self
            controller.videoURL = videoURL
            controller.delegate = delegate
            controller.canShare = canShare
            controller.canDelete = canDelete
            controller.videoGravity = videoGravity
            controller.modalTransitionStyle = .crossDissolve
            controller.modalPresentationStyle = .overFullScreen
            presenting.present(controller, animated: true)
            // Show Controller
            self.controller = controller
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
            controller?.cancellables.removeAll()
            self.controller?.dismiss(animated: true, completion: completion)
        }
    }
}
