//
//  PreviewVideoViewModel.swift
//  dancing
//
//  Created by viet on 03/08/2023.
//

import UIKit

protocol PreviewVideoViewModelFactory: BaseViewModelFactory {
    
}

class PreviewVideoViewModel: PreviewVideoViewModelFactory {
    
    func appear() {
        
    }
}
