//
//  GameResultViewModel.swift
//  dancing
//
//  Created by viet on 01/08/2023.
//

import UIKit
import Photos
import AVFoundation

protocol GameResultViewModelFactory: BaseViewModelFactory {
    func saveVideo(asset: AVURLAsset, completion: ((Bool) -> Void)?)
    func setController(_ controller: UIViewController)
}

class GameResultViewModel: GameResultViewModelFactory {
    private weak var controller: UIViewController?
    
    func appear() {
        
    }
    
    func setController(_ controller: UIViewController) {
        self.controller = controller
    }
    
    func saveVideo(asset: AVURLAsset, completion: ((Bool) -> Void)?) {
        func save(collection: PHAssetCollection) {
            PHPhotoLibrary.shared().performChanges {
                let request = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: asset.url)
                let placeHolder = request?.placeholderForCreatedAsset
                _ = placeHolder?.localIdentifier
                let albumChangeRequest = PHAssetCollectionChangeRequest(for: collection)
                albumChangeRequest?.addAssets([placeHolder!] as NSFastEnumeration)
            } completionHandler: { success, _ in
                DispatchQueue.main.async {
                    completion?(success)
                }
            }
        }
        
        if let collection = getAlbum() {
            save(collection: collection)
        } else {
            createPhotoAlbum {
                DispatchQueue.main.async {
                    let collection = self.getAlbum()
                    if collection == nil {
                        self.showRequireAllPermission()
                    } else {
                        save(collection: collection!)
                    }
                }
            }
        }
    }
    
    // MARK: - Helper
    
    private func showRequireAllPermission() {
        let alert = UIAlertController(title: "Opps", message: "Please gave permission to access your photo library.\n\nThank you !", preferredStyle: .alert)
        let action = UIAlertAction.init(title: "OK", style: .default) { _ in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }

            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alert.addAction(action)
        controller?.present(alert, animated: true)
    }
    private func  getAlbum() -> PHAssetCollection? {
        let options = PHFetchOptions()
        options.predicate = NSPredicate.init(format: "title = %@", "AR_Dance")
        let result = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: options)
        return result.firstObject
    }
    
    private func createPhotoAlbum(completion: (()->Void)?) {
        PHPhotoLibrary.shared().performChanges {
            PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: "FaceMask")
        }completionHandler: { _, _ in
            completion?()
        }
    }
}
