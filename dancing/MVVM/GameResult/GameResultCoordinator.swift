//
//  GameResultCoordinator.swift
//  dancing
//
//  Created by viet on 01/08/2023.
//

import UIKit

class GameResultCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: GameResultVC?
    var navigation: UINavigationController!
    var gameResult: GameResult!
    var window: UIWindow!
    weak var delegate: GameResultVCDelegate?
    
    init(window: UIWindow) {
        self.window = window
    }
    
    init(navigation: UINavigationController, gameResult: GameResult, delegate: GameResultVCDelegate?) {
        self.navigation = navigation
        self.gameResult = gameResult
        self.delegate = delegate
    }
    
    func start() {
        if !started {
            started = true
            let controller = GameResultVC.factory()
            controller.coordinator = self
            controller.gameResult = gameResult
            controller.delegate = self.delegate
            navigation.pushViewController(controller, animated: true)
            // Show Controller
            self.controller = controller
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
            controller?.cancellables.removeAll()
            navigation.popViewController(animated: true)
        }
    }
}
