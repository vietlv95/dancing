//
//  GameResultVC.swift
//  dancing
//
//  Created by viet on 01/08/2023.
//

import UIKit
import AVFoundation

protocol GameResultVCDelegate: AnyObject {
    func gameResultVCDidTapReplayButton(_ vc: GameResultVC)
}

class GameResultVC: BaseViewController<GameResultViewModelFactory> {
    
    weak var delegate: GameResultVCDelegate?
    var gameResult: GameResult!
    var coordinator: GameResultCoordinator!
    // MARK: - IBOutlet
    
    @IBOutlet weak var scoreView: UIView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var previewVideoVideo: UIView!
    @IBOutlet weak var shareView: UIView!
    
    // MARK: - Variable
    private var player: AVPlayer?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
        Logger.logEvent(.showEndGame)
    }
    
    override func viewDidFirstLayoutSubView() {
        super.viewDidFirstLayoutSubView()
        self.view.layoutIfNeeded()
        shareView.roundView(coners: [.topLeft, .topRight], radius: 15)
        
        configScoreView()
    }

    // MARK: - Config
    func config() {
        configPlay()
    }
    
    private func configPlay() {
        self.player = AVPlayer(url: gameResult.videoURL)
        let playVideoVC = PlayVideoVC()
        addChild(playVideoVC)
        previewVideoVideo.addSubview(playVideoVC.view)
        playVideoVC.view.fitSuperviewConstraint()
        playVideoVC.player = self.player
    }
    
    private func configScoreView() {
        let scoreViewSize = scoreView.frame.size
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: scoreViewSize.height / 2))
        path.addLine(to: CGPoint(x: 10, y: 0))
        path.addLine(to: CGPoint(x: scoreViewSize.width - 10, y: 0))
        path.addLine(to: CGPoint(x: scoreViewSize.width, y: scoreViewSize.height / 2))
        path.addLine(to: CGPoint(x: scoreViewSize.width - 10, y: scoreViewSize.height))
        path.addLine(to: CGPoint(x: 10, y: scoreViewSize.height - 0))
        path.addLine(to: CGPoint(x: 0, y: scoreViewSize.height / 2))

        let mask = CAShapeLayer()
        mask.frame = CGRect(origin: .zero, size: scoreViewSize)
        mask.path = path.cgPath
        scoreView.layer.mask = mask
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = scoreView.bounds
        gradientLayer.colors = [UIColor(rgb: 0x7B915E).cgColor, UIColor(rgb: 0x84E58E).cgColor, UIColor(rgb: 0x57F2BA).cgColor]
        gradientLayer.locations = [0, 0.5, 1]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
        scoreView.layer.insertSublayer(gradientLayer, at: 0)
        
        scoreLabel.text = "\(gameResult.score)"
    }
    
    func bindViewModel() {
        viewModel.setController(self)
    }
    
    // MARK: - IBAction

    @IBAction func saveButtonDidTap(_ sender: Any) {
        PermissionManager.default.requestPermission(with: PermissionTypePhoto, on: self) {[weak self] isSuccess in
            guard let self = self else {
                return
            }
            if isSuccess {
                self.viewModel.saveVideo(asset: AVURLAsset(url: self.gameResult.videoURL), completion: nil)
            }
        }
        
        Logger.logEvent(.save)
    }
    
    @IBAction func shareButtonDidTap(_ sender: Any) {
        let items = [gameResult.videoURL]
        let activity = UIActivityViewController.init(activityItems: items as [Any], applicationActivities: nil)
        self.present(activity, animated: true)
        Logger.logEvent(.shareFile)
    }
    
    @IBAction func replayButtonDidTap(_ sender: Any) {
        coordinator.stop()
        delegate?.gameResultVCDidTapReplayButton(self)
        Logger.logEvent(.replayGame)
    }
    
    @IBAction func homeButtonDidTap(_ sender: Any) {
        if let scene = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate {
            scene.setRootViewController()
        }
    }
    
    @IBAction func rankButtonDidTap(_ sender: Any) {
        let coordinator = RankCoordinator(navigation: self.navigationController!, dance: gameResult.danceDetail.dance)
        coordinator.start()
    }
    
    // MARK: - Helper
    
    private func playResultVideo() {
        
    }
}
