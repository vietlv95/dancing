//
//  ProfileViewModel.swift
//  dancing
//
//  Created by Viet Le on 03/08/2023.
//

import UIKit
import Combine
import RealmSwift

protocol ProfileViewModelFactory: BaseViewModelFactory {
    var saveDancePassthroughSubject: PassthroughSubject<[SaveDance], Never>? {get set}
    func updateUserName(userName: String)
}

class ProfileViewModel: ProfileViewModelFactory {
    var saveDancePassthroughSubject: PassthroughSubject<[SaveDance], Never>? = PassthroughSubject()
    
    func appear() {
        let realm = try! Realm()
        let rs = realm.objects(SaveDance.self)
        saveDancePassthroughSubject?.send(rs.toArray(ofType: SaveDance.self))
    }
    
    func updateUserName(userName: String) {
        let user = AppState.shared.userInfo!
        API.shared.updateUserInfo(userID: user.id, name: userName, avatarURL: userName)
    }
}

extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for i in 0 ..< count {
            if let result = self[i] as? T {
                array.append(result)
            }
        }

        return array
    }
}
