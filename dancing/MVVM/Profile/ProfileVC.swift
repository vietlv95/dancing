//
//  ProfileVC.swift
//  dancing
//
//  Created by Viet Le on 03/08/2023.
//

import UIKit
import GoogleSignIn
import RealmSwift
import ProgressHUD

class ProfileVC: BaseViewController<ProfileViewModelFactory> {
    var coordinator: ProfileCoordinator!
    // MARK: - IBOutlet
    @IBOutlet weak var fluidityEvaluateView: EvaluateView!
    
    @IBOutlet weak var backButton: DimableView!
    @IBOutlet weak var timingEvaluateView: EvaluateView!
    
    @IBOutlet weak var energyEvaluateView: EvaluateView!
    
    @IBOutlet weak var creativityEvaluateView: EvaluateView!
    
    @IBOutlet weak var accuracyEvaluateView: EvaluateView!
    
    @IBOutlet weak var avatarImageContainerView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var analysisContainerView: UIView!
    @IBOutlet weak var analysisViet: UIView!
    
    @IBOutlet weak var xLabel: UILabel!
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    
    private var selectVideoURL: URL!
    // MARK: - Variables
    private var saveDances: [SaveDance] = []
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
        
    }

    override func viewDidFirstLayoutSubView() {
        super.viewDidFirstLayoutSubView()
        view.layoutIfNeeded()
        
        fluidityEvaluateView.image = UIImage(named: "ic_profile_fluidity")
        fluidityEvaluateView.score = 1
        fluidityEvaluateView.typeColor = UIColor(rgb: 0xF97FFE)
        fluidityEvaluateView.initLayout()
        
        timingEvaluateView.image = UIImage(named: "ic_profile_timing")
        timingEvaluateView.score = 2
        timingEvaluateView.typeColor = UIColor(rgb: 0x2DDBF3)
        timingEvaluateView.initLayout()
        
        accuracyEvaluateView.image = UIImage(named: "ic_profile_accuracy")
        accuracyEvaluateView.score = 3
        accuracyEvaluateView.typeColor = UIColor(rgb: 0xB0D545)
        accuracyEvaluateView.initLayout()
        
        energyEvaluateView.image = UIImage(named: "ic_profile_energy")
        energyEvaluateView.score = 4
        energyEvaluateView.typeColor = UIColor(rgb: 0xF59E05)
        energyEvaluateView.initLayout()
        
        creativityEvaluateView.image = UIImage(named: "ic_profile_creativity")
        creativityEvaluateView.score = 5
        creativityEvaluateView.typeColor = UIColor(rgb: 0xFEDA09)
        creativityEvaluateView.initLayout()
        
        configAvatarView()
        
        viewModel.appear()
    }
    // MARK: - Config
    func config() {
        configAnalysisView()
        configCollectionView()
        
        if let user = GIDSignIn.sharedInstance.currentUser,  let imgUrl = user.profile?.imageURL(withDimension: 150) {
            self.avatarImageView.sd_setImage(with: imgUrl) { _, _, _, _ in
                self.avatarImageView.backgroundColor = .clear
            }
        } else {
            let img = UIImage(named: "ic_avatar")
            self.avatarImageView.image = img
            self.avatarImageView.backgroundColor = .darkGray
        }
        userNameLabel.text = AppState.shared.userInfo?.name
        scoreLabel.text = "\(AppState.shared.userInfo?.score() ?? 0)"
        rankLabel.text = "\(AppState.shared.userInfo?.rank ?? 0)"
    }
    
    private func configCollectionView() {
        collectionView.registerCell(type: ProfileVideoCell.self)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    private func configAnalysisView() {
        analysisViet.cornerRadius = 10
        analysisContainerView.shadowColor = UIColor.black
        analysisContainerView.shadowOpacity = 0.25
        analysisContainerView.shadowRadius = 4
        analysisContainerView.shadowOffset = CGSize(width: 0, height: 4)
    }
    
    private func configAvatarView() {
        avatarImageView.backgroundColor = .darkGray
        
        let avatarSize = avatarImageView.frame.size
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: avatarSize.height / 2))
        path.addLine(to: CGPoint(x: avatarSize.width / 4, y: 0))
        path.addLine(to: CGPoint(x: avatarSize.width / 4 * 3, y: 0))
        path.addLine(to: CGPoint(x: avatarSize.width - 0, y: avatarSize.height / 2))
        path.addLine(to: CGPoint(x: avatarSize.width / 4 * 3, y: avatarSize.height - 0))
        path.addLine(to: CGPoint(x: avatarSize.width / 4, y: avatarSize.height - 0))
        path.addLine(to: CGPoint(x: 0, y: avatarSize.height / 2))

        let mask = CAShapeLayer()
        mask.frame = avatarImageView.bounds
        mask.path = path.cgPath
        avatarImageView.layer.mask = mask
        
        let borderLayer = CAShapeLayer()
        borderLayer.path = path.cgPath
        borderLayer.lineWidth = 2
        borderLayer.strokeColor = UIColor.white.cgColor
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.frame = avatarImageView.bounds
        avatarImageView.layer.addSublayer(borderLayer)
        
        avatarImageContainerView.shadowColor = UIColor.black
        avatarImageContainerView.shadowOffset = CGSize(width: 0, height: 4)
        avatarImageContainerView.shadowRadius = 4
        avatarImageContainerView.shadowOpacity = 0.25
    }
    
    func bindViewModel() {
        viewModel.saveDancePassthroughSubject?.sink(receiveValue: { saveDances in
            self.saveDances = saveDances
            self.collectionView.reloadData()
        }).store(in: &cancellables)
    }
    
    // MARK: - IBAction

    @IBAction func editNameButtonDidTap(_ sender: Any) {
        let alert = UIAlertController(title: "user name", message: "", preferredStyle: .alert)
        alert.addTextField()
        let updateAction = UIAlertAction(title: "Change", style: .default) { _ in
            let text = alert.textFields?.first?.text ?? ""
            if text.isEmpty {
                alert.dismiss(animated: true) {
                    self.showAlert(withTitle: "Error", message: "user name can't empty")
                }
            } else {
                self.viewModel.updateUserName(userName: text)
                self.userNameLabel.text = text
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        alert.addAction(updateAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true)
    }
    
    @IBAction func backButtonDidTap(_ sender: Any) {
        coordinator.stop()
    }
    
    // MARK: - Helper
}

// MARK: - UICollectionViewDataSource
extension ProfileVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return saveDances.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueCell(type: ProfileVideoCell.self, indexPath: indexPath) else {
            return UICollectionViewCell()
        }
        
        cell.scoreLabel.text = "\(saveDances[indexPath.item].score)"
        let videoURL = URL(fileURLWithPath: CacheManager.shared.highScoreVideoFolder() + "/\(saveDances[indexPath.item].saveVideoName)")
        cell.thumbnailImageView.image = AVAsset(url: videoURL).imageAtTime(CMTime(value: 1, timescale: 1))
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ProfileVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let column = 3
        let w = (Int(collectionView.frame.size.width) - (column - 1) * 2) / column
        let h = w * 158 / 119
        return CGSize(width: w, height: h)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectVideoURL = URL(fileURLWithPath: CacheManager.shared.highScoreVideoFolder() + "/\(saveDances[indexPath.item].saveVideoName)")
        let coordinator = PreviewVideoCoordinator(presenting: self, videoURL: selectVideoURL, canShare: true, delegate: self)
        coordinator.start()
        backButton.isHidden = true
        Logger.logEvent(.previewVideoInProfile)
    }
}

extension ProfileVC: PreviewVideoVCDelegate {
    func previewVideoDidTapDeleteButton(_ vc: PreviewVideoVC) {
        
    }
    
    func previewVideoDidTapShareButton(_ vc: PreviewVideoVC) {
        let items = [self.selectVideoURL]
        let activity = UIActivityViewController.init(activityItems: items as [Any], applicationActivities: nil)
        vc.present(activity, animated: true)
    }
    
    func previewVideoDidDismiss(_ vc: PreviewVideoVC) {
        backButton.isHidden = false
    }
}
