//
//  EvaluateView.swift
//  dancing
//
//  Created by viet on 07/08/2023.
//

import Foundation
import UIKit

class EvaluateView: UIView {
    var image: UIImage!
    
    var score: Int = 1
    
    var typeColor: UIColor = .yellow
    
    private var imageView: UIImageView!
    private var roundView: UIView!
    private var scoreLabel: UILabel!
    private var score_1_View: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        customInit()
    }
    
    func initLayout() {
        roundView = UIView()
        addSubview(roundView)
        roundView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            roundView.topAnchor.constraint(equalTo: topAnchor),
            roundView.leadingAnchor.constraint(equalTo: leadingAnchor),
            roundView.trailingAnchor.constraint(equalTo: trailingAnchor),
            roundView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5)
        ])
        
        imageView = UIImageView()
        addSubview(imageView!)
        imageView?.fitSuperviewConstraint(edgeInset: UIEdgeInsets(top: 5, left: 4, bottom: 7, right: 4))
        imageView.image = image
        
        scoreLabel = UILabel()
        scoreLabel.font = UIFont(name: "Poppins-Bold", size: 8)
        scoreLabel.textAlignment = .center
        scoreLabel.textColor = .white
        scoreLabel.cornerRadius = 2
        scoreLabel.text = "\(score)/5"
        addSubview(scoreLabel)
        scoreLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            scoreLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            scoreLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            scoreLabel.heightAnchor.constraint(equalToConstant: 12),
            scoreLabel.widthAnchor.constraint(equalToConstant: 29)
        ])
        
        layoutIfNeeded()
        initMask()
    }
    
    private func customInit() {
        
    }
    
    private func initMask() {
        let imageSize = imageView?.frame.size ?? .zero
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: imageSize.height / 2))
        path.addLine(to: CGPoint(x: imageSize.width / 4, y: 0))
        path.addLine(to: CGPoint(x: imageSize.width / 4 * 3, y: 0))
        path.addLine(to: CGPoint(x: imageSize.width - 0, y: imageSize.height / 2))
        path.addLine(to: CGPoint(x: imageSize.width / 4 * 3, y: imageSize.height - 0))
        path.addLine(to: CGPoint(x: imageSize.width / 4, y: imageSize.height - 0))
        path.addLine(to: CGPoint(x: 0, y: imageSize.height / 2))
        
        let mask = CAShapeLayer()
        mask.frame = imageView!.bounds
        mask.path = path.cgPath
        imageView!.layer.mask = mask

        let roundSize = roundView.frame.size
        for i in 1...5 {
            let roundPath = UIBezierPath()
            let strokerColor: UIColor! = score >= i ? typeColor : UIColor(rgb: 0xA6A8AB)
            
            switch i {
            case 1:
                roundPath.move(to: CGPoint(x: 2, y: roundSize.height / 2 + 4))
                roundPath.addLine(to: CGPoint(x: roundSize.width / 4 - 2, y: roundSize.height - 2))
            case 2:
                roundPath.move(to: CGPoint(x: 2, y: roundSize.height / 2 - 1))
                roundPath.addLine(to: CGPoint(x: roundSize.width / 4 - 1, y: 4))
            case 3:
                roundPath.move(to: CGPoint(x: roundSize.width / 4 + 4, y: 2))
                roundPath.addLine(to: CGPoint(x: roundSize.width / 4 * 3 - 4, y: 2))
            case 4:
                roundPath.move(to: CGPoint(x: roundSize.width / 4 * 3 + 1, y: 4))
                roundPath.addLine(to: CGPoint(x: roundSize.width - 2, y: roundSize.height / 2 - 1))
            case 5:
                roundPath.move(to: CGPoint(x: roundSize.width - 2, y: roundSize.height / 2 + 4))
                roundPath.addLine(to: CGPoint(x: roundSize.width / 4 * 3 + 2, y: roundSize.height - 2))
                break

            default:
                break
            }
            
            let borderLayer = CAShapeLayer()
            borderLayer.path = roundPath.cgPath
            borderLayer.strokeColor = strokerColor.cgColor
            borderLayer.lineWidth = 4
            borderLayer.lineCap = .round
            roundView.layer.addSublayer(borderLayer)
        }
    }
}
