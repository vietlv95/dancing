//
//  ProfileVideoCell.swift
//  dancing
//
//  Created by viet on 08/08/2023.
//

import UIKit

class ProfileVideoCell: UICollectionViewCell {
    @IBOutlet weak var scoreLabel: UILabel!
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        thumbnailImageView.contentMode = .scaleAspectFill
        // Initialization code
    }
}
