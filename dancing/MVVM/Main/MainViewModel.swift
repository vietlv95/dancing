//
//  MainViewModel.swift
//  dancing
//
//  Created by viet on 18/07/2023.
//

import UIKit
import Combine
import GoogleSignIn
import RealmSwift

protocol MainViewModelFactory: BaseViewModelFactory {
    func login(googleUser: GIDGoogleUser?)
    func loadData()
    var dancePassthroughSubject: PassthroughSubject<[Dance], Never>? {get set}
    var saveDancePassthroughSubject: PassthroughSubject<[SaveDance], Never>? {get set}
    var userInfoPassthroughSubject: PassthroughSubject<UserInfo?, Never>? {get set}
}

class MainViewModel: MainViewModelFactory {
    var currentUser: UserInfo?
    var dancePassthroughSubject: PassthroughSubject<[Dance], Never>?
    var saveDancePassthroughSubject: PassthroughSubject<[SaveDance], Never>? = PassthroughSubject()
    var userInfoPassthroughSubject: PassthroughSubject<UserInfo?, Never>? = PassthroughSubject()
    
    init() {
        dancePassthroughSubject = PassthroughSubject()
    }
    
    func login(googleUser: GIDGoogleUser?) {
        DispatchQueue.global().async {
            if let googleUser = googleUser {
                let userInfo = UserInfo()
                let avatarURL = googleUser.profile?.imageURL(withDimension: 300)?.absoluteString ?? ""
                userInfo.name = googleUser.profile?.name ?? ""
                userInfo.avatar = avatarURL
                API.shared.signin(username: googleUser.userID ?? "", password: googleUser.accessToken.tokenString, userInfo: userInfo, completion: { user in
                    if let user = user {
                        AppState.shared.userInfo = user
                        self.userInfoPassthroughSubject?.send(user)
                        self.currentUser = user
                        self.loadData()
                        self.uploadUserInfoIfNeed(user: user, googleUser: googleUser)
                    }
                })
            } else {
                API.shared.signin(username: UIDevice.current.identifierForVendor?.uuidString ?? "", password: UUID().uuidString, userInfo: UserInfo()) { user in
                    if let user = user {
                        AppState.shared.userInfo = user
                        self.userInfoPassthroughSubject?.send(user)
                        self.currentUser = user
                        self.loadData()
                    }
                }
            }
        }
    }
    
    func loadData() {
        let id = currentUser?.id ?? -1//        id = "-1"
        API.shared.getListDance(userID: id) { dances, status in
            self.dancePassthroughSubject?.send(dances)
        }
        
        let realm = try! Realm()
        let rs = realm.objects(SaveDance.self)
        saveDancePassthroughSubject?.send(rs.toArray(ofType: SaveDance.self))
    }
    
    private func uploadUserInfoIfNeed(user: UserInfo, googleUser: GIDGoogleUser) {
        DispatchQueue.global().async {
            if let url = googleUser.profile?.imageURL(withDimension: 200)?.absoluteString {
                let name = googleUser.profile?.name ?? ""
                if user.avatar.isEmpty {
                    API.shared.updateUserInfo(userID: user.id, name: name, avatarURL: url)
                }
            }
        }
    }
    
    func appear() {
        
    }
}
