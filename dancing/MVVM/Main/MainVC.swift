//
//  MainVC.swift
//  dancing
//
//  Created by viet on 18/07/2023.
//

import UIKit
import GoogleSignIn
import SDWebImage
import ProgressHUD

class MainVC: BaseViewController<MainViewModelFactory> {
    var coordinator: MainCoordinator!
    // MARK: - IBOutlet
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var profileBgView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    private var saveDances: [SaveDance] = []
    
    // MARK: - Variables
    var dances: [Dance] = []
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
        GIDSignIn.sharedInstance.restorePreviousSignIn { rs, _ in
            self.loadUserInfo()
        }
    }
    
    override func viewDidFirstLayoutSubView() {
        super.viewDidFirstLayoutSubView()
        self.view.layoutIfNeeded()
        collectionView.reloadData()
    }
    
    
    override func viewDidFirstAppear() {
        super.viewDidFirstAppear()
        configProfile()
        ProgressHUD.show()
    }

    // MARK: - Config
    func config() {
        configCollectionView()
    }
    
    func configCollectionView() {
        collectionView.registerCell(type: DanceInfoCell.self)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    func configProfile() {
        avatarImageView.backgroundColor = .darkGray
        
        let avatarSize = avatarImageView.frame.size
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: avatarSize.height / 2))
        path.addLine(to: CGPoint(x: avatarSize.width / 4, y: 0))
        path.addLine(to: CGPoint(x: avatarSize.width / 4 * 3, y: 0))
        path.addLine(to: CGPoint(x: avatarSize.width - 0, y: avatarSize.height / 2))
        path.addLine(to: CGPoint(x: avatarSize.width / 4 * 3, y: avatarSize.height - 0))
        path.addLine(to: CGPoint(x: avatarSize.width / 4, y: avatarSize.height - 0))
        path.addLine(to: CGPoint(x: 0, y: avatarSize.height / 2))

        let mask = CAShapeLayer()
        mask.frame = avatarImageView.bounds
        mask.path = path.cgPath
        avatarImageView.layer.mask = mask
        
        let borderLayer = CAShapeLayer()
        borderLayer.path = path.cgPath
        borderLayer.lineWidth = 2
        borderLayer.strokeColor = UIColor(rgb: 0x00FF47).cgColor
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.frame = avatarImageView.bounds
        avatarImageView.layer.addSublayer(borderLayer)
        
        profileBgView.alpha = 0.8
        let gradientLayer = CAGradientLayer()
        gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
        gradientLayer.colors = [UIColor.white.cgColor, UIColor(rgb: 0xD9D9D9).withAlphaComponent(0).cgColor]
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = profileBgView.bounds
        profileBgView.layer.addSublayer(gradientLayer)
    }
    
    func bindViewModel() {
        viewModel.dancePassthroughSubject?.sink(receiveValue: { dances in
            self.dances = dances
            self.collectionView.reloadData()
            ProgressHUD.dismiss()
        }).store(in: &cancellables)
        
        viewModel.saveDancePassthroughSubject?.sink(receiveValue: { saveDances in
            self.saveDances = saveDances
            self.scoreLabel.text = "\(AppState.shared.userInfo?.score() ?? 0)"
        }).store(in: &cancellables)
    }
    
    // MARK: - IBAction

    @IBAction func settingButtonDidTap(_ sender: Any) {
        let coordinator = SettingCoordinator(preseting: self, delegate: self)
        coordinator.start()
    }
    
    @IBAction func profileButtonDidTap(_ sender: Any) {
        if GIDSignIn.sharedInstance.currentUser != nil {
            let profileCoordinator = ProfileCoordinator(navigation: self.navigationController!)
            profileCoordinator.start()
            Logger.logEvent(.openProfile)
            return
        }
        
        GIDSignIn.sharedInstance.signIn(withPresenting: self) { result, err in
            self.loadUserInfo()
        }
    }
    
    @IBAction func uploadedButtonDidTap(_ sender: Any) {
        let coordinator = UploadedCoordinator(navigation: self.navigationController!)
        coordinator.start()
    }
    // MARK: - Helper
    
    private func loadUserInfo() {
        if let user = GIDSignIn.sharedInstance.currentUser {
            if let imgUrl = user.profile?.imageURL(withDimension: 150) {
                self.avatarImageView.sd_setImage(with: imgUrl) { _, _, _, _ in
                    self.avatarImageView.backgroundColor = .clear
                }
            }
            viewModel.login(googleUser: user)
        } else {
            self.avatarImageView.image = UIImage(named: "ic_avatar")
            self.avatarImageView.backgroundColor = .gray
            viewModel.login(googleUser: nil)
        }
    }
}

// MARK: - UICollectionViewDataSource
extension MainVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dances.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueCell(type: DanceInfoCell.self, indexPath: indexPath) else {
            return UICollectionViewCell()
        }
        cell.bind(dance: dances[indexPath.row], saveDance: saveDances.first(where: {String($0.danceID) == dances[indexPath.row].id}))
        cell.configBg()
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension MainVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 64)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 24
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let coordinator = DanceInfoCoordinator(presenting: self, dance: dances[indexPath.row])
        coordinator.start()
    }
}

extension MainVC: SettingVCDelegate {
    func settingVCDidTapLogout(_ settingVC: SettingVC) {
        self.loadUserInfo()
    }
}
