//
//  DanceInfoCell.swift
//  dancing
//
//  Created by viet on 18/07/2023.
//

import UIKit

class DanceInfoCell: UICollectionViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var icPlayImageView: UIImageView!
    @IBOutlet weak var thumbnailContainerView: UIView!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.masksToBounds = false
        contentView.layer.masksToBounds = false
        contentView.clipsToBounds = false
        containerView.layer.masksToBounds = false
        
        icPlayImageView.shadowColor = UIColor.black
        icPlayImageView.shadowOpacity = 0.25
        icPlayImageView.shadowOffset = CGSize(width: 0, height: 4)
        icPlayImageView.shadowRadius = 4
        
        thumbnailContainerView.layer.masksToBounds = false
//        thumbnailContainerView.shadowColor = UIColor.black
//        thumbnailContainerView.shadowOpacity = 0.25
//        thumbnailContainerView.shadowOffset = CGSize(width:2, height: 1)
//        thumbnailContainerView.shadowRadius = 4
        
        thumbnailImageView.cornerRadius = 4
    }
    
    func bind(dance: Dance, saveDance: SaveDance?) {
        self.nameLabel.text = dance.title
        self.thumbnailImageView.sd_setImage(with: URL(string: dance.audioCover))
        self.durationLabel.text = dance.durationTimeFormat()
        self.scoreLabel.text = String(saveDance?.totalScore ?? 0)
    }

    func configBg() {
        self.layoutIfNeeded()
        bgView.layer.backgroundColor = UIColor(rgb: 0x833454).cgColor
        bgView.layer.sublayers?.forEach({$0.removeFromSuperlayer()})
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bgView.bounds
        gradientLayer.startPoint = CGPoint.zero
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        gradientLayer.locations = [0, 0.5, 1]
        gradientLayer.colors = [
            UIColor(rgb: 0x833454).cgColor,
            UIColor(rgb: 0x7576A5).cgColor,
            UIColor(rgb: 0x4A4969).cgColor
        ]
        bgView.layer.addSublayer(gradientLayer)
        
        let containerSize = containerView.frame.size
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: containerSize.width - 18, y: 0))
        path.addLine(to: CGPoint(x: containerSize.width , y: containerSize.height / 2))
        path.addLine(to: CGPoint(x: containerSize.width - 18, y: containerSize.height))
        path.addLine(to: CGPoint(x: 0, y: containerSize.height))
        path.addLine(to: .zero)
        let mask = CAShapeLayer()
        mask.frame = containerView.bounds
        mask.path = path.cgPath
        containerView.layer.mask = mask
    }
}
