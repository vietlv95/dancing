//
//  MainCoordinator.swift
//  dancing
//
//  Created by viet on 18/07/2023.
//

import UIKit

class MainCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: MainVC?
    var window: UIWindow?
    var nav: UINavigationController!
    init(window: UIWindow) {
        self.window = window
        
    }
    
    func start() {
        if !started {
            started = true
            let controller = MainVC.factory()
            controller.coordinator = self
            let nav = BaseUINavigationController(rootViewController: controller)
            nav.isNavigationBarHidden = true
            self.nav = nav
            window?.rootViewController = nav
            // Show Controller
            self.controller = controller
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
            controller?.cancellables.removeAll()
            
        }
    }
}
