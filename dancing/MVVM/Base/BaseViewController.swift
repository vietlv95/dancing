//
//  BaseViewController.swift
//  dmv
//
//  Created by thanh on 25/05/2021.
//

import Foundation
import UIKit
import Combine

class BaseViewController<ViewModelFactory>: UIViewController {
    var viewModel: ViewModelFactory!
    var cancellables = [AnyCancellable]()
    
    static func factory() -> Self {
        let controller = Self()
        controller.injectViewModel()
        return controller
    }
    
    func injectViewModel() {
        viewModel = DIContainer.shared.container.resolve(ViewModelFactory.self)!
    }
    
    private var viewWillAppeared: Bool = false
    private var viewDidAppeared: Bool = false
    private var viewDidLayoutSubView: Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !viewWillAppeared {
            viewWillAppeared = true
            self.viewWillFirstAppear()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if !viewDidAppeared {
            viewDidAppeared = true
            self.viewDidFirstAppear()
        }
    }
    
    override func viewDidLayoutSubviews() {
        if !viewDidLayoutSubView {
            viewDidLayoutSubView = true
            self.view.layoutIfNeeded()
            self.viewDidFirstLayoutSubView()
        }
    }
    
    func viewDidFirstLayoutSubView() {
        
    }

    func viewWillFirstAppear() {

    }

    func viewDidFirstAppear() {

    }
    
    func showAlert(title: String = "", message: String = "", titleButtons: [String] = ["OK"], destructiveIndexs: [Int] = [], action: ((Int) -> Void)? = nil) {
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)

        titleButtons.forEach { (titleButton) in
            let index = titleButtons.firstIndex(of: titleButton)!
            let style = destructiveIndexs.contains(index) ? UIAlertAction.Style.destructive : UIAlertAction.Style.default
            let alertAction = UIAlertAction.init(title: titleButton, style: style, handler: { (_) in
                action?(index)
            })

            alert.addAction(alertAction)
        }

        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}
