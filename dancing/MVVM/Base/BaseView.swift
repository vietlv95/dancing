//
//  BaseView.swift
//  MetalFaceMask
//
//  Created by viet on 04/07/2023.
//

import Foundation
import UIKit

class BaseView: UIView {
    
    private var didFirstLayoutSubView: Bool = false
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if !didFirstLayoutSubView {
            firstLayoutSubView()
            didFirstLayoutSubView = true
        }
    }
    
    func firstLayoutSubView() {
        
    }
}
