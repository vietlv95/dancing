//
//  BaseCoordinator.swift
//  dmv
//
//  Created by thanh on 25/05/2021.
//

import Foundation

protocol BaseCoordinator: AnyObject {
    func start()
}
