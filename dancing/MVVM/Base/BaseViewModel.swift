//
//  BaseViewModel.swift
//  dmv
//
//  Created by thanh on 25/05/2021.
//

import Foundation

protocol BaseViewModelFactory {
    func appear()
}
