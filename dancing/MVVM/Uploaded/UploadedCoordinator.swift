//
//  UploadedCoordinator.swift
//  dancing
//
//  Created by viet on 17/08/2023.
//

import UIKit

class UploadedCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: UploadedVC?
    var navigation: UINavigationController
    
    init(navigation: UINavigationController) {
        self.navigation = navigation
        
    }
    
    func start() {
        if !started {
            started = true
            let controller = UploadedVC.factory()
            controller.coordinator = self
            
            navigation.pushViewController(controller, animated: true)
            // Show Controller
            self.controller = controller
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
            controller?.cancellables.removeAll()
            navigation.popViewController(animated: true)
        }
    }
}
