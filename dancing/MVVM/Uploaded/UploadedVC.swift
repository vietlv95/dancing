//
//  UploadedVC.swift
//  dancing
//
//  Created by viet on 17/08/2023.
//

import UIKit
import ProgressHUD

class UploadedVC: BaseViewController<UploadedViewModelFactory> {
    var coordinator: UploadedCoordinator!
    // MARK: - IBOutlet
    
    @IBOutlet weak var backButton: DimableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var selectVideoIndex: Int = -1
    
    // MARK: - Variables
    private var matchs: [Match] = []
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
        viewModel.appear()
    }

    // MARK: - Config
    func config() {
        configCollectionView()
    }
    
    private func configCollectionView() {
        collectionView.registerCell(type: ProfileVideoCell.self)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    func bindViewModel() {
        ProgressHUD.show()
        viewModel.matchsPassthroughSubject?.sink(receiveValue: { matchs in
            ProgressHUD.dismiss()
            self.matchs = matchs
            self.collectionView.reloadData()
        }).store(in: &cancellables)
        
    }
    
    // MARK: - IBAction

    @IBAction func backButtonDidTap(_ sender: Any) {
        coordinator.stop()
    }
    
    // MARK: - Helper
}

extension UploadedVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.matchs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueCell(type: ProfileVideoCell.self, indexPath: indexPath) else {
            return UICollectionViewCell()
        }
        
        let match = self.matchs[indexPath.item]
        
        cell.scoreLabel.text = "\(match.score)"
        cell.thumbnailImageView.sd_setImage(with: URL(string: match.cover))
        return cell
    }
    
    
}

extension UploadedVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let column = 3
        let w = (Int(collectionView.frame.size.width) - (column - 1) * 2) / column
        let h = w * 158 / 119
        return CGSize(width: w, height: h)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let match = self.matchs[indexPath.item]
        self.backButton.isHidden = true
        
        let previewCoordinator = PreviewVideoCoordinator(presenting: self, videoURL: URL(string: match.video)!, canShare: true, canDelete: true, delegate: self)
        previewCoordinator.start()
        selectVideoIndex = indexPath.item
    }
}

extension UploadedVC: PreviewVideoVCDelegate {
    func previewVideoDidTapDeleteButton(_ vc: PreviewVideoVC) {
        viewModel.deleteMatch(id: matchs[selectVideoIndex].id)
        matchs.remove(at: selectVideoIndex)
        collectionView.reloadData()
    }
    
    func previewVideoDidTapShareButton(_ vc: PreviewVideoVC) {
        let videoURL = URL(string: self.matchs[selectVideoIndex].video)
        let items = [videoURL]
        let activity = UIActivityViewController.init(activityItems: items as [Any], applicationActivities: nil)
        vc.present(activity, animated: true)
    }
    
    func previewVideoDidDismiss(_ vc: PreviewVideoVC) {
        self.backButton.isHidden = false
    }
}
