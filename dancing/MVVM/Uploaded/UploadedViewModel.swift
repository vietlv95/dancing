//
//  UploadedViewModel.swift
//  dancing
//
//  Created by viet on 17/08/2023.
//

import UIKit
import Combine

protocol UploadedViewModelFactory: BaseViewModelFactory {
    var matchsPassthroughSubject: PassthroughSubject<[Match], Never>? {get set}
    func deleteMatch(id: Int)
    func fetchMatch()
}

class UploadedViewModel: UploadedViewModelFactory {
    var matchsPassthroughSubject: PassthroughSubject<[Match], Never>? = PassthroughSubject()
    
    func appear() {
        fetchMatch()
    }
    
    func deleteMatch(id: Int) {
        let userID = AppState.shared.userInfo?.id ?? -1
        API.shared.deleteMatch(userID: userID, matchID: id) { success in
            print(success)
        }
    }
    
    func fetchMatch() {
        API.shared.getListMatch(userID: AppState.shared.userInfo?.id ?? -1) { matchs in
            self.matchsPassthroughSubject?.send(matchs)
        }
    }
}
