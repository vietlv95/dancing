//
//  DanceInfoViewModel.swift
//  dancing
//
//  Created by viet on 19/07/2023.
//

import UIKit
import Alamofire
import Combine

protocol DanceInfoViewModelFactory: BaseViewModelFactory {
    var loadingPassthroughSubject: PassthroughSubject<String, Never>? {get set}
    func getDirectURL(urlString: String, completion: ((_ redirectURLString: String) -> Void)?)
    func loadDetail(_ dance: Dance, completion: ((_ detail: DanceDetail?) -> Void)?)
}

class DanceInfoViewModel: DanceInfoViewModelFactory {
    var loadingPassthroughSubject: PassthroughSubject<String, Never>? = PassthroughSubject()
    
    func getDirectURL(urlString: String, completion: ((_ redirectURLString: String) -> Void)?) {
        let request = (try? URLRequest.init(url: urlString, method: .get))!
        print(urlString)
        let task = URLSession.shared.dataTask(with: request) { data, response, err in
            if let str = response?.url?.absoluteString {
                completion?(str)
            }
        }
        task.resume()
    }
    
    func loadDetail(_ dance: Dance, completion: ((DanceDetail?) -> Void)?) {
        Logger.logEvent(.prepareData)
        API.shared.getDanceDetail(dance, userID: AppState.shared.userInfo?.id ?? -1) { danceDetail, status in
            danceDetail?.dance = dance
            if (danceDetail?.frames.count ?? 0) > 0 {
                danceDetail?.updateFrameTime()
                self.loadDanceImageFrame(danceDetail: danceDetail!) { count in
                    self.loadingPassthroughSubject?.send("\(count) / \(danceDetail!.frames.count + 1)")
                } completion: {
                    self.loadAudio(dance: dance, completion: {
                        completion?(danceDetail)
                    })
                }
            }
        }
    }
    
    private func loadAudio(dance: Dance, completion: CompletionBlock) {
        if (FileManager.default.fileExists(atPath: dance.localAudioUrl().path)) {
            completion?()
            return
        }
        let destination: DownloadRequest.Destination = { _, _ in
            return (dance.localAudioUrl(), [.removePreviousFile, .createIntermediateDirectories])
            }
        AF.download(dance.audio, to: destination).responseData { response in
            completion?()
        }
    }
    
    private func loadDanceImageFrame(danceDetail: DanceDetail, loadCount: ((_ count: Int)-> Void)?, completion: CompletionBlock) {
        let group = DispatchGroup()
        
        let loadCacheQueue = DispatchQueue(label: "load cache frame")
        var s = 0
        
        for i in 0..<danceDetail.frames.count {
            let danceFrame = danceDetail.frames[i]
            let url = danceFrame.coverCacheURL()
            if !FileManager.default.fileExists(atPath: url.path) {
                group.enter()
                loadCacheQueue.async {
                    let data = try? Data.init(contentsOf: URL(string: danceFrame.cover)!)
                    try? data?.write(to: danceFrame.coverCacheURL())
                    s += 1
                    loadCount?(s)
                    group.leave()
                }
            }
        }
        
        group.notify(queue: .main) {
            completion?()
        }
    }
    
    func appear() {
        
    }
}
