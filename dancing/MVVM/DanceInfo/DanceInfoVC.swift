//
//  DanceInfoVC.swift
//  dancing
//
//  Created by viet on 19/07/2023.
//

import UIKit
import AVFoundation
import ProgressHUD

class DanceInfoVC: BaseViewController<DanceInfoViewModelFactory> {
    var coordinator: DanceInfoCoordinator!
    var dance: Dance!
    // MARK: - IBOutlet
    
    @IBOutlet weak var numPlayerLable: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var previewContainerView: UIView!
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var playButtonContainerView: UIView!
    @IBOutlet weak var playButton: UIButton!
    
    // MARK: - Variable
    private var previewAsset: AVAsset!
    private var player: AVPlayer?
    private var playVideoVC: PlayVideoVC!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        config()
        bindViewModel()
        initPlayVideoVC()
        viewModel.getDirectURL(urlString: dance.videoURL) { redirectURLString in
            DispatchQueue.main.async {
                if let url = URL(string: redirectURLString) {
                    self.previewAsset = AVAsset(url: url)
                    self.playPreview()
                }
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }

    // MARK: - Config
    func config() {
        closeButton.setTitle("", for: .normal)
        titleLabel.text = dance.title
        durationLabel.text = dance.durationTimeFormat()
        artistLabel.text = dance.artist
        numPlayerLable.text = "\(dance.numPlayer)"
        configPlayButton()
    }
    
    private func configPlayButton() {
        playButton.setTitle("PLAY", for: .normal)
        let size = playButton.frame.size
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: size.height / 2))
        path.addLine(to: CGPoint(x: 9 , y: 0))
        path.addLine(to: CGPoint(x: size.width - 9, y: 0))
        path.addLine(to: CGPoint(x: size.width, y: size.height / 2))
        path.addLine(to: CGPoint(x: size.width - 9, y: size.height))
        path.addLine(to: CGPoint(x: 9 , y: size.height))
        path.addLine(to: CGPoint(x: 0, y: size.height / 2))

        let mask = CAShapeLayer()
        mask.frame = playButton.bounds
        mask.path = path.cgPath
        playButton.layer.mask = mask
        
        playButtonContainerView.superview?.layer.masksToBounds = false
        playButtonContainerView.shadowColor = UIColor.black
        playButtonContainerView.shadowOffset = CGSize(width: 0, height: 4)
        playButtonContainerView.shadowRadius = 4
        playButtonContainerView.shadowOpacity = 0.25
    }
    
    func initPlayVideoVC() {
        playVideoVC = PlayVideoVC()
        addChild(playVideoVC)
        previewView.addSubview(playVideoVC.view)
        playVideoVC.view.fitSuperviewConstraint()
        
        
        previewView.cornerRadius = 5
        previewContainerView.shadowColor = UIColor.black
        previewContainerView.shadowOffset = CGSize(width: 0, height: 4)
        previewContainerView.shadowRadius = 4
        previewContainerView.shadowOpacity = 0.25
    }
    
    func bindViewModel() {
        viewModel.loadingPassthroughSubject?.sink(receiveValue: { progress in
            ProgressHUD.show(progress)
        }).store(in: &cancellables)
    }
    
    // MARK: - IBAction

    @IBAction func closeButtonDidTap(_ sender: Any) {
        coordinator.stop()
    }
    
    @IBAction func rankButtonDidTap(_ sender: Any) {
        let coordinator = RankCoordinator(navigation: self.navigationController!, dance: dance)
        coordinator.start()
    }
    
    @IBAction func playButtonDidTap(_ sender: Any) {
        player?.pause()
        ProgressHUD.show()
        viewModel.loadDetail(dance) { detail in
            ProgressHUD.dismiss()
            if (detail?.frames.count ?? 0) > 0 {
                let coordinator = GameCoordinator(navigation: self.navigationController!, danceDetail: detail!)
                coordinator.start()
            }
        }
    }
    @IBAction func trainButtonDidTap(_ sender: Any) {
        ProgressHUD.show()
        viewModel.loadDetail(dance) { detail in
            ProgressHUD.dismiss()
            let coordinator = TrainningCoordinator(navigation: self.navigationController!, danceDetail: detail!)
            coordinator.start()
        }
        
    }
    // MARK: - Helper
    
    private func playPreview() {
        self.player = AVPlayer(playerItem: AVPlayerItem(asset: self.previewAsset))
        playVideoVC.player = self.player

    }
}
