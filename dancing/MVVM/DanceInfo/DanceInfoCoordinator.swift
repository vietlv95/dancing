//
//  DanceInfoCoordinator.swift
//  dancing
//
//  Created by viet on 19/07/2023.
//

import UIKit

class DanceInfoCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: DanceInfoVC?
    var presenting: UIViewController
    var dance: Dance
    
    init(presenting: UIViewController, dance: Dance) {
        self.presenting = presenting
        self.dance = dance
    }
    
    func start() {
        if !started {
            started = true
            let controller = DanceInfoVC.factory()
            controller.coordinator = self
            controller.dance = dance
            let nav = BaseUINavigationController(rootViewController: controller)
            nav.isNavigationBarHidden = true
            nav.modalPresentationStyle = .fullScreen
            presenting.present(nav, animated: true)
            // Show Controller
            self.controller = controller
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
            controller?.cancellables.removeAll()
            self.controller?.dismiss(animated: true, completion: completion)
        }
    }
}
