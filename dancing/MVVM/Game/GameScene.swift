//
//  GameScene.swift
//  dancing
//
//  Created by viet on 25/07/2023.
//

import Foundation
import SpriteKit

class GameScene: SKScene {
    var waterMaskNode: SKSpriteNode!
    
    var bgProgressCropNode: SKCropNode!
    var bgProgressMaskNode: SKShapeNode!
    var bgProgressSpriteNode: SKSpriteNode!
    
    var remainProgressCropNode: SKCropNode!
    var remainProgressMaskNode: SKShapeNode!
    var remainProgressSpriteNode: SKSpriteNode!
    
    private var topLeftCornerNode: SKSpriteNode!
    private var topRightCornerNode: SKSpriteNode!
    private var botLeftCornerNode: SKSpriteNode!
    private var botRightCornerNode: SKSpriteNode!
    private var soundNameNode: SKLabelNode!
    private var modelNode: SKSpriteNode!
    private var nextModelNode: SKSpriteNode!
    
    private var timeBgNode: SKSpriteNode!
    private var timeCenterNode: SKSpriteNode!
    private var timeProgressNode: SKSpriteNode?
    private var scoreNodes = [SKSpriteNode]()
    private var desScoreNode: SKSpriteNode?
    private var operationQueue: OperationQueue = OperationQueue()
    
    // MARK: - func
    func updateModel(image: UIImage, nextModelImage: UIImage?) {
        updateModelNode(modelImage: image, nextModelImage: nextModelImage)
    }
    
    func updateScore(totalScore: Int, getNewScore: Score?, old: Int) {
        showAnimationGetScore(from: old, to: totalScore)
                
        if getNewScore != nil && getNewScore?.scoreRank != .fail {
            showGetScoreDes(score: getNewScore!)
        }
        
        if totalScore > 0 {
            showStarParticleEff()
        }
    }
    
    func updateFrameProgress(startTime: Int, endTime: Int, middleTime: Int){
        let timeBGMinY = timeBgNode.position.y + 3
        let timeBGMaxY = timeBgNode.frame.maxY - 3
        let percent = CGFloat(middleTime - startTime) / CGFloat(endTime - startTime)
        let y = timeBGMinY + (timeBGMaxY - timeBGMinY - 65) * percent
        
        timeCenterNode.position = CGPoint(x: 13, y: y)
        
        timeProgressNode?.removeFromParent()
        
        timeProgressNode = SKSpriteNode(imageNamed: "ic_game_time_progress")
        timeProgressNode?.size = CGSize(width: 21, height: 21)
//        insertChild(timeProgressNode!, at: 0)
        addChild(timeProgressNode!)
        timeProgressNode?.anchorPoint = .zero
        timeProgressNode?.position = CGPoint(x: 14, y: timeBgNode.position.y + 4)
        let moveAction = SKAction.move(to: CGPoint(x: 14, y: timeBgNode.frame.maxY - 4 - 21), duration: TimeInterval(endTime - startTime) / 1000)
        let doneAction = SKAction.removeFromParent()
        timeProgressNode?.run(SKAction.sequence([moveAction, doneAction]))
    }
    
    func setGameProgress(_ progress: CGFloat) {
        let x: CGFloat = 85
        let max_w = size.width - 85 - 15
        let h: CGFloat = 14
        let y: CGFloat = 43
        if bgProgressCropNode == nil {
            bgProgressCropNode = SKCropNode()
            bgProgressMaskNode = SKShapeNode(rect: CGRect(x: 0, y: 0, width: max_w, height: 14), cornerRadius: 12)
            bgProgressMaskNode.fillColor = UIColor(rgb: 0xD9D9D9)
            bgProgressSpriteNode = SKSpriteNode(color: UIColor(rgb: 0xD9D9D9), size: CGSize(width: max_w, height: h))
            bgProgressSpriteNode.anchorPoint = .zero
            bgProgressSpriteNode.position = .zero
            
            bgProgressCropNode.maskNode = bgProgressMaskNode
            bgProgressCropNode.addChild(bgProgressSpriteNode)
            bgProgressCropNode.position = CGPoint(x: x, y: y)
            addChild(bgProgressCropNode)
        }
        
        remainProgressSpriteNode?.removeFromParent()
        remainProgressCropNode?.maskNode = nil
        remainProgressCropNode?.removeFromParent()
        
        remainProgressCropNode = SKCropNode()
        remainProgressMaskNode = SKShapeNode(rect: CGRect(x: 0, y: 0, width: max(0, max_w * (1 - progress)), height: 14), cornerRadius: 12)
        remainProgressMaskNode.fillColor = UIColor(rgb: 0x00FFFF)
        remainProgressSpriteNode = SKSpriteNode(color: UIColor(rgb: 0x00FFFF), size: CGSize(width: max(0, max_w * (1 - progress)), height: h))
        remainProgressSpriteNode.anchorPoint = .zero
        remainProgressSpriteNode.position = .zero
        remainProgressCropNode.maskNode = remainProgressMaskNode
        remainProgressCropNode.addChild(remainProgressSpriteNode)
        remainProgressCropNode.position = CGPoint(x: x, y: 43)
        addChild(remainProgressCropNode)
    }
    
    func setAudioURL(_ url: URL) {
        let audioNode = SKAudioNode(url: url)
        audioNode.autoplayLooped = false
        addChild(audioNode)
    }
    
    func setSoundName(_ name: String) {
        addSoundNameNode(soundName: name)
    }
    
    private var starParticleEmitterNode: SKEmitterNode?
    
    func showStarParticleEff() {
        
        if starParticleEmitterNode == nil {
            starParticleEmitterNode = SKEmitterNode(fileNamed: "Star.sks")
            starParticleEmitterNode?.zPosition = 0
            starParticleEmitterNode?.position = CGPoint(x: size.width / 2, y: size.height / 2)
            starParticleEmitterNode?.targetNode = self
            let groupNode = SKSpriteNode()
            groupNode.position = .zero
            groupNode.size = size
            
            
//            addChild(starParticleEmitterNode!)
            
            self.addChild(groupNode)
            groupNode.addChild(self.starParticleEmitterNode!)
            self.starParticleEmitterNode?.resetSimulation()
            
            let waitAction = SKAction.wait(forDuration: 2)
            let removeAction = SKAction.run {
                self.starParticleEmitterNode?.removeFromParent()
                self.starParticleEmitterNode = nil
            }
            
            starParticleEmitterNode?.run(SKAction.sequence([waitAction, removeAction]))
        }
//        let emitter = starParticleEmitterNode!
//        let emitterPaused = emitter.isPaused
//        if emitterPaused {
//            emitter.isPaused = false
//        }
//        emitter.advanceSimulationTime(0)
//        if emitterPaused {
//            emitter.isPaused = true
//        }
        
    }

    // MARK: - lifecycle
    
    func initSomeNode() {
        addFooter()
        addWaterMask()
        addTimeFrameNode()
        addPersonCorner()
    }
    
    override func didMove(to view: SKView) {
//        addWaterMask()
//        addTimeFrameNode()
//        addPersonCorner()
        
    }
    
    private func addFooter() {
        let node = SKSpriteNode(imageNamed: "game_foot_header")
        addChild(node)
        node.anchorPoint = .zero
        node.size = CGSize(width: self.size.width, height: 163)
        node.position = .zero
    }
    
    // MARK: - Help
    
    private func addWaterMask() {
        waterMaskNode = SKSpriteNode(imageNamed: "ic_game_water_mask")
        addChild(waterMaskNode)
        waterMaskNode.size = CGSize(width: 72, height: 69)
        waterMaskNode.position = CGPoint(x: 9, y: 12)
        waterMaskNode.anchorPoint = .zero
    }
    
    private func addPersonCorner() {
        let cornerWidth = 40
        let cornerHeight = 40
        
        botLeftCornerNode = SKSpriteNode(imageNamed: "ic_botleft_person_corner")
        botLeftCornerNode.alpha = 0.45
        addChild(botLeftCornerNode)
        botLeftCornerNode.anchorPoint = .zero
        botLeftCornerNode.size = CGSize(width: cornerWidth, height: cornerHeight)
        
        botRightCornerNode = SKSpriteNode(imageNamed: "ic_botright_person_corner")
        botRightCornerNode.alpha = 0.45
        addChild(botRightCornerNode)
        botRightCornerNode.anchorPoint = .zero
        botRightCornerNode.size = CGSize(width: cornerWidth, height: cornerWidth)
        
        topLeftCornerNode = SKSpriteNode(imageNamed: "ic_topleft_person_corner")
        topLeftCornerNode.alpha = 0.45
        addChild(topLeftCornerNode)
        topLeftCornerNode.anchorPoint = .zero
        topLeftCornerNode.size = CGSize(width: cornerWidth, height: cornerHeight)
        
        topRightCornerNode = SKSpriteNode(imageNamed: "ic_topright_person_corner")
        topRightCornerNode.alpha = 0.45
        addChild(topRightCornerNode)
        topRightCornerNode.anchorPoint = .zero
        topRightCornerNode.size = CGSize(width: 40, height: 40)
        updateCornerPosition()
    }
    
    private func addTimeFrameNode() {
        timeBgNode = SKSpriteNode(imageNamed: "ic_game_time_bg")
        timeBgNode.anchorPoint = .zero
        timeBgNode.position = CGPoint(x: 10, y: size.height - 175 - 210)
        timeBgNode.size = CGSize.init(width: 29, height: 210)
        
        timeCenterNode = SKSpriteNode(imageNamed: "ic_game_time_center")
        timeCenterNode.anchorPoint = .zero
        timeCenterNode.size = CGSize(width: 23, height: 65)
        
        addChild(timeCenterNode)
        addChild(timeBgNode)
    }
    
    private func updateCornerPosition() {
        let marginHoriziontal = 9
        let marginBot = 153
        let marginTop = 63
        let cornerWidth = 40
        let cornerHeight = 40
        botLeftCornerNode.position = CGPoint(x: marginHoriziontal, y: marginBot)
        botRightCornerNode.position = CGPoint(x: Int(size.width) - cornerWidth - marginHoriziontal, y: marginBot)
        topLeftCornerNode.position = CGPoint(x: marginHoriziontal, y: Int(size.height) - marginTop - cornerHeight)
        topRightCornerNode.position = CGPoint(x: Int(size.width) - cornerWidth - marginHoriziontal, y: Int(size.height) - marginTop - cornerHeight)
    }
    
    private func addSoundNameNode(soundName: String) {
        soundNameNode = SKLabelNode()
        soundNameNode.fontName = "Poppins-Bold"
        soundNameNode.fontSize = 16
        soundNameNode.fontColor = .white
        soundNameNode.horizontalAlignmentMode = .left
        addChild(soundNameNode)
        soundNameNode.position = CGPoint(x: 83, y: 21)
        soundNameNode.text = soundName
    }
    
    private func updateModelNode(modelImage: UIImage, nextModelImage: UIImage?) {
        modelNode?.removeFromParent()
        modelNode = SKSpriteNode.init(texture: SKTexture(image: modelImage))
        addChild(modelNode)
        modelNode.anchorPoint = .zero
        modelNode.size = CGSizeMake(279, 279)
        modelNode.position = CGPointMake(23, 65)
        
        if let nextModelImage = nextModelImage {
            if nextModelImage.size.width != 0 {
                nextModelNode?.removeFromParent()
                nextModelNode = SKSpriteNode.init(texture: SKTexture(image: nextModelImage))
                addChild(nextModelNode!)
                nextModelNode?.anchorPoint = .zero
                nextModelNode?.size = CGSizeMake(120, 120)
                nextModelNode.position = CGPointMake(size.width - 125, 57)
            }
        } else {
            nextModelNode?.removeFromParent()
        }
    }
    
    var effectNode: SKEmitterNode!
    
    override func didChangeSize(_ oldSize: CGSize) {
        if topLeftCornerNode != nil {
            updateCornerPosition()
        }
    }
    
    private var scoreTimer: Timer?
    private var isScaleScoreNode = false
    
    private func showAnimationGetScore(from fromScore: Int, to toScore: Int) {
        addScoreNodeContainer()
        scoreTimer?.invalidate()
        if !self.isScaleScoreNode {
            self.scoreNodeContainer.run(SKAction.scale(by: 1.2, duration: 0.04))
            self.isScaleScoreNode = true
        }
        
        let duration = 0.8
        let timing = 0.04
        var curTime: CGFloat = 0
        
        scoreTimer = Timer.scheduledTimer(withTimeInterval: timing, repeats: true, block: { timer in
            curTime += timing
            if curTime > duration {
                timer.invalidate()
                if !self.isScaleScoreNode {
                    self.scoreNodeContainer.run(SKAction.scale(by: 1.2, duration: 0.04))
                    self.isScaleScoreNode = true
                }
                return
            }
            let percentTime = curTime / duration
            let curScore = Int((CGFloat(toScore) - CGFloat(fromScore)) * percentTime) + fromScore
            self.showScoreValue(score: curScore)
            if !self.isScaleScoreNode {
                self.scoreNodeContainer.run(SKAction.scale(by: 1.2, duration: 0.04))
            } else {
                self.scoreNodeContainer.run(SKAction.scale(by: 1 / 1.2, duration: 0.04))
            }
            self.isScaleScoreNode.toggle()
        })
    }
    
    private var scoreNodeContainer: SKSpriteNode!
    private func addScoreNodeContainer() {
        if scoreNodeContainer != nil {
            return
        }
        scoreNodeContainer = SKSpriteNode()
        scoreNodeContainer.position = CGPoint(x: self.size.width / 2, y: self.size.height - 85)
        scoreNodeContainer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        desScoreNode?.size = CGSize(width: 150, height: 35)
        addChild(scoreNodeContainer)
    }
    
    private var increeScoreColor: UIColor?
    
    private func showScoreValue(score: Int) {
        scoreNodes.forEach { obj in
            obj.removeFromParent()
        }
        if !scoreNodes.isEmpty {
            scoreNodes.removeAll()
        }
        
        var scoreStr = "0"
        if score != 0 {
            scoreStr = String(score)
        }
        
        let w: CGFloat = 25
        let h: CGFloat = 35
        let y: CGFloat = 0//self.size.height - 40 - h
        let space: CGFloat = 0
        let count: CGFloat = CGFloat(scoreStr.count)
        let totalW = count * w + (count - 1) * space
        let startX: CGFloat = scoreNodeContainer.size.width / 2 - totalW / 2
        
        for i in 0..<scoreStr.count {
            let node = SKSpriteNode(imageNamed: String(scoreStr[i]))
            scoreNodeContainer.addChild(node)
            scoreNodes.append(node)
            node.anchorPoint = .zero
            node.size = CGSize(width: w, height: h)
            node.position = CGPoint(x: startX + CGFloat(i) * (w + space), y: y)
        }
    }

    func showGetScoreDes(score: Score) {
        if (desScoreNode?.parent) != nil {
            return
        }
        
        desScoreNode = SKSpriteNode(imageNamed: score.scoreRank.rawValue)
        desScoreNode?.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        desScoreNode?.size = CGSize(width: 120, height: 80)
        desScoreNode?.position = CGPoint(x: (size.width - 0) / 2, y: self.size.height - 170)
        self.addChild(desScoreNode!)
        
        var maxScale: CGFloat = 2
        switch score.scoreRank {
        case .perfect:
            maxScale = CGFloat.random(in: 1.9...2.2)
        case .great:
            maxScale = CGFloat.random(in: 1.8...2.1)
        case .cool:
            maxScale = CGFloat.random(in: 1.7...2.6)
        case .ok:
            maxScale = CGFloat.random(in: 1.6...2.0)
        default:
            break
        }
        
        var miniScale = CGFloat.random(in: 1...1.1)
        var miniScaleDuration = CGFloat.random(in: 0.03...0.08)
        let scale1Action = SKAction.scale(to: miniScale, duration: miniScaleDuration)
        let wait1 = SKAction.wait(forDuration: miniScaleDuration)
        let scale2Action = SKAction.scale(to: 1 / miniScale, duration: miniScaleDuration)
        let wait2 = SKAction.wait(forDuration: miniScaleDuration)
        let scale3Action = SKAction.scale(to: maxScale, duration: 0.2)
        let alpha3Action = SKAction.fadeAlpha(to: 0.5, duration: 0.15)
        let wait3 = SKAction.wait(forDuration: 0.25)
        let scale4Action = SKAction.scale(to: 1 / maxScale, duration: 0.15)
        let alpha4Action = SKAction.fadeAlpha(to: 0, duration: 0.1)
        let wait4 = SKAction.wait(forDuration: 0.15)
        let removeAction = SKAction.removeFromParent()
        desScoreNode?.run(SKAction.sequence([scale1Action, wait1, scale2Action, wait2, scale3Action, alpha3Action, wait3, scale4Action, alpha4Action, wait4, removeAction]))
    }
    
    private func showPerfectAnim(node: SKSpriteNode) {
        showCoolAnim(node: node)
    }
    
    private func showGreatAnim(node: SKSpriteNode) {
//        let scaleAction = SKAction.scale(by: 1.8, duration: .zero)
//        let wait = SKAction.wait(forDuration: 0.6)
//        let removeAction = SKAction.removeFromParent()
//        node.run(SKAction.sequence([scaleAction, wait, removeAction]))
        showCoolAnim(node: node)
    }
    
    private func showCoolAnim(node: SKSpriteNode) {
        let scale1Action = SKAction.scale(to: 1.1, duration: 0.08)
        let wait1 = SKAction.wait(forDuration: 0.1)
        let scale2Action = SKAction.scale(to: 1 / 1.1, duration: 0.08)
        let wait2 = SKAction.wait(forDuration: 0.1)
        let scale3Action = SKAction.scale(to: 2, duration: 0.2)
        let alpha3Action = SKAction.fadeAlpha(to: 0.8, duration: 0.15)
        let wait3 = SKAction.wait(forDuration: 0.25)
        let scale4Action = SKAction.scale(to: 1 / 2, duration: 0.15)
        let alpha4Action = SKAction.fadeAlpha(to: 0, duration: 0.1)
        let wait4 = SKAction.wait(forDuration: 0.15)
        let removeAction = SKAction.removeFromParent()
        node.run(SKAction.sequence([scale1Action, wait1, scale2Action, wait2, scale3Action, alpha3Action, wait3, scale4Action, alpha4Action, wait4, removeAction]))
    }
    
    private func showOkAnim(node: SKSpriteNode) {
        showCoolAnim(node: node)
    }
    // MARK: - Helper
}
