//
//  GameViewModel.swift
//  dancing
//
//  Created by viet on 20/07/2023.
//

import UIKit
import Combine
import MetalKit
import Metal
import AVFoundation
import AVKit
import RealmSwift

protocol GameViewModelFactory: BaseViewModelFactory {
    var poseFramePassthroughSubject: PassthroughSubject<PoseFrame, Never>? {get set}
    var danceFramePassthroughSubject: PassthroughSubject<DanceFrame, Never>? {get set}
    var addScorePassthroughSubject: PassthroughSubject<Score, Never>? {get set}
    var gameProgressPassthroughSubject: PassthroughSubject<CGFloat, Never>? {get set}
    var finishGamePassthroughSubject: PassthroughSubject<Void, Never>? {get set}
    var gameResultPassthroughSubject: PassthroughSubject<GameResult, Never>? {get set}
    var detectPeopleStatePassthroughSubject: PassthroughSubject<DetectPeopleState, Never>? {get set}
    var danceDetail: DanceDetail? {get set}
    var gameState: GameState? {get set}
    var camera: MetalCamera? {get set}
    var videoGenerateQueue: DispatchQueue? {get set}
//    func
    func setController(_ controller: UIViewController?)
    func startCamera()
    func startGame()
    func replay()
    func recordGameFrame(image: UIImage)
}

class GameViewModel: GameViewModelFactory {
    var poseFramePassthroughSubject: PassthroughSubject<PoseFrame, Never>? = PassthroughSubject()
    var addScorePassthroughSubject: PassthroughSubject<Score, Never>? = PassthroughSubject()
    var danceFramePassthroughSubject: PassthroughSubject<DanceFrame, Never>? = PassthroughSubject()
    var gameProgressPassthroughSubject: PassthroughSubject<CGFloat, Never>? = PassthroughSubject()
    var finishGamePassthroughSubject: PassthroughSubject<Void, Never>? = PassthroughSubject()
    var gameResultPassthroughSubject: PassthroughSubject<GameResult, Never>? = PassthroughSubject()
    var detectPeopleStatePassthroughSubject: PassthroughSubject<DetectPeopleState, Never>? = PassthroughSubject()
    
    var danceDetail: DanceDetail? {
        didSet {
            self.frameScores = [Score].init(repeating: Score(minScore: 0, score: 0, isCombo: false, timeDiffScore: 0, angleScore: 0, des: ""), count: danceDetail?.frames.count ?? 0)
        }
    }
    var gameState: GameState?
    var camera: MetalCamera?
    var videoGenerateQueue: DispatchQueue? = DispatchQueue(label: "videoGenerateQueue")
    
    // MARK: - Variable
    
    private var device = MTLCreateSystemDefaultDevice()
    private var commandQueue: MTLCommandQueue!
    private weak var controller: UIViewController?
    private var timer: Timer?
    private var startTime: TimeInterval = 0
    private var gameDuration: TimeInterval = 0
    private var score: Int = 0
    private var frameScores: [Score] = []
    private var videoGenerate: VideoGerenate?
    private var videoCoverPath: String?
    private var detectPeopleState: DetectPeopleState = .inStep1
    private var passCurrentDetectPeopleState = false
    // MARK: - Function
    
    init() {
        commandQueue = device?.makeCommandQueue()
    }
    
    func appear() {
        
    }
    
    func setController(_ controller: UIViewController?) {
        self.controller = controller
    }
    
    func startCamera() {
        camera = MetalCamera(device: device!, commandQueue: commandQueue, mtlDevice: self.device ?? device!, delegate: self)
        let rs = camera?.startSession(quality: .hd) ?? false
        if !rs {
            controller?.showAlertMessage(titleStr: "Opps", messageStr: "your device can't start Camera")
        }
    }
    
    var player: AVPlayer?
    
    func startGame() {
        videoGenerate = VideoGerenate(queue: videoGenerateQueue!, width: controller!.view.frame.width, height: controller!.view.frame.height)
        videoGenerate?.newComponentVideo()
        
        gameState = .playing
        danceDetail?.frames.forEach({ danceFrame in
            let waitTime = CGFloat(danceFrame.startTime) / 1000
            DispatchQueue.main.asyncAfter(deadline: .now() + waitTime) {
                print(waitTime)
                print("send \(danceFrame.idx)")
                self.danceFramePassthroughSubject?.send(danceFrame)
            }
        })
        
        let gameDuration = TimeInterval(danceDetail?.dance.duration ?? "0") ?? 0
        self.gameDuration = gameDuration
        
        DispatchQueue.main.asyncAfter(deadline: .now() + gameDuration) {
            self.camera?.stopSession()
            self.finishGamePassthroughSubject?.send(())
            self.timer?.invalidate()
            self.timer = nil
            
            self.videoGenerate?.finish({ assets in
                guard let asset = assets.first else {
                    fatalError()
                }
                let audioAsset = AVAsset(url: self.danceDetail!.dance.localAudioUrl())
                let mergeAsset = AVMutableComposition()
                mergeAsset.copyAllTrack(from: asset)
                mergeAsset.copyAudioTrack(from: audioAsset, volume: 1)
                self.toAVURLAsset(mergeAsset) { saveAsset in
                    guard let saveAsset = saveAsset else {
                        fatalError()
                    }
                    self.saveAndUploadGameResult(saveAsset: saveAsset)
                }
            })
        }
        
        startTime = Date().timeIntervalSince1970
        timer = Timer.scheduledTimer(withTimeInterval: 0.03, repeats: true, block: {[weak self] _ in
            guard let self = self else {return}
            let curTime = Date().timeIntervalSince1970
            let progress = CGFloat(curTime - startTime) / self.gameDuration
            self.gameProgressPassthroughSubject?.send(progress)
        })
    }
    
    func replay() {
        self.score = 0
        detectPeopleState = .inStep1
        passCurrentDetectPeopleState = false
        self.frameScores = [Score].init(repeating: Score(minScore: 0, score: 0, isCombo: false, timeDiffScore: 0, angleScore: 0, des: ""), count: danceDetail?.frames.count ?? 0)
        self.startCamera()
    }
    
    func recordGameFrame(image: UIImage) {
        _ = videoGenerate?.add(image: image, addTime: Date().timeIntervalSince1970)
    }
    // MARK: - Helper
    
    private func checkGetScore(poseFrame: PoseFrame, currentTime: Int) {
        for i in 0..<(self.danceDetail?.frames.count)! {
            let frame = self.danceDetail!.frames[i]
            if frame.startTime <= currentTime && currentTime < frame.endTime {
                if frameScores[i].score == 0 && !poseFrame.poses.isEmpty {
                    caculatoreScore(poseFrame: poseFrame, frameIndex: i, currentTime: currentTime)
                }
                break
            }
        }
    }
    
    private func caculatoreScore(poseFrame: PoseFrame, frameIndex: Int, currentTime: Int) {
        var score = ScoreRake().caculator(poseFrame: poseFrame, danceFrame: danceDetail!.frames[frameIndex], currentTime: currentTime, danceDetail: danceDetail!, index: frameIndex)
//        score = Score(minScore: 4, score: Int.random(in: 10...25), isCombo: false, timeDiffScore: score?.timeDiffScore ?? 0, angleScore: score?.angleScore ?? 0, des: score?.des ?? "")
        let COMBO_LENGTH = 3
        
        if frameIndex >= COMBO_LENGTH && score != nil {
            var isCombo = true
            for i in frameIndex - 3...frameIndex - 1 {
                if frameScores[i].score <= 0 {
                    isCombo = false
                }
            }
            if isCombo {
                score = ScoreRake().rakeCombo(score: score!)
            }
            
        }
        
        if score != nil {
            self.frameScores[frameIndex] = score!
        }
        
        if (score?.score ?? 0) > 0 {
            self.score += score!.score
            addScorePassthroughSubject?.send(score!)
        }
    }
    
    private func toAVURLAsset(_ asset: AVAsset, completion: ((_ urlAsset: AVURLAsset?)->Void)?) {
        let path = CacheManager().saveVideoFolder() + "/dance_\(danceDetail!.dance.id).mp4"
        let url = URL(fileURLWithPath: path)
        if FileManager.default.fileExists(atPath: path) {
            try? FileManager.default.removeItem(at: url)
        }
        let composition = AVMutableComposition()
        composition.copyAllTrack(from: asset)
        
        let audioMix = AVMutableAudioMix()
        var params = [AVMutableAudioMixInputParameters]()
        let tracks = composition.tracks(withMediaType: .audio)
        let volume = AppState.shared.settingMusicValue
        tracks.forEach { track in
            let audioInputParams = AVMutableAudioMixInputParameters.init()
            audioInputParams.setVolume(volume, at: .zero)
            audioInputParams.trackID = track.trackID
            params.append(audioInputParams)
        }
        audioMix.inputParameters = params
        
        let exporter = VideoExporter()
        exporter.composition = composition
        exporter.audioMix = audioMix
        
        exporter.exportVideo(at: url) { _ in
            let urlAsset = AVURLAsset(url: url)
            completion?(urlAsset)
        } progress: { _ in
            
        }
    }
    
    private func saveAndUploadGameResult(saveAsset: AVURLAsset) {
//        return
        saveGame(saveAsset)
        
        let userId = AppState.shared.userInfo?.id ?? -1
        let audioID = Int(danceDetail!.dance.id)!
        let score = self.score
        let accuracy = getAccuracy()
        let poseTime = getPoseTime()
        let videoMd5 = getVideoMd5(saveAsset: saveAsset)
        let coverMd5 = getCoverVideoMd5(saveAsset: saveAsset)
        let playInfo = GamePlayInfo(level: 0, maxCombo: getMaxCombo(), score: score, soundId: Int(danceDetail!.dance.id)!, urlVideo: "")
        let devideID = UIDevice.current.identifierForVendor!.uuidString
        
        let matchInfo = MatchInfo(userID: userId, audioID: audioID, score: score, accuracy: accuracy, pose_time: poseTime, video_md5: videoMd5, cover_md5: coverMd5, playInfo: playInfo.toParam(), deviceID: devideID)
        
        API.shared.uploadMatch(matchInfo: matchInfo) { uploadMatchResult in
            API.shared.uploadVideo(matchID: uploadMatchResult.id, videoMd5: videoMd5, coverMd5: coverMd5, videoAsset: saveAsset, coverImagePath: self.videoCoverPath!) { success in
                let gameResult = GameResult(videoURL: saveAsset.url, danceDetail: self.danceDetail!)
                gameResult.score = self.score
                self.gameResultPassthroughSubject?.send(gameResult)
            }
        } failed: { errMessage in
            fatalError()
        }

    }
    
    private func saveGame(_ saveAsset: AVURLAsset) {
        let realm = try! Realm()
        let danceID = Int(danceDetail?.dance.id ?? "") ?? 0
        let saveVideoName = "\(danceID).mp4"
        let path = CacheManager.shared.highScoreVideoFolder().appending("/\(saveVideoName)")
        
        if let obj = realm.object(ofType: SaveDance.self, forPrimaryKey: danceID) {
            if score >= obj.score {
                try? FileManager.default.removeItem(atPath: path)
                try? realm.write({
                    obj.score = score
                    obj.totalScore += score
                })
            }
        } else {
            let obj = SaveDance()
            obj.danceID = danceID
            obj.saveVideoName = saveVideoName
            obj.score = score
            obj.totalScore = score
            try? realm.write({
                realm.add(obj, update: .modified)
            })
        }
        try? FileManager.default.copyItem(at: saveAsset.url, to: URL.init(fileURLWithPath: path))
    }
    
    private func getMaxCombo() -> Int {
        var curCombo = 0
        var maxCombo = 0
        for i in 1..<frameScores.count {
            if frameScores[i].score > 0 && frameScores[i - 1].score > 0{
                curCombo += 1
            } else {
                if maxCombo < curCombo {
                    maxCombo = curCombo
                }
                curCombo = 0
            }
        }
        
        if maxCombo < curCombo {
            maxCombo = curCombo
        }
        return 0
    }
    
    private func getPoseTime() -> CGFloat {
        let timeDiffScores: [CGFloat] =  frameScores.map { score in
            return CGFloat(score.timeDiffScore)
        }
        
        let sum = timeDiffScores.reduce(0) { partialResult, element in
            return partialResult + element
        }
        return sum / CGFloat(frameScores.count)
    }
    
    private func getAccuracy() -> CGFloat {
        let accuracies: [CGFloat] =  frameScores.map { score in
            return CGFloat(score.angleScore)
        }
        
        let sum = accuracies.reduce(0) { partialResult, element in
            return partialResult + element
        }
        return sum / CGFloat(frameScores.count)
    }
    
    private func getVideoMd5(saveAsset: AVURLAsset) -> String {
        let data = try! Data(contentsOf: saveAsset.url)
        return MD5.get(data: data)
    }
    
    private func getCoverVideoMd5(saveAsset: AVURLAsset) -> String {
        let img = saveAsset.imageAtTime(CMTime(value: 1, timescale: 1))
        let data = img.jpegData(compressionQuality: 1)!
        videoCoverPath = NSTemporaryDirectory() + "\(danceDetail!.dance.id)_videoCover.jpeg"
        if FileManager.default.fileExists(atPath: videoCoverPath!) {
            try? FileManager.default.removeItem(atPath: videoCoverPath!)
        }
        try! data.write(to: URL(fileURLWithPath: videoCoverPath!))
        
        return MD5.get(data: data)
    }
    
    private func createVideoCover() {
        
    }
}

// MARK: - MetalCameraDelegate
extension GameViewModel: MetalCameraDelegate {
    func metalCamera(_ camera: MetalCamera, didCapturePoseFrame poseFrame: PoseFrame) {
        poseFramePassthroughSubject?.send(poseFrame)
        let imageSize = CGSize(width: poseFrame.imageNode.texture.width, height: poseFrame.imageNode.texture.height)
        let poseAnalysic = PoseAnalysic()
        if gameState == .detectPeople {
            if !passCurrentDetectPeopleState {
                switch detectPeopleState {
                case .inStep1:
                    if poseAnalysic.analysic(poseLandmarkPoints: poseFrame.poses.first ?? [], imageSize: imageSize) != .none {
//                    if true {
                        passCurrentDetectPeopleState = true
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                            self.detectPeopleState = .inStep2
                            self.detectPeopleStatePassthroughSubject?.send(self.detectPeopleState)
                            self.passCurrentDetectPeopleState = false
                        }
                    }
                    
                case .inStep2:
                    if poseAnalysic.analysic(poseLandmarkPoints: poseFrame.poses.first ?? [], imageSize: imageSize) == .raiseLeftHand {
//                    if true {
                        passCurrentDetectPeopleState = true
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                            self.detectPeopleState = .inStep3
                            self.detectPeopleStatePassthroughSubject?.send(self.detectPeopleState)
                            self.passCurrentDetectPeopleState = false
                        }
                    }
                case .inStep3:
                    if poseAnalysic.analysic(poseLandmarkPoints: poseFrame.poses.first ?? [], imageSize: imageSize) == .raiseTwoHand {
//                    if true {
                        passCurrentDetectPeopleState = true
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.detectPeopleState = .doneStep3
                            self.detectPeopleStatePassthroughSubject?.send(self.detectPeopleState)
                            //self.passCurrentDetectPeopleState = false
                        }
                    }
                default:
                    break
                }
            }
        }
        
        if gameState == .playing {
            let currentTime = Int((Date().timeIntervalSince1970 - self.startTime) * 1000)
            self.checkGetScore(poseFrame: poseFrame, currentTime: currentTime)
        }
    }
}


