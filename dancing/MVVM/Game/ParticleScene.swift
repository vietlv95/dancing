//
//  ParticleScene.swift
//  dancing
//
//  Created by viet on 21/08/2023.
//

import Foundation
import SpriteKit

class ParticleScene: SKScene {
    func playStar() {
        let scence = SKScene(size: size)
        let node = SKEmitterNode(fileNamed: "Star.sks")!
        
        self.children.forEach { child in
            if child is SKEmitterNode {
                child.removeFromParent()
            }
        }
        
        let show = SKAction.run {
            self.addChild(scence)
            node.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
            node.targetNode = scence
            scence.addChild(node)
            node.resetSimulation()
        }
        
        let wait = SKAction.wait(forDuration: 1.1)
        let hide = SKAction.run {
            scence.removeFromParent()
        }
        
        self.update(CACurrentMediaTime())
        run(SKAction.sequence([show]))
    }
}
