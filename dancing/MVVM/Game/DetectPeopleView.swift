//
//  DetectPeopleView.swift
//  dancing
//
//  Created by viet on 04/08/2023.
//

import Foundation
import UIKit
import THLabel

enum DetectPeopleState: Int {
    case inStep1 = 0
    case inStep2 = 1
    case inStep3 = 2
    case doneStep3 = 3
}

class DetectPeopleView: UIView {
    var dectPeopleStep: DetectPeopleState = .inStep1 {
        didSet {
            updateState()
        }
    }
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var guideLabel: THLabel!
    @IBOutlet weak var guideImageView: UIImageView!
    @IBOutlet weak var step_1_ImageView: UIImageView!
    @IBOutlet weak var step_2_ImageView: UIImageView!
    @IBOutlet weak var step_3_ImageView: UIImageView!
    @IBOutlet weak var lineBetwenStep12View: UIView!
    @IBOutlet weak var lineBetwenStep23View: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        customInit()
    }
    
    private func customInit() {
        loadContentView()
        backgroundColor = UIColor(rgb: 0xD9D9D9).withAlphaComponent(0.5)
        
        guideLabel.strokeSize = 1
        guideLabel.strokeColor = UIColor(rgb: 0xBD0DA1)
    }
    
    private func loadContentView() {
        contentView = Bundle.main.loadNibNamed(String(describing: self.classForCoder), owner: self, options: nil)?.first as? UIView
        guard let view = contentView else {
            return
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        view.fitSuperviewConstraint()
    }
    
    private func updateState() {
        lineBetwenStep12View.backgroundColor = dectPeopleStep.rawValue >= DetectPeopleState.inStep2.rawValue ? UIColor(rgb: 0xBD0DA1) : UIColor(rgb: 0xD1D5DB)
        lineBetwenStep23View.backgroundColor = dectPeopleStep.rawValue >= DetectPeopleState.inStep3.rawValue ? UIColor(rgb: 0xBD0DA1) : UIColor(rgb: 0xD1D5DB)
        
        step_1_ImageView.image =  UIImage(named: dectPeopleStep.rawValue >= DetectPeopleState.inStep2.rawValue ? "ic_guide_done" : "ic_guide_testing")
        step_2_ImageView.image =  UIImage(named: dectPeopleStep.rawValue >= DetectPeopleState.inStep3.rawValue ? "ic_guide_done" : "ic_guide_testing")
        step_3_ImageView.image =  UIImage(named: dectPeopleStep.rawValue > DetectPeopleState.inStep3.rawValue ? "ic_guide_done" : "ic_guide_testing")
        
        switch dectPeopleStep {
            case .inStep1:
                guideImageView.image = UIImage(named: "guide_stand_in_frame")
                guideLabel.text = "Stand in the frame"
            case .inStep2:
                guideImageView.image = UIImage(named: "guide_raise_left_hand")
                guideLabel.text = "Raise your left hand"
            case .inStep3:
                guideImageView.image = UIImage(named: "guide_raise_two_hand")
                guideLabel.text = "Raise your right hand"
            case .doneStep3:
                guideImageView.image = UIImage(named: "guide_raise_two_hand")
        }
    }
    
}
