//
//  ReadyView.swift
//  dancing
//
//  Created by viet on 26/07/2023.
//

import Foundation
import UIKit

class ReadyView: UIView {
    
    @IBOutlet weak var labelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelWidthConstraint: NSLayoutConstraint!
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var imageView: UIImageView!
    var timer: Timer?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        customInit()
    }
        
    private func customInit() {
        backgroundColor = UIColor(rgb: 0xD9D9D9B2).withAlphaComponent(0.7)
        loadContentView()
    }
    
    func show(completion: (()->Void)?) {
        let queue = DispatchQueue(label: "wait_ready")
        queue.async {
            let finished = DispatchWorkItem.init {
                DispatchQueue.main.async {
                    self.removeFromSuperview()
                    completion?()
                }
            }
            
            let showReady = DispatchWorkItem {
                DispatchQueue.main.async {
                    self.labelWidthConstraint.constant = 214
                    self.labelHeightConstraint.constant = 50
                    self.layoutIfNeeded()
                    self.imageView.image = UIImage(named: "ready")
                }
                Thread.sleep(forTimeInterval: 1)
            }
            
            let showOne = DispatchWorkItem {
                DispatchQueue.main.async {
                    self.imageView.image = UIImage(named: "r1")
                }
                Thread.sleep(forTimeInterval: 1)
            }
            
            let showTwo = DispatchWorkItem {
                DispatchQueue.main.async {
                    self.imageView.image = UIImage(named: "r2")
                }
                Thread.sleep(forTimeInterval: 1)
            }
            
            let showThree = DispatchWorkItem {
                DispatchQueue.main.async {
                    self.imageView.image = UIImage(named: "r3")
                }
                Thread.sleep(forTimeInterval: 1)
            }
            
            showThree.notify(queue: queue, execute: showTwo)
            showTwo.notify(queue: queue, execute: showOne)
            showOne.notify(queue: queue, execute: showReady)
            showReady.notify(queue: queue, execute: finished)
            
            showThree.perform()
        }
        
    }
    
    private func loadContentView() {
        contentView = Bundle.main.loadNibNamed(String(describing: self.classForCoder), owner: self, options: nil)?.first as? UIView
        guard let view = contentView else {
            return
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        view.fitSuperviewConstraint()
    }
}
