//
//  GameVC.swift
//  dancing
//
//  Created by viet on 20/07/2023.
//

import UIKit
import Metal
import MetalKit
import SpriteKit
import ProgressHUD
import AVFoundation

class GameVC: BaseViewController<GameViewModelFactory> {
    var coordinator: GameCoordinator!
    var danceDetail: DanceDetail!
    // MARK: - IBOutlet
    
    @IBOutlet weak var guideView: UIView!
    private var skrenderer: SKRenderer!
    private var skView: SKView!
    private var mtkView: MTKView!
    private var metalRender: MetalMixingRender!
    private var detectPeopleView: DetectPeopleView!
    // MARK: - Variable
    
    private var gameScene: GameScene!
    private var cutOffX: CGFloat = 0
    private var cutOffY: CGFloat = 0
    private var device: MTLDevice!
    private var commandQueue: MTLCommandQueue!
    private var gameState: GameState = .showGuide {
        didSet {
            self.viewModel.gameState = gameState
        }
    }
    private var isStartedGuide = false
    private var scoreValue = 0
    
    private var finishView: FinishView?
    private var captureTimer: Timer?
    private var player: AVPlayer?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
        viewModel.startCamera()
    }
    
    override func viewDidFirstLayoutSubView() {
        super.viewDidFirstLayoutSubView()
        view.layoutIfNeeded()
        updateCutoff()
        bindData()
    }
    
    override func viewDidFirstAppear() {
        super.viewDidFirstAppear()
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            if !self.isStartedGuide {
                self.StartGuide()
            } 
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        player?.pause()
        player = nil
    }
    

    // MARK: - Config
    func config() {
        configMetalRender()
    }
    
    private func bindData() {
        
    }
    
    private func configMetalRender() {
        device = MTLCreateSystemDefaultDevice()
        commandQueue = device.makeCommandQueue()
        mtkView = MTKView()
        metalRender = MetalMixingRender(device: device, commandQueue: commandQueue!, mtkView: mtkView)
        mtkView.device = device
        view.insertSubview(mtkView, at: 0)
        mtkView.fitSuperviewConstraint()
        mtkView.delegate = metalRender
    }
    
    private func configGameRender() {
        gameScene = GameScene(size: view.bounds.size)
        gameScene.backgroundColor = .clear
        gameScene.isPaused = false
        
        
        skrenderer = SKRenderer(device: device)
        skrenderer.scene = gameScene
        gameScene.setSoundName(danceDetail.dance.title)
        gameScene.initSomeNode()
    }
    
    func bindViewModel() {
        viewModel.danceDetail = danceDetail
        viewModel.setController(self)
        
        viewModel.poseFramePassthroughSubject?.receive(on: RunLoop.main).sink(receiveValue: { poseFrame in
            self.renderPoseFrame(poseFrame)
        }).store(in: &cancellables)
        
        viewModel.danceFramePassthroughSubject?.sink(receiveValue: { danceFrame in
            var nextModelImage: UIImage?
            let idx = danceFrame.idx
            if idx < self.danceDetail.frames.count - 1 {
                let nextFrame = self.danceDetail.frames[idx + 1]
                nextModelImage = UIImage(contentsOfFile: nextFrame.coverCacheURL().path)
            }
            
            self.gameScene.updateModel(image: UIImage(contentsOfFile: danceFrame.coverCacheURL().path)!, nextModelImage: nextModelImage ?? nil)
            self.gameScene.updateFrameProgress(startTime: danceFrame.startTime, endTime: danceFrame.endTime, middleTime: danceFrame.middleTime)
        }).store(in: &cancellables)
        
        viewModel.addScorePassthroughSubject?.receive(on: RunLoop.main).sink(receiveValue: { score in
            let oldScore = self.scoreValue
            self.scoreValue += score.score
            self.gameScene.updateScore(totalScore: self.scoreValue, getNewScore: score, old: oldScore)
        }).store(in: &cancellables)
        
        viewModel.gameProgressPassthroughSubject?.receive(on: RunLoop.main).sink(receiveValue: { progress in
            self.gameScene.setGameProgress(progress)
        }).store(in: &cancellables)
        
        viewModel.finishGamePassthroughSubject?.receive(on: RunLoop.main).sink(receiveValue: { _ in
            self.finishGame()
            //TODO
//            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
//                if let scene = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate {
//                    scene.setRootViewController()
//                }
//            }
        }).store(in: &cancellables)
        
        viewModel.gameResultPassthroughSubject?.receive(on: RunLoop.main).sink(receiveValue: { gameResult in
            self.finishView?.removeFromSuperview()
            ProgressHUD.dismiss()
            let coordinator = GameResultCoordinator(navigation: self.navigationController!, gameResult: gameResult, delegate: self)
            coordinator.start()
        }).store(in: &cancellables)
        
        viewModel.detectPeopleStatePassthroughSubject?.sink(receiveValue: { state in
            self.detectPeopleView?.dectPeopleStep = state
            if state == .doneStep3 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.detectPeopleView.isHidden = true
                    self.showReady()
                }
            }
        }).store(in: &cancellables)
    }
    
    // MARK: - IBAction
    @IBAction func closePopupButton(_ sender: Any) {
        StartGuide()
    }
    
    // MARK: - Handle
    private func StartGuide() {
        isStartedGuide = true
        UIView.animate(withDuration: 0.3) {
            self.guideView.alpha = 0
        } completion: { _ in
            self.gameState = .detectPeople
            self.showGuideDetectPeople()
        }
    }
    
    private func renderPoseFrame(_ poseFrame: PoseFrame) {
        let renderNode = poseFrame.imageNode
        var topLeft: Vertex!
        var botLeft: Vertex!
        var topRight: Vertex!
        var botRight: Vertex!
        guard let camera = viewModel.camera else {
            return
        }
        let expectSize = mtkView.frame.size
        if camera.size.width / camera.size.height > expectSize.width / expectSize.height {
            topLeft = Vertex(x: -1, y: 1, z: 0.95, s: Float(cutOffX) / 2, t: 0)
            botLeft = Vertex(x: -1, y: -1, z: 0.95, s: Float(cutOffX) / 2, t: 1)
            topRight = Vertex(x: 1, y: 1, z: 0.95, s: 1 - Float(cutOffX) / 2, t: 0)
            botRight = Vertex(x: 1, y: -1, z: 0.95, s: 1 - Float(cutOffX) / 2, t: 1)
        } else {
            topLeft = Vertex(x: -1, y: 1, z: 0.95, s: 0, t: Float(cutOffY) / 2)
            botLeft = Vertex(x: -1, y: -1, z: 0.95, s: 0, t: 1 - Float(cutOffY) / 2)
            topRight = Vertex(x: 1, y: 1, z: 0.95, s: 1, t: Float(cutOffY) / 2)
            botRight = Vertex(x: 1, y: -1, z: 0.95, s: 1, t: 1 - Float(cutOffY) / 2)
        }
        
        renderNode.updateVertexs(vertexs: [topLeft, botLeft, topRight , botRight, topRight, botLeft])
        metalRender.backgroundNode = renderNode
        
        if gameState == .playing {
            metalRender.render(skrenderer: skrenderer)
        } else {
            metalRender.render()
        }
        
    }
    
    private func showReady() {
        self.gameState = .showReady
        viewModel.gameState = gameState
        let readyView = ReadyView()
        view.addSubview(readyView)
        readyView.fitSuperviewConstraint()
        readyView.show {
            self.startGame()
        }
    }
    
    private func showGuideDetectPeople() {
        let view = DetectPeopleView()
        self.view.addSubview(view)
        view.fitSuperviewConstraint()
        view.dectPeopleStep = .inStep1
        detectPeopleView = view
    }
    
    private func startGame() {
        configGameRender()
        gameScene.updateScore(totalScore: 0, getNewScore: nil, old: 0)
        viewModel.startGame()
        gameState = .playing
//        gameScene.setAudioURL(danceDetail.dance.localAudioUrl())
        playMusic()
        captureTimer = Timer.scheduledTimer(withTimeInterval: 1 / 30.0, repeats: true, block: { _ in
            self.viewModel.videoGenerateQueue?.async {
                let img = self.metalRender.lastImage
                self.viewModel.recordGameFrame(image: img)
            }
        })
    }
    
    private func playMusic() {
        let asset = AVAsset(url: danceDetail.dance.localAudioUrl())
        let playerItem = AVPlayerItem(asset: asset)
        
        let audioMix = AVMutableAudioMix()
        var params = [AVMutableAudioMixInputParameters]()
        let tracks = asset.tracks(withMediaType: .audio)
        let volume = AppState.shared.settingMusicValue
        tracks.forEach { track in
            let audioInputParams = AVMutableAudioMixInputParameters.init()
            audioInputParams.setVolume(volume, at: .zero)
            audioInputParams.trackID = track.trackID
            params.append(audioInputParams)
        }
        audioMix.inputParameters = params
        playerItem.audioMix = audioMix
        player = AVPlayer(playerItem: playerItem)
        player?.volume = AppState.shared.settingMusicValue
        player?.play()
    }
        
    private func finishGame() {
        finishView = FinishView()
        self.view.addSubview(finishView!)
        finishView?.fitSuperviewConstraint()
        ProgressHUD.show()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.captureTimer?.invalidate()
            self.captureTimer = nil
        }
    }
    
    // MARK: - Helper
    private func updateCutoff() {
        cutOffX = 0
        cutOffY = 0
        let expectSize = view.frame.size
        guard let camera = viewModel.camera else {
            return
        }
        if camera.size.width / camera.size.height > expectSize.width / expectSize.height {
            let frameSize = CGSize(width: camera.size.height * expectSize.width / expectSize.height, height: camera.size.height)
            cutOffX = CGFloat((camera.size.width - frameSize.width) / camera.size.width)
        } else {
            let frameSize = CGSize(width: camera.size.width, height: camera.size.width * expectSize.height / expectSize.width)
            cutOffY = CGFloat((camera.size.height - frameSize.height) / camera.size.height)
        }
    }
}

extension GameVC: GameResultVCDelegate {
    func gameResultVCDidTapReplayButton(_ vc: GameResultVC) {
        self.gameState = .detectPeople
        self.scoreValue = 0
        showGuideDetectPeople()
        self.viewModel.replay()
        self.gameScene.updateScore(totalScore: self.scoreValue, getNewScore: nil, old: 0)
    }
}
