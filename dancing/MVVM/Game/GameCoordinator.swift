//
//  GameCoordinator.swift
//  dancing
//
//  Created by viet on 20/07/2023.
//

import UIKit

class GameCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: GameVC?
    var navigation: UINavigationController
    var danceDetail: DanceDetail
    
    init(navigation: UINavigationController, danceDetail: DanceDetail) {
        self.navigation = navigation
        self.danceDetail = danceDetail
    }
    
    func start() {
        if !started {
            started = true
            let controller = GameVC.factory()
            controller.coordinator = self
            controller.danceDetail = danceDetail
            navigation.pushViewController(controller, animated: true)
            // Show Controller
            self.controller = controller
            Logger.logEvent(.startGame)
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
            controller?.cancellables.removeAll()
            navigation.popViewController(animated: true)
        }
    }
}
