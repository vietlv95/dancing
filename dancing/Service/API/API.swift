//
//  API.swift
//  dancing
//
//  Created by viet on 19/07/2023.
//

import Foundation
import Alamofire

let BaseURL = "https://mangaserver.skymeta.pro/pose_dance_api/v1_0/"
let SignInPath = "sign_in"
let ListDancePath = "audios/list"
let UploadMatchPath = "match_results/upload/score"
let UploadVideoPath = "upload/video"
let TopMatchPath = "match_results/audios"

class APIStatus {
    var code = 0
    var message = ""
}

class API {
    static let shared = API()
    
    public typealias RequestModifier = (inout URLRequest) throws -> Void
    // MARK: - User Info API
    
    func signin(username: String, password: String, userInfo: UserInfo, completion: ((_ user: UserInfo?)-> Void)?) {
        let params: [String: Any] = [
                "provider":"google",
                "device_id":UIDevice.current.identifierForVendor!.uuidString,
                "user_name":username,
                "password":password,
                "name":userInfo.name,
                "avatar":userInfo.avatar,
                "birth_day":userInfo.birth_day,
                "gender":userInfo.gender,
                "app_version":AppState.shared.appVersion,
        ]
        
        let url = BaseURL + SignInPath
        logCall(url: url)
        
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.default).response { response in
            if let dt = response.data, let json = try? JSONSerialization.jsonObject(with: dt) as? [String: Any], let dict = json["data"] as? [String: Any] {
                let obj = UserInfo()
                obj.loadInfo(from: dict)
                completion?(obj)
            } else {
                completion?(nil)
            }
        }
    }
    
    func signOut(userID: Int) {
        let url = BaseURL + "users/\(userID)/sign_out"
        
        logCall(url: url)
        AF.request(url, method: .post).response { _ in
            
        }
    }
    
    func updateUserInfo(userID: Int, name: String, avatarURL: String?) {
        
        let url = BaseURL + "users/\(userID)/update"
        logCall(url: url)
        
        AF.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(Data(name.utf8), withName: "name")
            if let avatarURL = avatarURL {
                multipartFormData.append(Data(avatarURL.utf8), withName: "avatar")
                1
            }
        }, to: url).response { response in
            1
//            if let dt = response.data, let json = try? JSONSerialization.jsonObject(with: dt) as? [String: Any] {
//
//            } else {
//
//            }
        }
    }
    
    // MARK: - Match API
    func uploadMatch(matchInfo: MatchInfo, success: ((_ matchResult: UploadMatchResult)-> Void)?, failed: ((_ errMessage: String)->Void)?) {
        let url = BaseURL + UploadMatchPath
        logCall(url: url)
        
        let params: [String: Any] = [
            "user_id": matchInfo.userID,
            "audio_id": matchInfo.audioID,
            "score": matchInfo.score,
            "accuracy": matchInfo.accuracy,
            "pose_time": matchInfo.pose_time,
            "video_md5": matchInfo.video_md5,
            "cover_md5": matchInfo.cover_md5,
            "play_info": matchInfo.playInfo,
            "device_id": matchInfo.deviceID
        ]
        print(params)
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.default).response { response in
            var rs: UploadMatchResult?
            if let dt = response.data, let json = try? JSONSerialization.jsonObject(with: dt) as? [String: Any] {
                if let dataDict = json["data"] as? [String: Any]{
                    rs = UploadMatchResult(dict: dataDict["match"] as? [String: Any] ?? [:])
                }
            }
            
            if rs == nil {
                failed?("")
            } else {
                success?(rs!)
            }
        }
    }
    
    func uploadVideo(matchID: Int , videoMd5: String, coverMd5: String, videoAsset: AVURLAsset, coverImagePath: String, completion: ((_ success: Bool)-> Void)?) {
        
        let url = BaseURL + "match_results/\(matchID)/\(UploadVideoPath)"
        logCall(url: url)
        
        AF.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(videoAsset.url, withName: "video")
            multipartFormData.append(URL(fileURLWithPath: coverImagePath), withName: "cover")
            multipartFormData.append(Data(videoMd5.utf8), withName: "video_md5")
            multipartFormData.append(Data(coverMd5.utf8), withName: "cover_md5")
        }, to: url).response { response in
            if let dt = response.data, let json = try? JSONSerialization.jsonObject(with: dt) as? [String: Any] {
                1
            } else {
                1
            }
            completion?(true)
        }
    }
    
    func getTopMatch(danceID: Int, userID: Int, completion: ((_ matchs: [Match])-> Void)?) {
        let url = BaseURL + TopMatchPath + "/\(danceID)"
        let params: [String: Any] = [
            "user_id": userID,
            "length": 20,
            "offset": -1,
            "type": "video"
        ]
        
        logCall(url: url)
        
        AF.request(url, parameters: params).response { response in
            var rs: [Match] = [Match]()
            if let dt = response.data, let json = try? JSONSerialization.jsonObject(with: dt) as? [String: Any] {
                if let dataDict = json["data"] as? [String: Any], let list = dataDict["data"] as? [[String: Any]]{
                    list.forEach { obj in
                        if let matchDict = obj["match"] as? [String: Any], let userDict = obj["user"] as? [String: Any] {
                            let match = Match(dict: matchDict)
                            match.user = User(dict: userDict)
                            rs.append(match)
                        }
                        
                    }
                }
            }
            
            completion?(rs)
        }
    }
    
    func getListMatch(userID: Int, completion: ((_ matchs: [Match])->Void)?) {
        let url = BaseURL + "match_results/users/\(userID)?length=50&offset=0"
        logCall(url: url)
        
        AF.request(url).response { response in
            var rs: [Match] = [Match]()
            if let dt = response.data, let json = try? JSONSerialization.jsonObject(with: dt) as? [String: Any] {
                if let dataDict = json["data"] as? [String: Any], let list = dataDict["data"] as? [[String: Any]]{
                    list.forEach { obj in
                        if let matchDict = obj["match"] as? [String: Any] {
                            let match = Match(dict: matchDict)
                            if let userDict = obj["user"] as? [String: Any] {
                                match.user = User(dict: userDict)
                            }
                            if !match.video.isEmpty {
                                rs.append(match)
                            }
                        }
                    }
                }
            }
            
            completion?(rs)
        }
    }
    
    func deleteMatch(userID: Int, matchID: Int, completion: ((_ success: Bool)->Void)?) {
        let url = BaseURL + "users/\(userID)/matches/\(matchID)/remove"
        logCall(url: url)
        AF.request(url, method: .post).response { response in
            var rs = false
            if let dt = response.data, let json = try? JSONSerialization.jsonObject(with: dt) as? [String: Any] {
                if let code = json["code"] as? Int {
                    rs = code == 0
                }
            }
            completion?(rs)
        }
    }
    
    // MARK: - Dance API
    func getListDance(userID: Int, completion: ((_ dances: [Dance], _ status: APIStatus)->Void)?) {
        let url = BaseURL + ListDancePath + "?user_id=\(userID)"
        logCall(url: url)
        
        AF.request(url).response { response in
            var rs: [Dance] = []
            let apiStatus = APIStatus()
            if let dt = response.data, let json = try? JSONSerialization.jsonObject(with: dt) as? [String: Any] {
                apiStatus.code = json["code"] as? Int ?? 0
                apiStatus.message = json["message"] as? String ?? ""
                let datas = json["data"] as? [[String: Any]]
                datas?.forEach({ dict in
                    let dance = Dance(dict: dict)
                    rs.append(dance)
                })
            }
            completion?(rs, apiStatus)
        }
    }
    
    func getDanceDetail(_ dance: Dance, userID: Int, completion: ((_ danceDetail: DanceDetail?, _ status: APIStatus)->Void)?) {
        let url = BaseURL + "audios/\(dance.id)/info?user_id=\(userID)"
        logCall(url: url)
        
        AF.request(url).response { response in
            var detail: DanceDetail?
            let apiStatus = APIStatus()
            if let dt = response.data, let json = try? JSONSerialization.jsonObject(with: dt) as? [String: Any] {
                apiStatus.code = json["code"] as? Int ?? 0
                apiStatus.message = json["message"] as? String ?? ""
                let datas = json["data"] as? [String: Any]
                if let frames = datas?["frames"] as? [[String: Any]] {
                    detail = DanceDetail()
                    for i in 0..<frames.count {
                        let frameDict = frames[i]
                        let frame = DanceFrame()
                        detail?.frames.append(frame)
                        if let config = frameDict["config"] as? [String: Any], let angles = config["angles"] as? [[String: Any]] {
                            angles.forEach { dict in
                                frame.anglePoints.append(AnglePoint(dict: dict))
                            }
                        }
                        frame.cover = frameDict["frame"] as! String
                        frame.middleTime = frameDict["time"] as! Int
                        frame.idx = i
                        frame.danceID = dance.id
                    }
                    
                }
                
                completion?(detail, apiStatus)
            }
        }
    }
    
    func logCall(url: String) {
        print("Call API \(url)")
    }
}
