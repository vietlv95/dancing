//
//  BufferProvider.swift
//  FaceMask
//
//  Created by Viet Le on 17/05/2023.
//

import UIKit

class BufferProvider: NSObject {
    let inflightBuffersCount: Int = 3
    // 2
    private var uniformsBuffers: [[MTLBuffer?]]
    // 3
    private var avaliableBufferIndex: Int = 0
    var avaliableResourcesSemaphore: DispatchSemaphore
    
    init(device:MTLDevice, sizeOfUniformsBuffer1: Int, sizeOfUniformsBuffer2: Int) {
      avaliableResourcesSemaphore = DispatchSemaphore(value: inflightBuffersCount)
      uniformsBuffers = [[MTLBuffer?]]()
      
      for _ in 0...inflightBuffersCount-1 {
        uniformsBuffers.append([])
      }
    }
    
    func nextUniformBuffer(buffers: [MTLBuffer?]) -> [MTLBuffer?] {
        let cacheBuffers = uniformsBuffers[avaliableBufferIndex]
        
        uniformsBuffers[avaliableBufferIndex] = buffers
        
        avaliableBufferIndex += 1
        if avaliableBufferIndex == inflightBuffersCount{
          avaliableBufferIndex = 0
        }
        return cacheBuffers
    }
    
    deinit{
      for _ in 0...self.inflightBuffersCount{
        self.avaliableResourcesSemaphore.signal()
      }
    }
    
}
