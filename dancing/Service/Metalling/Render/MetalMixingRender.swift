//
//  MyRender.swift
//  Metall
//
//  Created by Viet Le on 13/04/2023.
//

import Foundation
import Metal
import MetalKit
import SpriteKit

class MetalMixingRender: NSObject {
    var commandQueue: MTLCommandQueue!
    
    var clearColor: MTLClearColor?
    var device: MTLDevice!
    lazy var samplerState: MTLSamplerState? = MetalMixingRender.defaultSampler(device: self.device)
    var bufferProvider: BufferProvider
    var depthState: MTLDepthStencilState!
    var mtkView: MTKView!
    var didRender: ((_ image: UIImage)->Void)?
    
    var lastImage = UIImage()
    
    init(device: MTLDevice, commandQueue: MTLCommandQueue!, clearColor: MTLClearColor? = nil, mtkView: MTKView) {
        self.device = device
        self.commandQueue = commandQueue
        self.clearColor = clearColor
        self.bufferProvider = BufferProvider(device: device, sizeOfUniformsBuffer1: 0, sizeOfUniformsBuffer2: 0)
        
        mtkView.clearColor = MTLClearColorMake(1, 1, 1, 1)
        mtkView.framebufferOnly = false
        mtkView.colorPixelFormat = .bgra8Unorm
        
        self.mtkView = mtkView
        mtkView.depthStencilPixelFormat = .depth32Float
        let depthDescriptor = MTLDepthStencilDescriptor()
        depthDescriptor.depthCompareFunction = .lessEqual
        depthDescriptor.isDepthWriteEnabled = true
        self.depthState = device.makeDepthStencilState(descriptor: depthDescriptor)!
        self.mtkView.enableSetNeedsDisplay = true
    }
    
    var backgroundNode: TextureNode?
    var maskNode: TextureNode?
    var skrenderer: SKRenderer?
    
    func updateBackgroundTexture(node: TextureNode?) {
        self.backgroundNode = node
    }
    
    func updateMaskNode(_ node: TextureNode?) {
        self.maskNode = node
    }
    
    func render(skrenderer: SKRenderer? = nil) {
        self.skrenderer = skrenderer
        DispatchQueue.main.async {
            self.mtkView.setNeedsDisplay()
        }
    }
    
    class func defaultSampler(device: MTLDevice) -> MTLSamplerState {
        let sampler = MTLSamplerDescriptor()
        sampler.minFilter             = MTLSamplerMinMagFilter.nearest
        sampler.magFilter             = MTLSamplerMinMagFilter.nearest
        sampler.mipFilter             = MTLSamplerMipFilter.nearest
        sampler.maxAnisotropy         = 1
        sampler.sAddressMode          = MTLSamplerAddressMode.clampToEdge
        sampler.tAddressMode          = MTLSamplerAddressMode.clampToEdge
        sampler.rAddressMode          = MTLSamplerAddressMode.clampToEdge
        sampler.normalizedCoordinates = true
        sampler.lodMinClamp           = 0
          sampler.lodMaxClamp           = .greatestFiniteMagnitude
        return device.makeSamplerState(descriptor: sampler)!
    }
}

extension MetalMixingRender {
    func mergeImage(bgImage: UIImage, topImage: UIImage) -> UIImage {
        autoreleasepool {
            let commandBuffer = commandQueue.makeCommandBuffer()!
            let library = device.makeDefaultLibrary()!
            let mergeFunction = library.makeFunction(name: "merge_fragment")!
            let computePipeline = try! device.makeComputePipelineState(function: mergeFunction)
            
            let textureLoader = MTKTextureLoader(device: device!)
            let bgTexture = try! textureLoader.newTexture(cgImage: getCgImageFromImage(bgImage))
            let topTexture = try! textureLoader.newTexture(cgImage: getCgImageFromImage(topImage))
            
            let textureDescriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: .bgra8Unorm, width: Int(bgImage.size.width), height: Int(bgImage.size.height), mipmapped: true)
            textureDescriptor.usage =  [.shaderRead, .shaderWrite]
            let outTexture = device.makeTexture(descriptor: textureDescriptor)
        
            let commandEncoder = commandBuffer.makeComputeCommandEncoder()
            commandEncoder?.setComputePipelineState(computePipeline)
            commandEncoder?.setTexture(bgTexture, index: 0)
            commandEncoder?.setTexture(topTexture, index: 1)
            commandEncoder?.setTexture(outTexture, index: 2)
            
            let threadsPerThreadGroup = MTLSize(width: 1, height: 1, depth: 1)
            let threadgroupsPerGrid = MTLSize(width: Int(bgImage.size.width), height: Int(bgImage.size.height), depth: 1)
            commandEncoder?.dispatchThreadgroups(threadgroupsPerGrid, threadsPerThreadgroup: threadsPerThreadGroup)
            commandEncoder?.endEncoding()
            
            commandBuffer.commit()
            commandBuffer.waitUntilCompleted()
            
            let kciOptions: [CIImageOption:Any] = [.colorSpace: CGColorSpaceCreateDeviceRGB()]
            guard let ciImage = CIImage.init(mtlTexture: outTexture!, options: kciOptions) else {return UIImage()}
            let transform = CGAffineTransform.identity
                                  .scaledBy(x: 1, y: -1)
                                  .translatedBy(x: 0, y: ciImage.extent.height)
            let transformed = ciImage.transformed(by: transform)
            return UIImage.init(ciImage: transformed)
        }
    }
    
    private func getCgImageFromImage(_ image: UIImage) -> CGImage {
        if let cgImage = image.cgImage {
            return cgImage
        }
        
        let context = CIContext()
        return context.createCGImage(image.ciImage!, from: image.ciImage!.extent)!
    }
}

extension MetalMixingRender: MTKViewDelegate {
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
    }
    
    func draw(in view: MTKView) {
        if backgroundNode == nil {
            return
        }
        if maskNode == nil {
            drawSingleNode(view: view)
        } else {
            drawMixingNode(view: view)
        }
    }
    
    
    func drawSingleNode(view: MTKView) {
        autoreleasepool {
            let drawable = view.currentDrawable
            let commandBuffer = commandQueue.makeCommandBuffer()!
            let library = device.makeDefaultLibrary()!
            
            let renderPassDescriptor = view.currentRenderPassDescriptor!
            renderPassDescriptor.colorAttachments[0].texture = drawable?.texture
            renderPassDescriptor.colorAttachments[0].loadAction = .clear
            renderPassDescriptor.colorAttachments[0].clearColor =
              MTLClearColor(red: 0.0, green: 104.0/255.0, blue: 5.0/255.0, alpha: 1.0)
            renderPassDescriptor.colorAttachments[0].storeAction = .store
            mtkView.clearDepth = 1.0
            
            let pipelineStateDescriptor = MTLRenderPipelineDescriptor()
            pipelineStateDescriptor.vertexFunction = library.makeFunction(name: "basic_vertex")
            pipelineStateDescriptor.fragmentFunction = library.makeFunction(name: "basic_fragment")
            pipelineStateDescriptor.colorAttachments[0].pixelFormat = .bgra8Unorm
            pipelineStateDescriptor.enableTransparent()
            pipelineStateDescriptor.depthAttachmentPixelFormat = self.mtkView.depthStencilPixelFormat
            
            let pipelineState = try! device.makeRenderPipelineState(descriptor: pipelineStateDescriptor)
            renderPassDescriptor.colorAttachments[0].loadAction = .clear
            
            let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor)!
            renderEncoder.setDepthStencilState(depthState)
            renderEncoder.setRenderPipelineState(pipelineState)
            renderEncoder.setFragmentTexture(backgroundNode!.texture, index: 0)
            renderEncoder.setVertexBuffer(backgroundNode!.vertexBuffer, offset: 0, index: 0)
            if let samplerState = samplerState {
                renderEncoder.setFragmentSamplerState(samplerState, index: 0)
            }
            renderEncoder.drawPrimitives(type: .triangle, vertexStart: 0, vertexCount: backgroundNode!.vertexCount, instanceCount: 3)
            
            if skrenderer != nil {
                skrenderer?.update(atTime: CACurrentMediaTime())
                skrenderer?.render(withViewport: view.bounds, renderCommandEncoder: renderEncoder, renderPassDescriptor: renderPassDescriptor, commandQueue: commandQueue)
            }
            renderEncoder.endEncoding()
            
            commandBuffer.present(drawable!)
            commandBuffer.addCompletedHandler { _ in
                let kciOptions: [CIImageOption:Any] = [.colorSpace: CGColorSpaceCreateDeviceRGB()]
                guard let ciImage = CIImage.init(mtlTexture: drawable!.texture, options: kciOptions) else {return}
                let transform = CGAffineTransform.identity
                                      .scaledBy(x: 1, y: -1)
                                      .translatedBy(x: 0, y: ciImage.extent.height)
                let transformed = ciImage.transformed(by: transform)
                self.lastImage = UIImage.init(ciImage: transformed)
            }
            commandBuffer.commit()
        }
    }
    
    func drawMixingNode(view: MTKView) {
        autoreleasepool {
            let drawable = view.currentDrawable
            let commandBuffer = commandQueue.makeCommandBuffer()!
            let library = device.makeDefaultLibrary()!
            
            let renderPassDescriptor = view.currentRenderPassDescriptor!
            renderPassDescriptor.colorAttachments[0].texture = drawable?.texture
            renderPassDescriptor.colorAttachments[0].loadAction = .clear
            renderPassDescriptor.colorAttachments[0].clearColor =
              MTLClearColor(red: 0.0, green: 104.0/255.0, blue: 5.0/255.0, alpha: 1.0)
            renderPassDescriptor.colorAttachments[0].storeAction = .store
            mtkView.clearDepth = 1.0
            
            func renderBg() {
                let pipelineStateDescriptor = MTLRenderPipelineDescriptor()
                pipelineStateDescriptor.vertexFunction = library.makeFunction(name: "basic_vertex")
                pipelineStateDescriptor.fragmentFunction = library.makeFunction(name: "basic_fragment")
                pipelineStateDescriptor.colorAttachments[0].pixelFormat = .bgra8Unorm
                pipelineStateDescriptor.enableTransparent()
                pipelineStateDescriptor.depthAttachmentPixelFormat = self.mtkView.depthStencilPixelFormat
                
                let pipelineState = try! device.makeRenderPipelineState(descriptor: pipelineStateDescriptor)
                renderPassDescriptor.colorAttachments[0].loadAction = .clear
                
                let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor)!
                renderEncoder.setDepthStencilState(depthState)
                renderEncoder.setRenderPipelineState(pipelineState)
                renderEncoder.setFragmentTexture(backgroundNode!.texture, index: 0)
                renderEncoder.setVertexBuffer(backgroundNode!.vertexBuffer, offset: 0, index: 0)
                if let samplerState = samplerState {
                    renderEncoder.setFragmentSamplerState(samplerState, index: 0)
                }
                renderEncoder.drawPrimitives(type: .triangle, vertexStart: 0, vertexCount: backgroundNode!.vertexCount, instanceCount: 3)
                renderEncoder.endEncoding()
            }
            
            func renderTop() {
                let pipelineStateDescriptor = MTLRenderPipelineDescriptor()
                pipelineStateDescriptor.vertexFunction = library.makeFunction(name: "mix_vertex")
                pipelineStateDescriptor.fragmentFunction = library.makeFunction(name: "mix_fragment")
                pipelineStateDescriptor.colorAttachments[0].pixelFormat = .bgra8Unorm
                pipelineStateDescriptor.enableTransparent()
                pipelineStateDescriptor.depthAttachmentPixelFormat = self.mtkView.depthStencilPixelFormat
                
                let pipelineState = try! device.makeRenderPipelineState(descriptor: pipelineStateDescriptor)
                renderPassDescriptor.colorAttachments[0].loadAction = .load
                
                let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor)!
                renderEncoder.setDepthStencilState(depthState)
                renderEncoder.setRenderPipelineState(pipelineState)
                renderEncoder.setFragmentTexture(backgroundNode!.texture, index: 0)
                renderEncoder.setFragmentTexture(maskNode!.texture, index: 1)
                renderEncoder.setVertexBuffer(maskNode!.vertexBuffer, offset: 0, index: 0)
                if let samplerState = samplerState {
                    renderEncoder.setFragmentSamplerState(samplerState, index: 0)
                }
                renderEncoder.drawPrimitives(type: .triangle, vertexStart: 0, vertexCount: maskNode!.vertexCount, instanceCount: 3)
                renderEncoder.endEncoding()
            }
            
            renderBg()
            renderTop()
            
            commandBuffer.present(drawable!)
            commandBuffer.addCompletedHandler { _ in
                let kciOptions: [CIImageOption:Any] = [.colorSpace: CGColorSpaceCreateDeviceRGB()]
                guard let ciImage = CIImage.init(mtlTexture: drawable!.texture, options: kciOptions) else {return}
                let transform = CGAffineTransform.identity
                                      .scaledBy(x: 1, y: -1)
                                      .translatedBy(x: 0, y: ciImage.extent.height)
                let transformed = ciImage.transformed(by: transform)
                self.lastImage = UIImage.init(ciImage: transformed)
            }
            
            commandBuffer.commit()
        }
    }
}

extension MTLRenderPipelineDescriptor {
    func enableTransparent() {
        self.colorAttachments[0].isBlendingEnabled = true
        self.colorAttachments[0].rgbBlendOperation = .add
        self.colorAttachments[0].alphaBlendOperation = .add
        self.colorAttachments[0].sourceRGBBlendFactor = .sourceAlpha
        self.colorAttachments[0].sourceAlphaBlendFactor = .sourceAlpha
        self.colorAttachments[0].destinationRGBBlendFactor = .oneMinusSourceAlpha
        self.colorAttachments[0].destinationAlphaBlendFactor = .oneMinusSourceAlpha
    }
}
