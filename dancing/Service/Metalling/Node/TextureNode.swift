//
//  TextureNode.swift
//  Metall
//
//  Created by Viet Le on 13/04/2023.
//
import Metal
import MetalKit
import Foundation

class TextureNode {
    var texture: MTLTexture!
    var device: MTLDevice!
    var vertexBuffer: MTLBuffer?
    var vertexCount: Int = 0
    var vertexsData: [Float] = []
    var vertexs: [Vertex] = []
    var renderWithTransparent = false
    var renderWithChangeAlpha = false
    var renderWithColor = false
}
