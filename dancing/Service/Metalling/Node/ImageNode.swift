//
//  MetalImageView.swift
//  Metall
//
//  Created by Viet Le on 12/04/2023.
//

import Foundation
import MetalKit
import Metal

enum ImageNodeType {
    case background
    case faceMask
    case sticker
}

class ImageNode: TextureNode {
    let A = Vertex(x: -1, y: 1, z: 1, s: 0, t: 0)
    let B = Vertex(x: -1, y: -1, z: 1, s: 0, t: 1)
    let C = Vertex(x: 1, y: 1, z: 1, s: 1, t: 0)
    let D = Vertex(x: 1, y: -1, z: 1, s: 1, t: 1)
    
    var drawAbleSize: CGSize = .zero
    var image: UIImage?
    var imageNodeType = ImageNodeType.background
    
    init(texture: MTLTexture, device: MTLDevice, drawAbleSize: CGSize = .zero) {
        super.init()
        self.device = device
        self.texture = texture
        self.initVertexPoint()
    }
    
    init(image: UIImage, device: MTLDevice, commandQ: MTLCommandQueue, drawAbleSize: CGSize = .zero) {
      // 1
        super.init()
        self.image = image
        self.device = device
        let texture = ImageTexture.init(image: image, mipmaped: true, drawAbleSize: drawAbleSize)
        texture.loadTexture(device: device, commandQ: commandQ, flip: true)
        self.texture = texture.texture
        initVertexPoint()
    }
    
    init(cgImage: CGImage, device: MTLDevice, commandQ: MTLCommandQueue, drawAbleSize: CGSize = .zero) {
      // 1
        super.init()
        self.device = device
        let texture = ImageTexture.init(cgImage: cgImage, mipmaped: true, drawAbleSize: drawAbleSize)
        texture.loadTexture(device: device, commandQ: commandQ, flip: true)
        self.texture = texture.texture
        initVertexPoint()
    }
    
    private func initVertexPoint() {
        let verticesArray:Array<Vertex> = [
          A,B,C ,B,C,D
        ]
        var vertexData = Array<Float>()
        for vertex in verticesArray{
          vertexData += vertex.floatBuffer()
        }
        
        // 2
        let dataSize = vertexData.count * MemoryLayout.size(ofValue: vertexData[0])
        vertexBuffer = device.makeBuffer(bytes: vertexData, length: dataSize, options: [])!
        vertexCount = verticesArray.count
        self.vertexsData = vertexData
        self.vertexs = verticesArray
    }
    
    func updateVertexs(vertexs: [Vertex]) {
        if vertexs.count == 0 {
            return
        }
        var vertexData = Array<Float>()
        for vertex in vertexs{
          vertexData += vertex.floatBuffer()
        }
        // 2
        let dataSize = vertexData.count * MemoryLayout.size(ofValue: vertexData[0])
        vertexBuffer = device.makeBuffer(bytes: vertexData, length: dataSize, options: [])!
        vertexCount = vertexs.count
        self.vertexsData = vertexData
        self.vertexs = vertexs
    }
    
    func updateMixVertexs(mixVertexs: [MixVertex]) {
        if mixVertexs.count == 0 {
            return
        }
        var vertexData = Array<Float>()
        for vertex in mixVertexs{
          vertexData += vertex.floatBuffer()
        }
        // 2
        let dataSize = vertexData.count * MemoryLayout.size(ofValue: vertexData[0])
        vertexBuffer = device.makeBuffer(bytes: vertexData, length: dataSize, options: [])!
        vertexCount = mixVertexs.count
        self.vertexsData = vertexData
//        self.vertexs = vertexs
    }
}


class ImageTexture: NSObject {
    var texture: MTLTexture!
    var target: MTLTextureType!
    var width: Int!
    var height: Int!
    var depth: Int!
    var format: MTLPixelFormat!
    var hasAlpha: Bool!
    var isMipmaped: Bool!
    let bytesPerPixel:Int! = 4
    let bitsPerComponent:Int! = 8
    var cgImage:CGImage?
    var drawAbleSize: CGSize = .zero
//    var sourceImage: UIImage
    
    init(image: UIImage, mipmaped:Bool, drawAbleSize: CGSize = .zero) {
//        super.init()
        width    = 0
        height   = 0
        depth    = 1
        format   = MTLPixelFormat.rgba8Unorm
        target   = MTLTextureType.type2D
        texture  = nil
        isMipmaped = mipmaped
        if let cgImage = image.cgImage {
            self.cgImage = cgImage
        } else {
            let context = CIContext(options: nil)
             let ciImage = image.ciImage!
            self.cgImage = context.createCGImage(ciImage, from: ciImage.extent)
        }
        self.drawAbleSize = drawAbleSize
        super.init()
    }
    
    init(cgImage: CGImage, mipmaped:Bool, drawAbleSize: CGSize = .zero) {
//        super.init()
        width    = 0
        height   = 0
        depth    = 1
        format   = MTLPixelFormat.rgba8Unorm
        target   = MTLTextureType.type2D
        texture  = nil
        isMipmaped = mipmaped
        self.cgImage = cgImage
        self.drawAbleSize = drawAbleSize
        super.init()
    }
    
    func loadTexture(device: MTLDevice, commandQ: MTLCommandQueue, flip: Bool) {
        drawAbleSize  = CGSize(width: 750, height: 1000)
        let image = self.cgImage!
        let colorSpace = CGColorSpaceCreateDeviceRGB()
      
        if drawAbleSize.width == 0 {
            width = image.width
            height = image.height
        } else {
            width = Int(drawAbleSize.width)
            height = Int(drawAbleSize.height)
            isMipmaped = false
        }
        
      
      let rowBytes = width * bytesPerPixel
      
      let context = CGContext(data: nil, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: rowBytes, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
      let bounds = CGRect(x: 0, y: 0, width: Int(width), height: Int(height))
      context.clear(bounds)
      
      if flip == false{
        context.translateBy(x: 0, y: CGFloat(self.height))
        context.scaleBy(x: 1.0, y: -1.0)
      }
      
      context.draw(image, in: bounds)
      
      let texDescriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: MTLPixelFormat.rgba8Unorm, width: Int(width), height: Int(height), mipmapped: isMipmaped)
      target = texDescriptor.textureType
      texture = device.makeTexture(descriptor: texDescriptor)
      
      let pixelsData = context.data!
      let region = MTLRegionMake2D(0, 0, Int(width), Int(height))
      texture.replace(region: region, mipmapLevel: 0, withBytes: pixelsData, bytesPerRow: Int(rowBytes))
      
      if (isMipmaped == true){
        generateMipMapLayersUsingSystemFunc(texture: texture, device: device, commandQ: commandQ, block: { (buffer) -> Void in
          print("mips generated")
        })
      }
    }
    
    //MARK: - Generating UIImage from texture mip layers
    func image(mipLevel: Int) -> UIImage{
      
      let p = bytesForMipLevel(mipLevel: mipLevel)
      let q = Int(powf(2, Float(mipLevel)))
      let mipmapedWidth = max(width / q,1)
      let mipmapedHeight = max(height / q,1)
      let rowBytes = mipmapedWidth * 4
      
      let colorSpace = CGColorSpaceCreateDeviceRGB()
      
      let context = CGContext(data: p, width: mipmapedWidth, height: mipmapedHeight, bitsPerComponent: 8, bytesPerRow: rowBytes, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
      let imgRef = context.makeImage()
      let image = UIImage(cgImage: imgRef!)
      return image
    }
    
    func image() -> UIImage{
      return image(mipLevel: 0)
    }
    
    //MARK: - Getting raw bytes from texture mip layers
    func bytesForMipLevel(mipLevel: Int) -> UnsafeMutableRawPointer{
      let q = Int(powf(2, Float(mipLevel)))
      let mipmapedWidth = max(Int(width) / q,1)
      let mipmapedHeight = max(Int(height) / q,1)
      
      let rowBytes = Int(mipmapedWidth * 4)
      
      let region = MTLRegionMake2D(0, 0, mipmapedWidth, mipmapedHeight)
      let pointer = malloc(rowBytes * mipmapedHeight)!
      texture.getBytes(pointer, bytesPerRow: rowBytes, from: region, mipmapLevel: mipLevel)
      return pointer
    }
    
    func bytes() -> UnsafeMutableRawPointer{
      return bytesForMipLevel(mipLevel: 0)
    }
    
    func generateMipMapLayersUsingSystemFunc(texture: MTLTexture, device: MTLDevice, commandQ: MTLCommandQueue,block: @escaping MTLCommandBufferHandler){
      
      let commandBuffer = commandQ.makeCommandBuffer()!
      
      commandBuffer.addCompletedHandler(block)
      
      let blitCommandEncoder = commandBuffer.makeBlitCommandEncoder()!
      
      blitCommandEncoder.generateMipmaps(for: texture)
      blitCommandEncoder.endEncoding()
      
      commandBuffer.commit()
    }
}
