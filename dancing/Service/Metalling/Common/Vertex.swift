/**
 * Copyright (c) 2016 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

struct Vertex{
  
    var x,y,z: Float     // position data
    var s,t: Float       // texture coordinates
    var r, g, b, a: Float
    
    init(x: Float, y: Float, z: Float, s: Float, t: Float, alpha: Float) {
        self.x = x
        self.y = y
        self.z = z
        self.s = s
        self.t = t
        self.r = 0
        self.g = 0
        self.b = 0
        self.a = alpha
    }
    
    init(x: Float, y: Float, z: Float, s: Float, t: Float, r: Float, g: Float, b: Float, a: Float) {
        self.x = x
        self.y = y
        self.z = z
        self.s = s
        self.t = t
        self.r = r
        self.g = g
        self.b = b
        self.a = a
    }
    
    init(x: Float, y: Float, z: Float, s: Float, t: Float) {
        self.x = x
        self.y = y
        self.z = z
        self.s = s
        self.t = t
        self.a = 1
        self.r = 0
        self.g = 0
        self.b = 0
    }
  
    func floatBuffer() -> [Float] {
        return [x,y,z,s,t,r,g,b,a]
    }
  
}
