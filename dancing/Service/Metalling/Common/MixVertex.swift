//
//  MixVertex.swift
//  MetalFaceMask
//
//  Created by viet on 16/06/2023.
//

import Foundation
class MixVertex {
    var x,y,z: Float     // position data
    var bg_s, bg_t: Float       // bg coord
    var top_s, top_t: Float // top coord
    var top_a: Float
    
    init(x: Float, y: Float, z: Float, bg_s: Float, bg_t: Float, top_s: Float, top_t: Float) {
        self.x = x
        self.y = y
        self.z = z
        self.bg_s = bg_s
        self.bg_t = bg_t
        self.top_s = top_s
        self.top_t = top_t
        self.top_a = 1
    }
    
    init(x: Float, y: Float, z: Float, bg_s: Float, bg_t: Float, top_s: Float, top_t: Float, top_a: Float) {
        self.x = x
        self.y = y
        self.z = z
        self.bg_s = bg_s
        self.bg_t = bg_t
        self.top_s = top_s
        self.top_t = top_t
        self.top_a = top_a
    }
  
    func floatBuffer() -> [Float] {
        return [x,y,z,bg_s,bg_t,top_s,top_t, top_a]
    }
}
