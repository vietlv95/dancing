//
//  ImageShader.metal
//  Metall
//
//  Created by Viet Le on 12/04/2023.
//

#include <metal_stdlib>
using namespace metal;

struct VertexIn{
    packed_float3 position;
    packed_float2 texCoord;
    packed_float4 color;
};

struct VertexOut{
    float4 position [[position]];
    float2 texCoord;
    float4 color;
};

vertex VertexOut basic_vertex(const device VertexIn* vertex_array [[ buffer(0) ]],
                              unsigned int vid [[ vertex_id ]]) {
    VertexIn VertexIn = vertex_array[vid];
    VertexOut VertexOut;
    VertexOut.position = float4(VertexIn.position,1);
    VertexOut.texCoord = VertexIn.texCoord;
    VertexOut.color = VertexIn.color;
    return VertexOut;
}

fragment float4 basic_fragment(VertexOut interpolated [[stage_in]],
                               texture2d<float>  tex2D     [[ texture(0) ]],
                               sampler           sampler2D [[ sampler(0) ]]) {
    float4 color = tex2D.sample(sampler2D, interpolated.texCoord);
    return color;
}

fragment float4 mask_fragment(VertexOut interpolated [[stage_in]],
                               texture2d<float>  tex2D     [[ texture(0) ]],
                               sampler           sampler2D [[ sampler(0) ]]) {
    float4 color = tex2D.sample(sampler2D, interpolated.texCoord);
    float4 new_color = float4(color.r, color.g, color.b, interpolated.color.a);
    return new_color;
}

fragment float4 change_color_fragment(VertexOut interpolated [[stage_in]],
                               texture2d<float>  tex2D     [[ texture(0) ]],
                               sampler           sampler2D [[ sampler(0) ]]) {
    float4 new_color = float4(interpolated.color.r, interpolated.color.g, interpolated.color.b, interpolated.color.a);
    return new_color;
}


//// mixing
struct MixVertexIn{
    packed_float3 position;
    packed_float2 bgCoord;
    packed_float2 topCoord;
    float alpha;
};

struct MixVertexOut{
    float4 position [[position]];
    float2 bgCoord;
    float2 topCoord;
    float alpha;
};

vertex MixVertexOut mix_vertex(const device MixVertexIn* vertex_array [[ buffer(0) ]],
                               unsigned int vid [[ vertex_id ]]) {
    MixVertexIn VertexIn = vertex_array[vid];
    MixVertexIn top_VertexIn = vertex_array[vid];
    
    MixVertexOut mixVertexOut;
    mixVertexOut.position = float4(top_VertexIn.position,1);
    mixVertexOut.bgCoord = VertexIn.bgCoord;
    mixVertexOut.topCoord = VertexIn.topCoord;
    mixVertexOut.alpha = VertexIn.alpha;
    return mixVertexOut;
}


fragment float4 mix_fragment(MixVertexOut interpolated [[stage_in]],
                             texture2d<float>  tex2D     [[ texture(0) ]],
                             texture2d<float>  top_tex2D     [[ texture(1) ]],
                             sampler           sampler2D [[ sampler(0) ]]) {
    float4 des_color = tex2D.sample(sampler2D, interpolated.bgCoord);
    float4 source_color = top_tex2D.sample(sampler2D, interpolated.topCoord);;
//    return source_color;
    float a = source_color.a;
    if (interpolated.alpha != 1) {
        a = interpolated.alpha;
    }
    float4 color;
    half r = des_color.r * (1 - a) + source_color.r * a;
    half g = des_color.g * (1 - a) + source_color.g * a;
    half b = des_color.b * (1 - a) + source_color.b * a;

    if (a == 0) {
        color = des_color;
    } else {
        color = float4(r, g, b, 1);
    }

    return color;
}

///

kernel void merge_fragment(texture2d<float>  tex2D     [[ texture(0) ]],
                           texture2d<float>  top_tex2D     [[ texture(1) ]],
                           texture2d<half, access::read_write> out_tex2D     [[ texture(2) ]],
                           uint2 gid [[ thread_position_in_grid ]]) {
    
    float4 des_color = tex2D.read(gid).rgba;
    float4 source_color = top_tex2D.read(gid).rgba;
//    return source_color;
    float a = source_color.a;
    half4 color;
    half r = des_color.r * (1 - a) + source_color.r * a;
    half g = des_color.g * (1 - a) + source_color.g * a;
    half b = des_color.b * (1 - a) + source_color.b * a;

    if (a == 0) {
        color = half4(des_color.r, des_color.g, des_color.b, des_color.a);
    } else {
        color = half4(r, g, b, 1);
    }
    
    out_tex2D.write(color, gid);
}

