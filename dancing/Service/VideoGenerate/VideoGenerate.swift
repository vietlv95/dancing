//
//  VideoGenerate.swift
//  FaceMask
//
//  Created by Viet Le on 28/04/2023.
//

import Foundation
import AVFoundation
import UIKit

class VideoGerenate: NSObject {
    fileprivate var writer: AVAssetWriter!
    fileprivate var writerInput: AVAssetWriterInput!
    fileprivate var pixelBufferAdaptor: AVAssetWriterInputPixelBufferAdaptor!
    fileprivate var queue: DispatchQueue!
    static var ciContext = CIContext.init() // we reuse a single context for performance reasons
    var pixelSize: CGSize!
    var lastAddTime: TimeInterval?
    var lastPresentationTime: CMTime = .zero
    var recordedTime: CMTime = .zero
    var lastBetweenTime: CMTime = .zero
    var duration: CMTime = .zero
    var isContinous = true
    
    // MARK: - x
    private let videoFolderPath = NSTemporaryDirectory() + "video"
    private var videoCount = 0
    private var width: CGFloat = 0
    private var height: CGFloat = 0
    private var previousDurations = [CMTime]()
    
    func newComponentVideo() {
        lastAddTime = nil
        duration = .zero
        videoCount += 1
        let videoPath = videoFolderPath + "/temp_\(videoCount).mp4"
        _ = createWriter(path: videoPath)
    }
    
    func finishWriteCurrentComponent() {
        writerInput.markAsFinished()
        writer.finishWriting(completionHandler: {
            let asset = AVAsset(url: self.writer.outputURL)
            self.previousDurations.append(asset.duration)
            self.writerInput = nil
            self.writer = nil
            self.duration = .zero
        })
    }
    
    func numberComponentVideo() -> Int {
        return videoCount
    }

    func removeLastComponentVideo() {
        writer?.cancelWriting()
        previousDurations.removeLast()
        videoCount -= 1
        duration = .zero
        lastAddTime = nil
    }
    
    func totalDuration() -> CMTime {
        let totalPreviousDur = previousDurations.reduce(CMTime.zero) { partialResult, dur in
            return CMTimeAdd(partialResult, dur)
        }
        return CMTimeAdd(totalPreviousDur, duration)
    }
    
    init(queue: DispatchQueue, width: CGFloat, height: CGFloat) {
        super.init()
        self.queue = queue
        self.width = width
        self.height = height
        
        if !FileManager.default.fileExists(atPath: videoFolderPath) {
            try? FileManager.default.createDirectory(atPath: videoFolderPath, withIntermediateDirectories: true)
        }
        
        let contents = try! FileManager.default.contentsOfDirectory(atPath: videoFolderPath)
        contents.forEach { path in
            let fullPath = self.videoFolderPath + "/" + path
            try? FileManager.default.removeItem(atPath: fullPath)
        }
    }
    
    private func createWriter(path: String) -> Bool {
        let sessionStartTime = CMTime.zero
        
        if FileManager.default.fileExists(atPath: path) {
            try? FileManager.default.removeItem(atPath: path)
        }
        let url = URL.init(fileURLWithPath: path)
        
        let outputSettings: [String:Any] = [
            AVVideoCodecKey : AVVideoCodecType.h264, // or .hevc if you like
            AVVideoWidthKey : width,
            AVVideoHeightKey: height,
        ]
        self.pixelSize = CGSize.init(width: width, height: height)
        let input = AVAssetWriterInput.init(mediaType: .video, outputSettings: outputSettings)
        input.expectsMediaDataInRealTime = true
        guard
            let writer = try? AVAssetWriter.init(url: url, fileType: .mp4),
            writer.canAdd(input),
            sessionStartTime != .invalid
        else {
            return false
        }
        
        let sourceBufferAttributes: [String:Any] = [
            String(kCVPixelBufferPixelFormatTypeKey) : kCVPixelFormatType_32ARGB, // yes, ARGB is right here for images...
            String(kCVPixelBufferWidthKey) : width,
            String(kCVPixelBufferHeightKey) : height,
        ]
        let pixelBufferAdaptor = AVAssetWriterInputPixelBufferAdaptor.init(assetWriterInput: input, sourcePixelBufferAttributes: sourceBufferAttributes)
        self.pixelBufferAdaptor = pixelBufferAdaptor
        
        writer.add(input)
        writer.startWriting()
        writer.startSession(atSourceTime: sessionStartTime)
        
        if let error = writer.error {
            NSLog("VideoWriter init: ERROR - \(error)")
            return false
        }
        
        self.writer = writer
        self.writerInput = input
        return true
    }
    
    init?(isRealTime: Bool, queue: DispatchQueue, width: CGFloat, height: CGFloat) {
        
    }

    func add(image: UIImage, addTime: TimeInterval) -> Bool {
        var presentationTime: CMTime!
        
        if let last = self.lastAddTime {
            let rangeTime = CMTime(seconds: addTime - last, preferredTimescale: 1000000)
            presentationTime = CMTimeAdd(self.lastPresentationTime, rangeTime)
            lastBetweenTime = rangeTime
        } else {
            presentationTime = .zero
        }
        
        self.lastAddTime = addTime
        duration = CMTimeAdd(duration, lastBetweenTime)
        if self.writerInput.isReadyForMoreMediaData == false {
            return false
        }
        
        if self.pixelBufferAdaptor.appendPixelBufferForImage(image, presentationTime: presentationTime) {
            self.lastPresentationTime = presentationTime
            return true
        }
        return false
    }
    
    func add(sampleBuffer: CMSampleBuffer) -> Bool {
        if self.writerInput.isReadyForMoreMediaData == false {
            NSLog("VideoWriter: not ready for more data")
            return false
        }

        if self.writerInput.append(sampleBuffer) {
            self.lastPresentationTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
            return true
        }
        return false
    }
    
    func finish(_ completionBlock: ((_ assets: [AVURLAsset])->Void)?) {
        func callback() {
            self.queue.async {
                var rs = [AVURLAsset]()
                for i in 0..<self.videoCount {
                    let videoPath = self.videoFolderPath + "/temp_\(i + 1).mp4"
                    let asset = AVURLAsset.init(url: URL(fileURLWithPath: videoPath), options: [AVURLAssetPreferPreciseDurationAndTimingKey : true])
                    rs.append(asset)
                }
                completionBlock?(rs)
            }
        }
        
        if let writer = writer {
            writerInput?.markAsFinished()
            writer.finishWriting {
                callback()
            }
        } else {
            callback()
        }
        
    }
}

extension AVAssetWriterInputPixelBufferAdaptor {
    func appendPixelBufferForImage(_ image: UIImage, presentationTime: CMTime) -> Bool {
        var appendSucceeded = false
        
        autoreleasepool {
            guard let pixelBufferPool = self.pixelBufferPool else {
                NSLog("appendPixelBufferForImage: ERROR - missing pixelBufferPool") // writer can have error:  writer.error=\(String(describing: self.writer.error))
                return
            }
                
            let pixelBufferPointer = UnsafeMutablePointer<CVPixelBuffer?>.allocate(capacity: 1)
            let _: CVReturn = CVPixelBufferPoolCreatePixelBuffer(
                kCFAllocatorDefault,
                pixelBufferPool,
                pixelBufferPointer
            )
            appendSucceeded = self.append(image.toCVPixelBuffer()!, withPresentationTime: presentationTime)
            pixelBufferPointer.deinitialize(count: 1)
            pixelBufferPointer.deallocate()
//            if let pixelBuffer = pixelBufferPointer.pointee, status == 0 {
//                pixelBuffer.fillPixelBufferFromImage(image)
//                appendSucceeded = self.append(pixelBuffer, withPresentationTime: presentationTime)
//                if !appendSucceeded {
//                    // If a result of NO is returned, clients can check the value of AVAssetWriter.status to determine whether the writing operation completed, failed, or was cancelled.  If the status is AVAssetWriterStatusFailed, AVAsset.error will contain an instance of NSError that describes the failure.
//                    NSLog("VideoWriter appendPixelBufferForImage: ERROR appending")
//                }
//                pixelBufferPointer.deinitialize(count: 1)
//            } else {
//                NSLog("VideoWriter appendPixelBufferForImage: ERROR - Failed to allocate pixel buffer from pool, status=\(status)") // -6680 = kCVReturnInvalidPixelFormat
//            }
//            pixelBufferPointer.deallocate()
        }
        return appendSucceeded
    }
}

extension UIImage {
    func getCGImage() -> CGImage? {
        if let cg = self.cgImage {
            return cg
        }
        
        if let ci = self.ciImage {
            let context = CIContext()
            return context.createCGImage(ci, from: ci.extent)
        }
        return nil
    }
    
    func pixelBufferFromCGImage() -> CVPixelBuffer? {
        guard let image = self.getCGImage() else {
            return nil
        }
        
        let options = [
                kCVPixelBufferCGImageCompatibilityKey as String: NSNumber(value: true),
                kCVPixelBufferCGBitmapContextCompatibilityKey as String: NSNumber(value: true),
                kCVPixelBufferIOSurfacePropertiesKey as String: [:]
        ] as CFDictionary
        
        let size:CGSize = .init(width: image.width, height: image.height)
        var pxbuffer: CVPixelBuffer? = nil
        let status = CVPixelBufferCreate(
            kCFAllocatorDefault,
            Int(size.width),
            Int(size.height),
            kCVPixelFormatType_32BGRA,
            options,
            &pxbuffer)
        guard let pxbuffer = pxbuffer else { return nil }
        
        CVPixelBufferLockBaseAddress(pxbuffer, [])
        guard let pxdata = CVPixelBufferGetBaseAddress(pxbuffer) else {return nil}
        
        let bitmapInfo = CGBitmapInfo(rawValue: CGBitmapInfo.byteOrder32Little.rawValue | CGImageAlphaInfo.premultipliedFirst.rawValue)
        
        guard let context = CGContext(data: pxdata, width: Int(size.width), height: Int(size.height), bitsPerComponent: 8, bytesPerRow: CVPixelBufferGetBytesPerRow(pxbuffer), space: CGColorSpaceCreateDeviceRGB(), bitmapInfo:bitmapInfo.rawValue) else {
            return nil
        }
        context.concatenate(CGAffineTransformIdentity)
        context.draw(image, in: .init(x: 0, y: 0, width: size.width, height: size.height))
        
        ///error: CGContextRelease' is unavailable: Core Foundation objects are automatically memory managed
        ///maybe CGContextRelease should not use it
        CVPixelBufferUnlockBaseAddress(pxbuffer, [])
        return pxbuffer
    }
    
    func toCVPixelBuffer() -> CVPixelBuffer? {
            let attrs = [kCVPixelBufferCGImageCompatibilityKey: kCFBooleanTrue, kCVPixelBufferCGBitmapContextCompatibilityKey: kCFBooleanTrue] as CFDictionary
            var pixelBuffer : CVPixelBuffer?
            let status = CVPixelBufferCreate(kCFAllocatorDefault, Int(self.size.width), Int(self.size.height), kCVPixelFormatType_32ARGB, attrs, &pixelBuffer)
            guard status == kCVReturnSuccess else {
                return nil
            }

            if let pixelBuffer = pixelBuffer {
                CVPixelBufferLockBaseAddress(pixelBuffer, CVPixelBufferLockFlags(rawValue: 0))
                let pixelData = CVPixelBufferGetBaseAddress(pixelBuffer)

                let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
                let context = CGContext(data: pixelData, width: Int(self.size.width), height: Int(self.size.height), bitsPerComponent: 8, bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer), space: rgbColorSpace, bitmapInfo: CGImageAlphaInfo.noneSkipFirst.rawValue)

                context?.translateBy(x: 0, y: self.size.height)
                context?.scaleBy(x: 1.0, y: -1.0)

                UIGraphicsPushContext(context!)
                self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
                UIGraphicsPopContext()
                CVPixelBufferUnlockBaseAddress(pixelBuffer, CVPixelBufferLockFlags(rawValue: 0))
                return pixelBuffer
            }

            return nil
        }
}

extension CVPixelBuffer {
    func fillPixelBufferFromImage(_ image: UIImage) {
        CVPixelBufferLockBaseAddress(self, [])
        
        if let cgImage = image.cgImage {
            let pixelData = CVPixelBufferGetBaseAddress(self)
            let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
            guard
                let context = CGContext.init(
                    data: pixelData,
                    width: Int(image.size.width),
                    height: Int(image.size.height),
                    bitsPerComponent: cgImage.bitsPerComponent,
                    bytesPerRow: CVPixelBufferGetBytesPerRow(self),
                    space: rgbColorSpace,
                    bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue
                )
            else {
                assert(false)
                return
            }
            
            context.draw(cgImage, in: CGRect.init(x: 0, y: 0, width: image.size.width, height: image.size.height))
        } else if let ciImage = image.ciImage {
            VideoGerenate.ciContext.render(ciImage, to: self)
        }
        CVPixelBufferUnlockBaseAddress(self, [])
    }
}

