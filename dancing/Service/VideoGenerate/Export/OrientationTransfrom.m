//
//  OrientationTransfrom.m
//  VideoSlowMotion
//
//  Created by VietLV on 10/25/19.
//  Copyright © 2019 Solar. All rights reserved.
//

#import "OrientationTransfrom.h"

@implementation OrientationTransfrom
- (id)initWithOrientation:(UIImageOrientation)orientation rotationAngle:(float)rotationAngle
{
    if (self = [super init]) {
        self.orientation = orientation;
        self.rotationAngle = rotationAngle;
    }
    return self;
}

- (BOOL)needRotation {
    return self.rotationAngle != 0;
}

- (BOOL)isPortrait {
    return self.needRotation;
}

+ (OrientationTransfrom*)orientationFromTransform:(CGAffineTransform)transform
{
    UIImageOrientation assetOrientation = UIImageOrientationUp;
    float rotationAngle = 0;
    if (transform.a == 0 && transform.b == 1.0 && transform.c == -1.0 && transform.d == 0  ){
        assetOrientation = UIImageOrientationRight;
        rotationAngle = -M_PI_2;
    } else if (transform.a == 0 && transform.b == -1.0 && transform.c == 1.0 && transform.d == 0) {
        assetOrientation = UIImageOrientationLeft;
        rotationAngle = M_PI_2;
    } else if (transform.a == 1.0 && transform.b == 0 && transform.c == 0 && transform.d == 1.0) {
        assetOrientation = UIImageOrientationUp;
    } else if (transform.a == -1.0 && transform.b == 0 && transform.c == 0 && transform.d == -1.0) {
        assetOrientation = UIImageOrientationDown;
    }
    
    return [[OrientationTransfrom alloc] initWithOrientation:assetOrientation rotationAngle:rotationAngle];
}
@end
