//
//  VideoExporter.h
//  FaceMask
//
//  Created by Viet Le on 28/04/2023.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
NS_ASSUME_NONNULL_BEGIN

@interface VideoExporter : NSObject
@property(nonatomic) AVMutableComposition* composition;
@property(nonatomic, nullable) AVMutableVideoComposition* videoComposition;
@property(nonatomic) AVMutableAudioMix* audioMix;
- (void)exportVideoAtURL:(NSURL *)url completeHandler:(nonnull void (^)(BOOL))block progress:(nonnull void (^)(float))progressBlock;
@end

NS_ASSUME_NONNULL_END
