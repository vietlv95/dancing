//
//  VideoExporter.m
//  FaceMask
//
//  Created by Viet Le on 28/04/2023.
//

#import "VideoExporter.h"
#import "SDAVAssetExportSession.h"
#import "OrientationTransfrom.h"

@interface VideoExporter()
@property (nonatomic) SDAVAssetExportSession *exportSession;
@end

@implementation VideoExporter
- (void)cancelExportVideo
{
    [self.exportSession cancelExport];
}

- (void)exportVideoAtURL:(NSURL *)url completeHandler:(nonnull void (^)(BOOL))block progress:(nonnull void (^)(float))progressBlock {
    
    if ([NSFileManager.defaultManager fileExistsAtPath:url.path]) {
        [NSFileManager.defaultManager removeItemAtPath:url.path error:nil];
    }
    
    CGSize videoSize = self.composition.naturalSize;
    AVAssetTrack *videoTrack = [self.composition tracksWithMediaType:AVMediaTypeVideo].lastObject;
    OrientationTransfrom *orientaion = [OrientationTransfrom orientationFromTransform:videoTrack.preferredTransform];
    if (orientaion.isPortrait) {
        videoSize = CGSizeMake(videoSize.height, videoSize.width);
    }
    
    SDAVAssetExportSession* encoder = [[SDAVAssetExportSession alloc] initWithAsset:self.composition];
    encoder.outputFileType = AVFileTypeMPEG4;
    encoder.outputURL = url;
    encoder.videoComposition = self.videoComposition;
    encoder.audioMix = self.audioMix;
    encoder.videoSettings = @{
        AVVideoHeightKey : @(videoSize.height),
        AVVideoWidthKey : @(videoSize.width),
        AVVideoCodecKey : AVVideoCodecTypeH264,
        AVVideoCompressionPropertiesKey : @ {
            AVVideoAverageBitRateKey : @6000000,
            AVVideoProfileLevelKey : AVVideoProfileLevelH264MainAutoLevel,
        },
    };

    encoder.audioSettings = @{
        AVFormatIDKey : @(kAudioFormatMPEG4AAC),
        AVNumberOfChannelsKey : @2,
        AVSampleRateKey : @44100,
        AVEncoderBitRateKey : @128000,
    };

    [encoder exportAsynchronouslyWithCompletionHandler:^{
        if (encoder.status == AVAssetExportSessionStatusCompleted) {
            block(YES);
        } else {
            block(NO);
        }
    }];
    
    [encoder setProgressBlock:progressBlock];
    self.exportSession = encoder;
}
@end
