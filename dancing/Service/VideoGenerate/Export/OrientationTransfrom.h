//
//  OrientationTransfrom.h
//  VideoSlowMotion
//
//  Created by VietLV on 10/25/19.
//  Copyright © 2019 Solar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface OrientationTransfrom : NSObject
@property (nonatomic) UIImageOrientation orientation;
@property (nonatomic, readonly) BOOL isPortrait;
@property (nonatomic, readonly) BOOL needRotation;
@property (nonatomic) float rotationAngle;

+ (OrientationTransfrom*)orientationFromTransform:(CGAffineTransform)transform;
@end

NS_ASSUME_NONNULL_END
