//
//  PlayVideoVC.swift
//  dancing
//
//  Created by viet on 01/08/2023.
//

import Foundation
import AVFoundation

class PlayVideoVC: UIViewController {
    weak var player: AVPlayer? {
        didSet {
            self.updatePlayer()
        }
    }
    private var playerLayer: AVPlayerLayer?
    
    var autoPauseWhenDisappear = true
    var isReplay = true
    var videoGravity: AVLayerVideoGravity = .resizeAspect
    
    private var indicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        view.cornerRadius = 5
        
        view.addSubview(bgImageView)
        bgImageView.fitSuperviewConstraint()
        bgImageView.backgroundColor = UIColor.black
        
        indicator = UIActivityIndicatorView()
        view.addSubview(indicator)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            indicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            indicator.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
        indicator.style = .large
        indicator.color = .white
        indicator.hidesWhenStopped = true
        indicator.startAnimating()

        view.addSubview(previewVideoView)
        previewVideoView.backgroundColor = .clear
        previewVideoView.fitSuperviewConstraint()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    private var viewDidLayoutSubView: Bool = false
    private var bgImageView:UIImageView!  = UIImageView()
    private var previewVideoView: UIView = UIView()
    
    func viewDidFirstLayoutSubView() {
//        return
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        play()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        pause()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.layoutIfNeeded()
        
        self.playerLayer?.frame = self.previewVideoView.bounds
    }
    
    
    func play() {
        player?.play()
    }
    
    func pause() {
        player?.pause()
    }
    
    private func updatePlayer() {
        player?.addObserver(self, forKeyPath: "status", context: nil)
        
        guard let player = player else {
            return
        }
//        bgImageView?.isHidden = true
        playerLayer?.removeFromSuperlayer()
        playerLayer = AVPlayerLayer(player: player)
        
        playerLayer?.videoGravity = videoGravity
        playerLayer?.backgroundColor = UIColor.clear.cgColor
        previewVideoView.layer.addSublayer(playerLayer!)
        addPlayerItemObserver(playerItem: player.currentItem!)
        self.playerLayer?.frame = self.previewVideoView.bounds
        self.play()
    }
    
    private func addPlayerItemObserver(playerItem: AVPlayerItem) {
        removeCurrentPlayerItemObserver()
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidPlayToEndTimeNotification), name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
    }
    
    private func removeCurrentPlayerItemObserver() {
        if let playerItem = player?.currentItem {
            NotificationCenter.default.removeObserver(self, name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        }
    }
    
    @objc func playerItemDidPlayToEndTimeNotification() {
        if isReplay {
            player?.seek(to: .zero, completionHandler: { _ in
                self.player?.play()
            })
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "status" {
            if player?.status == .readyToPlay {
                bgImageView.isHidden = true
                indicator.stopAnimating()
            }
        }
    }
    
    deinit {
        removeCurrentPlayerItemObserver()
    }
}
