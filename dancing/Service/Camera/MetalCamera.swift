//
//  MetalCamera.swift
//  FaceMask
//
//  Created by Viet Le on 26/04/2023.
//

import Foundation
import Metal
import MetalKit
import AVFoundation
import AVKit

enum CameraQuality {
    case hd
    case sd
}

protocol MetalCameraDelegate: AnyObject {
    func metalCamera(_ camera: MetalCamera, didCapturePoseFrame poseFrame: PoseFrame)
}

let poseTrackQueue = DispatchQueue(label: "poseTrackQueue", qos: .userInitiated, attributes:[], autoreleaseFrequency: .workItem)

class MetalCamera: NSObject {
    weak var delegate: MetalCameraDelegate?
    
    var session: AVCaptureSession?
    var isDetectFace = true
    var poseTracking: PoseTracking!
    var size: CGSize = .zero
    var drawAbleSize: CGSize = .zero
    private var device: MTLDevice!
    private var commandQueue: MTLCommandQueue!
    private var mtlDevice: MTLDevice!
    
    private var excuting = false
    private var quality: CameraQuality = .hd
    
    init(device: MTLDevice, commandQueue: MTLCommandQueue, mtlDevice: MTLDevice, delegate: MetalCameraDelegate) {
        super.init()
        self.commandQueue = commandQueue
        self.mtlDevice = mtlDevice
        self.device = device
        self.delegate = delegate
        prepareMPFaceMeshRequest()
    }
    
    fileprivate func prepareMPFaceMeshRequest() {
        DispatchQueue.main.async {
            self.poseTracking = PoseTracking()
            self.poseTracking!.delegate = self
            self.poseTracking!.startGraph()
        }
    }
    
    func startSession(quality: CameraQuality = .hd) -> Bool {
        self.quality = quality
        session = setupAVCaptureSession()
        self.updateQuality(quality)
        DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + 0, execute: {
            self.session?.startRunning()
            
        })
        
        return session != nil
    }
    
    func updateQuality(_ quality: CameraQuality) {
        self.quality = quality
        setSessionPreset()
    }
    
    func pauseSession() {
        
    }
    
    func resumeSession() {
        
    }
    
    func selectCameraPosition(_ position: AVCaptureDevice.Position) -> Bool {
        session?.beginConfiguration()
        if let currentInput = session?.inputs.first {
            session?.removeInput(currentInput)
        }
        
        guard let newCamera = AVCaptureDevice.default(.builtInWideAngleCamera,
                                                   for: .video,
                                                   position: position) else {
//          fatalError("No front video camera available")
            return false
        }
        let newCameraInput = try! AVCaptureDeviceInput(device: newCamera)
        session?.addInput(newCameraInput)
        updateOutput()
        session?.commitConfiguration()
        return true
    }
    
    func updateOutput() {
        if let currentOutput = session?.outputs.first {
            session?.removeOutput(currentOutput)
        }
        let videoOutput = AVCaptureVideoDataOutput()
        let dataOutputQueue = DispatchQueue(label: "video data queue", qos: .userInitiated, attributes:[], autoreleaseFrequency: .workItem)
        videoOutput.setSampleBufferDelegate(self, queue: dataOutputQueue)
        videoOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_32BGRA]
        
        // Add the video output to the capture session
        session?.addOutput(videoOutput)
        
        let videoConnection = videoOutput.connection(with: .video)
        videoConnection?.videoOrientation = .portrait
        videoConnection?.isVideoMirrored = true
    }
    
    func setSessionPreset() {
        session?.beginConfiguration()
        if quality == .sd {
            session?.sessionPreset = .vga640x480
            self.size = CGSize(width: 480, height: 640)
        } else {
            session?.sessionPreset = .hd1280x720
            self.size = CGSize(width: 720, height: 1280)
        }
        session?.commitConfiguration()
    }
    
    func stopSession() {
        session?.stopRunning()
    }
    
    fileprivate func setupAVCaptureSession() -> AVCaptureSession? {
        var session: AVCaptureSession? = AVCaptureSession()
//        setSessionPreset()
        guard let camera = AVCaptureDevice.default(.builtInWideAngleCamera,
                                                   for: .video,
                                                   position: .front) else {
//            Logger.logEvent(name: "camera_session_error")
            return nil
        }
        
//        try? camera.lockForConfiguration()
//        let supporrtedFrameRanges = camera.activeFormat.videoSupportedFrameRateRanges.first
//        camera.activeVideoMaxFrameDuration = CMTimeMake(value: 60, timescale: 1)
//        camera.unlockForConfiguration()

        do {
          let cameraInput = try AVCaptureDeviceInput(device: camera)
          session?.addInput(cameraInput)
        } catch {
          session = nil
        }
        
        // Create the video data output
        let videoOutput = AVCaptureVideoDataOutput()
        let dataOutputQueue = DispatchQueue(label: "video data queue", qos: .userInitiated, attributes:[], autoreleaseFrequency: .workItem)
        videoOutput.setSampleBufferDelegate(self, queue: dataOutputQueue)
        videoOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_32BGRA]
        
        // Add the video output to the capture session
        session?.addOutput(videoOutput)
        
        let videoConnection = videoOutput.connection(with: .video)
        videoConnection?.videoOrientation = .portrait
        videoConnection?.isVideoMirrored = true
        return session
    }
    
    private func texture(sampleBuffer: CMSampleBuffer?, textureCache: CVMetalTextureCache?, planeIndex: Int = 0, pixelFormat: MTLPixelFormat = .bgra8Unorm) throws -> MTLTexture {
        guard let sampleBuffer = sampleBuffer else {
           fatalError()
        }
        guard let textureCache = textureCache else {
            fatalError()
        }
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            fatalError()
        }
        
        let isPlanar = CVPixelBufferIsPlanar(imageBuffer)
        let width = isPlanar ? CVPixelBufferGetWidthOfPlane(imageBuffer, planeIndex) : CVPixelBufferGetWidth(imageBuffer)
        let height = isPlanar ? CVPixelBufferGetHeightOfPlane(imageBuffer, planeIndex) : CVPixelBufferGetHeight(imageBuffer)
        
        var imageTexture: CVMetalTexture?
        
        let result = CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorDefault, textureCache, imageBuffer, nil, pixelFormat, width, height, planeIndex, &imageTexture)

        guard
            let unwrappedImageTexture = imageTexture,
            let texture = CVMetalTextureGetTexture(unwrappedImageTexture),
            result == kCVReturnSuccess
        else {
            fatalError()
        }

        return texture
    }
    
    deinit {
        session?.stopRunning()
    }
}

extension MetalCamera: AVCaptureVideoDataOutputSampleBufferDelegate {
    public func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            fatalError("err")
        }
        let timeStamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
//        guard let t = img?.pixelBufferFromCGImage() else {
//            return
//        }
        if isDetectFace {
            poseTrackQueue.async {
                self.poseTracking.processVideoFrame(imageBuffer, timeStamp: timeStamp)
                self.excuting = true
            }
        }
    }
}

extension MetalCamera: PoseTrackingDelegate {
    func poseTrackingDidReceive(_ texture: MTLTexture, poses: [[PoseLandmarkPoint]], timeStamp: Int) {
        poseTrackQueue.async { [self] in
            self.excuting = false
            let node = ImageNode(texture: texture, device: device!)
            delegate?.metalCamera(self, didCapturePoseFrame: PoseFrame(imageNode: node, poses: poses))
        }
    }
}
