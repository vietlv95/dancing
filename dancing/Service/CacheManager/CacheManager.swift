//
//  CacheManager.swift
//  dancing
//
//  Created by viet on 08/08/2023.
//

import Foundation

class CacheManager {
    
    static let shared = CacheManager()
    
    func dataFolder() -> String {
        return documentPath().appending("/data")
    }
    
    func highScoreVideoFolder() -> String {
        return dataFolder().appending("/high_score_video")
    }
    
    func saveVideoFolder() -> String {
        return dataFolder().appending("/video")
    }
    
    func cacheFrameFolder() -> String {
        return dataFolder().appending("/frame")
    }
    
    func audioFolder() -> String {
        return dataFolder().appending("/audio")
    }
    
    func initFolderIfNeed() {
        try? FileManager.default.createDirectory(atPath: dataFolder(), withIntermediateDirectories: true)
        try? FileManager.default.createDirectory(atPath: highScoreVideoFolder(), withIntermediateDirectories: true)
        try? FileManager.default.createDirectory(atPath: saveVideoFolder(), withIntermediateDirectories: true)
        try? FileManager.default.createDirectory(atPath: cacheFrameFolder(), withIntermediateDirectories: true)
        try? FileManager.default.createDirectory(atPath: audioFolder(), withIntermediateDirectories: true)
    }
    
    func clearFrameAndAudioCache() {
        clearContentOfFolder(cacheFrameFolder())
        clearContentOfFolder(audioFolder())
    }
    
    private func clearContentOfFolder(_ path: String) {
        let subFiles = try? FileManager.default.contentsOfDirectory(atPath: path)
        subFiles?.forEach({ subFile in
            let subFilePath = path.appending("/\(subFile)")
            try? FileManager.default.removeItem(atPath: subFilePath)
        })
    }
    
    private func documentPath() -> String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last!
    }
}
