//
//  PhotoPermission.h
//  VideoSlowMotion
//
//  Created by Thanh Vu on 11/2/19.
//  Copyright © 2019 Solar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>
NS_ASSUME_NONNULL_BEGIN

@interface PhotoPermission : NSObject
+ (void)requestPermissionWithCompletion:(void (^)(PHAuthorizationStatus status))block;
@end

NS_ASSUME_NONNULL_END
