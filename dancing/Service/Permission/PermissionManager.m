//
//  PermissionManager.m
//  VideoSlowMotion
//
//  Created by VietLV on 1/6/20.
//  Copyright © 2020 Solar. All rights reserved.
//

#import "PermissionManager.h"
#import <MediaPlayer/MediaPlayer.h>
#import <Photos/Photos.h>
#import "UIViewController+Solar.h"

static PermissionManager *shared;

@implementation PermissionManager

+ (PermissionManager*)defaultManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[PermissionManager alloc] init];
    });
    return shared;
}

- (void)firstRequestPermissionWithCompletionHandler:(void (^)(void))handler {
    if ([PHPhotoLibrary authorizationStatus] != PHAuthorizationStatusNotDetermined) {
        return;
    }
    [self requestCameraPermissionOn:nil completionHandler:^(BOOL status) {
        [self requestMicrophonePermissionOn:nil completionHandler:^(BOOL status) {
            [self requestPhotoPermissionOn:nil completionHandler:^(BOOL status) {
                handler();
            }];
        }];
    }];
}

- (void)requestPermissionWithType:(PermissionType)type on:(UIViewController *)vc completionHandler:(void (^)(BOOL))handler
{
    switch (type) {
        case PermissionTypePhoto:
            [self requestPhotoPermissionOn:vc completionHandler:handler];
            break;
        case PermissionTypeMusic:
            [self requestMusicPermissionOn:vc completionHandler:handler];
            break;
        case PermissionTypeCamera:
            [self requestCameraPermissionOn:vc completionHandler:handler];
            break;
        case PermissionTypeMicroPhone:
            [self requestMicrophonePermissionOn:vc completionHandler:handler];
            break;
        default:
            break;
    }
}

- (void)requestPhotoPermissionOn:(UIViewController*)vc completionHandler:(void (^)(BOOL status))handler
{
    
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        if (@available(iOS 14, *)) {
            if (status == PHAuthorizationStatusLimited) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [vc showAlertWithTitle:@"Warning" message:@"\nPlease gave permission to access all in your photo library.\n\nThank you !" actionsTitle:@[@"OK"] actionHandler:^(NSInteger actionIndex) {
                        [self openSetting];
                    }];
                });
                return;
            }
        }
        if (status == PHAuthorizationStatusAuthorized) {
            if (handler) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(true);
                });
            }
        } else {
            if (handler) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(false);
                });
            }
            if (!vc) {
                return ;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [vc showAlertWithTitle:@"Warning" message:@"\nPlease gave permission to access your photo library.\n\nThank you !" actionsTitle:@[@"OK"] actionHandler:^(NSInteger actionIndex) {
                    [self openSetting];
                }];
            });
        }
    }];
}

- (void)requestMusicPermissionOn:(UIViewController*)vc completionHandler:(void (^)(BOOL status))handler
{
    void(^requestPermission)(void) = ^(void) {
        [MPMediaLibrary requestAuthorization:^(MPMediaLibraryAuthorizationStatus status) {
            if (handler) {
                handler(status == MPMediaLibraryAuthorizationStatusAuthorized);
            }
        }];
    };

    
    MPMediaLibraryAuthorizationStatus status = MPMediaLibrary.authorizationStatus;
    if (status == MPMediaLibraryAuthorizationStatusNotDetermined ) {
        requestPermission();
        return;
    }
    
    if (status == MPMediaLibraryAuthorizationStatusAuthorized) {
        dispatch_async(dispatch_get_main_queue(), ^{
            handler(true);
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            handler(false);
        });
        if (!vc) {
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [vc showAlertWithTitle:@"Warning" message:@"Please gave permission to access your Music.\n\n Thank you!" actionsTitle:@[@"OK"] actionHandler:^(NSInteger actionIndex) {
                [self openSetting];
            }];
        });
    }
}

- (void)requestCameraPermissionOn:(UIViewController*)vc completionHandler:(void (^)(BOOL status))handler
{
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        if (granted) {
            if (handler) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(true);
                });
            }
        } else {
            if (handler) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(false);
                });
            }
            if (!vc) {
                return ;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [vc showAlertWithTitle:@"Warning" message:@"\nPlease gave permission to access your camera.\n\n Thank you!" actionsTitle:@[@"OK"] actionHandler:^(NSInteger actionIndex) {
                    [self openSetting];
                }];
            });
        }
    }];
}

- (void)requestMicrophonePermissionOn:(UIViewController*)vc completionHandler:(void (^)(BOOL status))handler
{
    [AVAudioSession.sharedInstance requestRecordPermission:^(BOOL granted) {
        if (granted) {
            if (handler) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(true);
                });
            }
        } else {
            if (handler) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(false);
                });
            }
            if (!vc) {
                return ;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [vc showAlertWithTitle:@"Warning" message:@"\nPlease gave permission to access your microphone.\n\n Thank you!" actionsTitle:@[@"OK"] actionHandler:^(NSInteger actionIndex) {
                    [self openSetting];
                }];
            });
        }
    }];
}

- (void) openSetting {
    NSURL *settingURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    if ([UIApplication.sharedApplication canOpenURL:settingURL]) {
        [UIApplication.sharedApplication openURL:settingURL options:@{} completionHandler:nil];
    }
}
@end
