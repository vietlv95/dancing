//
//  UIViewController+Solar.h
//  VideoSlowMotion
//
//  Created by Thanh Vu on 10/16/19.
//  Copyright © 2019 Solar. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (Solar)
- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message;
- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message actionsTitle:(nullable NSArray<NSString *>*)actionsTitle actionHandler:(nullable void (^)(NSInteger actionIndex))handler;

- (void)showActionSheetWithTitle:(NSString*__nullable)title message:(NSString*__nullable)message actionsTitle:(nullable NSArray<NSString*>*)actionsTitle actionHandler:(nullable void (^)(NSInteger actionIndex))handler;
- (void)showActionSheetWithTitle:(NSString* __nullable)title message:(NSString*__nullable)message actionsTitle:(nullable NSArray<NSString*>*)actionsTitle destructiveIndexes:(nullable NSArray<NSNumber*>*)destructiveIndexes cancelIndex:(NSInteger)cancelIndex actionHandler:(nullable void (^)(NSInteger actionIndex))handler;
@end

NS_ASSUME_NONNULL_END
