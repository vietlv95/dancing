//
//  PhotoPermission.m
//  VideoSlowMotion
//
//  Created by Thanh Vu on 11/2/19.
//  Copyright © 2019 Solar. All rights reserved.
//

#import "PhotoPermission.h"

@implementation PhotoPermission
+ (void)requestPermissionWithCompletion:(void (^)(PHAuthorizationStatus))block
{
    if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusNotDetermined) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            block(status);
        }];
    } else {
        block([PHPhotoLibrary authorizationStatus]);
    }
}
@end
