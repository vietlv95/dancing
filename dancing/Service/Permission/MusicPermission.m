//
//  MusicPermission.m
//  VideoSlowMotion
//
//  Created by VietLV on 12/9/19.
//  Copyright © 2019 Solar. All rights reserved.
//

#import "MusicPermission.h"
#import <UIKit/UIKit.h>
#import "UIViewController+Solar.h"

@implementation MusicPermission
+ (void)requestPermissionOn:(UIViewController *)vc completion:(void (^)(BOOL))block
{
    void(^requestPermission)(void) = ^(void) {
           [MPMediaLibrary requestAuthorization:^(MPMediaLibraryAuthorizationStatus status) {
               if (status == MPMediaLibraryAuthorizationStatusAuthorized) {
                   block(true);
               }
           }];
       };
       
       MPMediaLibraryAuthorizationStatus status = MPMediaLibrary.authorizationStatus;
       
       switch (status) {
           case MPMediaLibraryAuthorizationStatusAuthorized:
               block(true);
               break;
           case MPMediaLibraryAuthorizationStatusNotDetermined:
               requestPermission();
               break;
           default:
               [vc showAlertWithTitle:@"Warning" message:@"Musics library permission is not granted. Please grant int in app setting" actionsTitle:@[@"OK"] actionHandler:^(NSInteger actionIndex) {
                   NSURL *urlSetting = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                   if ([UIApplication.sharedApplication canOpenURL:urlSetting]) {
                       [UIApplication.sharedApplication openURL:urlSetting options:@{} completionHandler:nil];
                   }
               }];
               break;
       }
}
@end
