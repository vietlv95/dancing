//
//  PermissionManager.h
//  VideoSlowMotion
//
//  Created by VietLV on 1/6/20.
//  Copyright © 2020 Solar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    PermissionTypePhoto,
    PermissionTypeMusic,
    PermissionTypeCamera,
    PermissionTypeMicroPhone
} PermissionType;

@interface PermissionManager : NSObject
@property (class, nonatomic, readonly) PermissionManager *defaultManager;
- (void)firstRequestPermissionWithCompletionHandler:(void (^)(void))handler;
- (void)requestPermissionWithType:(PermissionType)type on:(UIViewController *)vc completionHandler:(void (^)(BOOL status))handler;
@end

NS_ASSUME_NONNULL_END
