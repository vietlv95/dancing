//
//  MusicPermission.h
//  VideoSlowMotion
//
//  Created by VietLV on 12/9/19.
//  Copyright © 2019 Solar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>

NS_ASSUME_NONNULL_BEGIN

@interface MusicPermission : NSObject
+ (void)requestPermissionOn:(UIViewController*)vc completion:(void (^)(BOOL status))block;
@end

NS_ASSUME_NONNULL_END
